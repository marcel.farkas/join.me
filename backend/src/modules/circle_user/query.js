import { isAuth } from '../../libs/auth';

export const userCircles = async (
  _,
  __,
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  const { id } = isAuth(authorization);

  const circles = await dbConnection.query(
    `SELECT circle.* 
    FROM circle_users 
    INNER JOIN user ON circle_users.user_id = user.id 
    INNER JOIN circle ON circle.id = circle_users.circle_id 
    WHERE user_id = ?`,
    [id],
  );

  return circles;
};
