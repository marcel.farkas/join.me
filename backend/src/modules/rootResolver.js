import { queries as UserQueries, mutations as UserMutations } from './user';
import { mutations as ResetPasswordMutations } from './passwordReset';
import { queries as EventQueries, mutations as EventMutations } from './event';
import { queries as CircleUserQueries } from './circle_user';
import {
  queries as FriendQueries,
  mutations as FriendMutations,
} from './friend';
import { queries as BusinessQueries } from './business';
import {
  queries as InvitationQueries,
  mutations as InvitationMutations,
} from './event_invitation';
import {
  queries as NotificationQueries,
  mutations as NotificationMutations,
} from './notification';
import { mutations as ChatMutations } from './chat';
import { queries as FeedQueries, mutations as FeedMutations } from './feed';
import {
  queries as CircleQueries,
  mutations as CircleMutations,
} from './circle';
import { queries as CircleInvitationQueries } from './circle_invitation';

export default {
  Query: {
    ...UserQueries,
    ...EventQueries,
    ...CircleUserQueries,
    ...FriendQueries,
    ...BusinessQueries,
    ...InvitationQueries,
    ...NotificationQueries,
    ...FeedQueries,
    ...CircleQueries,
    ...CircleInvitationQueries,
  },
  Mutation: {
    ...UserMutations,
    ...ResetPasswordMutations,
    ...EventMutations,
    ...InvitationMutations,
    ...NotificationMutations,
    ...ChatMutations,
    ...FeedMutations,
    ...FriendMutations,
    ...CircleMutations,
  },

  User: {
    async friends(parent, _, { dbConnection }) {
      return await dbConnection.query(
        `SELECT distinct * 
        FROM friends 
        INNER JOIN user ON friends.friend_id = user.id 
        WHERE user_id = ?`,
        [parent.id],
      );
    },
    async joinedEvents(parent, _, { dbConnection }) {
      return await dbConnection.query(
        `SELECT event.* 
        FROM event 
        INNER JOIN event_participants ON event.id = event_participants.event_id 
        INNER JOIN user ON event_participants.user_id = user.id 
        WHERE user_id = ? 
        ORDER BY eventDateTime ASC;`,
        [parent.id],
      );
    },
    async upcomingEvents(parent, _, { dbConnection }) {
      return await dbConnection.query(
        `SELECT event.* 
        FROM event 
        INNER JOIN event_participants ON event.id = event_participants.event_id 
        INNER JOIN user ON event_participants.user_id = user.id 
        WHERE user_id = ? 
        AND eventDateTime > now() 
        ORDER BY eventDateTime ASC;`,
        [parent.id],
      );
    },
    async pastEvents(parent, _, { dbConnection }) {
      return await dbConnection.query(
        `SELECT event.* 
        FROM event 
        INNER JOIN event_participants ON event.id = event_participants.event_id 
        INNER JOIN user ON event_participants.user_id = user.id 
        WHERE user_id = ? 
        AND eventDateTime < now() 
        UNION SELECT * 
        FROM event 
        WHERE creator_id = ? 
        AND eventDateTime < now() 
        ORDER BY eventDateTime ASC;`,
        [parent.id, parent.id],
      );
    },
    async createdEvents(parent, _, { dbConnection }) {
      return await dbConnection.query(
        `SELECT * FROM event WHERE creator_id = ? AND eventDateTime > now() ORDER BY eventDateTime ASC`,
        [parent.id],
      );
    },
    async circleCount(parent, _, { dbConnection }) {
      return (
        await dbConnection.query(
          `SELECT count(*) as count FROM circle_users WHERE user_id = ?`,
          [parent.id],
        )
      )[0].count;
    },
    async friendCount(parent, _, { dbConnection }) {
      return (
        await dbConnection.query(
          `SELECT count(*) AS count 
          FROM friends 
          INNER JOIN user ON friends.friend_id = user.id 
          WHERE user_id = ?`,
          [parent.id],
        )
      )[0].count;
    },
    async eventCount(parent, _, { dbConnection }) {
      return (
        await dbConnection.query(
          `SELECT COUNT(*) AS count 
          FROM user 
          INNER JOIN event_participants ON event_participants.user_id = user.id 
          WHERE user_id = ?`,
          [parent.id],
        )
      )[0].count;
    },
  },

  Event: {
    async creator(parent, _, { dbConnection }) {
      return (
        await dbConnection.query(`SELECT * FROM user WHERE id = ?`, [
          parent.creator_id,
        ])
      )[0];
    },
    async participants(parent, _, { dbConnection }) {
      let query = `SELECT * FROM user 
      INNER JOIN event_participants 
      ON user.id=event_participants.user_id 
      WHERE 1=1 
      AND event_participants.event_id = ? LIMIT 5`;

      return await dbConnection.query(query, [parent.id]);
    },
    async participantsCount(parent, _, { dbConnection }) {
      return (
        await dbConnection.query(
          `SELECT COUNT(*) AS count 
          FROM event_participants 
          WHERE event_id = ?`,
          [parent.id],
        )
      )[0].count;
    },
  },

  Invitation: {
    async event(parent, _, { dbConnection }) {
      return (
        await dbConnection.query(
          `SELECT DISTINCT * 
          FROM event 
          WHERE 1=1 
          AND event.id = ? 
          AND event.eventDateTime > now() 
          ORDER BY event.eventDateTime ASC`,
          [parent.id],
        )
      )[0];
    },
    async user({ userId }, _, { dbConnection }) {
      return (
        await dbConnection.query(`SELECT * FROM user WHERE id = ?`, [userId])
      )[0];
    },
  },

  Circle: {
    async owner(parent, _, { dbConnection }) {
      return (
        await dbConnection.query(`SELECT * FROM user WHERE id = ?`, [
          parent.owner,
        ])
      )[0];
    },
    async members(parent, _, { dbConnection }) {
      return await dbConnection.query(
        `SELECT user.* 
        FROM user 
        INNER JOIN circle_users 
        ON circle_users.user_id = user.id 
        WHERE circle_users.circle_id = ? LIMIT 5`,
        [parent.id],
      );
    },
  },

  FeedMessage: {
    async creator(parent, _, { dbConnection }) {
      return (
        await dbConnection.query(`SELECT * FROM user WHERE id = ?`, [
          parent.creator,
        ])
      )[0];
    },
  },
};
