import { isAuth } from '../../libs/auth';

export const eventFeed = async (
  _,
  { id },
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  isAuth(authorization);

  return await dbConnection.query('SELECT * FROM feed WHERE event_id = ?', [
    id,
  ]);
};
