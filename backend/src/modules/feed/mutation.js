import { isAuth } from '../../libs/auth';

export const addFeedMessage = async (
  _,
  { eventId, message },
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  const { id } = isAuth(authorization);

  const event = (
    await dbConnection.query('SELECT * FROM event WHERE id = ?', [eventId])
  )[0];

  if (event.creator_id !== id)
    throw new Error(
      'You have no permission to add feed messages for this event',
    );

  const dbResponse = await dbConnection.query(
    'INSERT INTO feed (event_id, message, creator) VALUES (?, ?, ?)',
    [eventId, message, id],
  );

  const eventParticipants = await dbConnection.query(
    'SELECT user_id FROM event_participants WHERE event_id = ?',
    [eventId],
  );

  const msg = `New feed message for event ${event.name}`;
  await Promise.all(
    eventParticipants.map(async (participant) => {
      await dbConnection.query(
        'INSERT INTO notification (user_id, message, type) VALUES (?, ?, "EVENT")',
        [participant.user_id, msg],
      );
    }),
  );

  const feedMessage = (
    await dbConnection.query('SELECT * FROM feed WHERE id = ?', [
      dbResponse.insertId,
    ])
  )[0];

  return feedMessage;
};
