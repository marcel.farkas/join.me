import { isAuth } from '../../libs/auth';

export const circleInvitations = async (
  _,
  __,
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  const { id } = isAuth(authorization);

  const result = await dbConnection.query(
    'SELECT * FROM circle_invitation WHERE user_id = ? AND accepted = 0',
    [id],
  );

  return result;
};
