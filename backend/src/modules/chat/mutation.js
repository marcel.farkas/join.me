import { isAuth } from '../../libs/auth';

export const notifyUsers = async (
  _,
  { id },
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  const { id: userId } = isAuth(authorization);

  const eventParticipants = await dbConnection.query(
    `SELECT DISTINCT user.* 
    FROM user 
    INNER JOIN event_participants ON user.id = event_participants.user_id 
    WHERE event_participants.event_id = ? 
    AND NOT event_participants.user_id = ?`,
    [id, userId],
  );

  const event = (
    await dbConnection.query('SELECT * FROM event WHERE event.id = ?', [id])
  )[0];

  const msg = `New message in event ${event.name}`;
  await Promise.all(
    eventParticipants.map(async (participant) => {
      await dbConnection.query(
        `INSERT INTO notification (user_id, message, type) VALUES (?, ?, ?)`,
        [participant.id, msg, 'CHAT'],
      );
    }),
  );

  return true;
};
