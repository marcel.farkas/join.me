import { isAuth } from '../../libs/auth';

export const declineInvitation = async (
  _,
  { eventId },
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  const { id } = isAuth(authorization);

  const event = (
    await dbConnection.query(`SELECT * FROM event WHERE id = ?`, [eventId])
  )[0];

  if (!event) throw new Error('Event does not exist');

  const alreadyParticipates = (
    await dbConnection.query(
      `SELECT event.* 
      FROM event 
      INNER JOIN event_participants ON event.id = event_participants.event_id 
      INNER JOIN user ON event_participants.user_id = user.id 
      WHERE event.id = ? 
      AND user.id = ?`,
      [eventId, id],
    )
  )[0];

  if (alreadyParticipates) {
    await dbConnection.query(
      `UPDATE event_invitations SET accepted = 1 WHERE event_id = ? AND user_id = ?`,
      [eventId, id],
    );
    throw new Error('You are already participating in this event');
  }

  await dbConnection.query(
    `DELETE FROM event_invitations WHERE event_id = ? AND user_id = ?`,
    [eventId, id],
  );

  return event;
};
