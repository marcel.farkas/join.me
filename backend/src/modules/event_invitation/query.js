import { isAuth } from '../../libs/auth';

export const invitations = async (
  _,
  __,
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  const { id } = isAuth(authorization);

  const result = await dbConnection.query(
    `SELECT DISTINCT event.*, user.id AS userId, event_invitations.accepted 
    FROM event 
    INNER JOIN event_invitations ON event.id = event_invitations.event_id 
    INNER JOIN user ON user.id = user_id 
    WHERE 1=1 
    AND event_invitations.accepted = 0 
    AND event_invitations.user_id = ?  
    AND event.eventDateTime > now() 
    ORDER BY event.eventDateTime ASC`,
    [id],
  );

  return result;
};
