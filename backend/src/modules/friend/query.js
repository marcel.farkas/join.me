import { isAuth } from '../../libs/auth';

export const friends = async (
  _,
  __,
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  const { id } = isAuth(authorization);

  return await dbConnection.query(
    'SELECT DISTINCT * FROM friends INNER JOIN user ON friends.friend_id = user.id WHERE user_id = ?',
    [id],
  );
};
