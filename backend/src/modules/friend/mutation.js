import { isAuth } from '../../libs/auth';

export const addFriend = async (
  _,
  { selectedUserId },
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  const { id } = isAuth(authorization);

  if (id == selectedUserId)
    throw new Error('You cannot add yourself to your list of contacts.');

  const user = await dbConnection.query(`SELECT * FROM user WHERE id = ?`, [
    selectedUserId,
  ]);

  if (!user) throw new Error('User does not exist');

  const alreadyFriend = (
    await dbConnection.query(
      `SELECT * FROM friends WHERE user_id = ? AND friend_id = ?`,
      [id, selectedUserId],
    )
  )[0];

  if (alreadyFriend) {
    throw new Error('You have already added this person to your contacts.');
  }

  await dbConnection.query(
    `INSERT INTO friends (user_id, friend_id) VALUES (?, ?)`,
    [id, selectedUserId],
  );

  return user.id;
};

export const removeFriend = async (
  _,
  { selectedUserId },
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  const { id } = isAuth(authorization);

  const user = await dbConnection.query(`SELECT * FROM user WHERE id = ?`, [
    selectedUserId,
  ]);

  if (!user) throw new Error('User does not exist');

  const notYetFriend = (
    await dbConnection.query(
      `SELECT * FROM friends WHERE user_id = ? AND friend_id = ?`,
      [id, selectedUserId],
    )
  )[0];

  if (!notYetFriend) {
    throw new Error('You have not yet added this person to your contacts.');
  }

  await dbConnection.query(
    `DELETE FROM friends WHERE user_id = ? AND friend_id = ?`,
    [id, selectedUserId],
  );

  return user.id;
};
