import { isAuth } from '../../libs/auth';

export const deleteNotification = async (
  _,
  { id },
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  const { id: userId } = isAuth(authorization);

  const deletedNotification = await dbConnection.query(
    `DELETE FROM notification WHERE id = ? AND user_id = ?`,
    [id, userId],
  );

  return deletedNotification.insertId;
};

export const deleteAllNotifications = async (
  _,
  __,
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  const { id } = isAuth(authorization);

  const deletedNotifications = await dbConnection.query(
    `DELETE FROM notification WHERE user_id = ?`,
    [id],
  );

  return deletedNotifications.affectedRows;
};
