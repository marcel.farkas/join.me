import { isAuth } from '../../libs/auth';

export const userNotifications = async (
  _,
  __,
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  const { id } = isAuth(authorization);

  return await dbConnection.query(
    'SELECT * FROM notification WHERE user_id = ?',
    id,
  );
};

export const notificationsCount = async (
  _,
  __,
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  const { id } = isAuth(authorization);

  return (
    await dbConnection.query(
      'SELECT COUNT(*) AS count FROM notification WHERE user_id = ?',
      id,
    )
  )[0].count;
};
