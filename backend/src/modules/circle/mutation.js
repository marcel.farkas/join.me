const crypto = require('crypto');

import { isAuth } from '../../libs/auth';

export const deleteCircle = async (
  _,
  { circleId },
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  const { id: userId } = isAuth(authorization);

  const circle = (
    await dbConnection.query('SELECT * FROM circle WHERE id = ?', [circleId])
  )[0];
  if (circle.owner !== userId)
    throw new Error('You can not delete circles that are not yours');

  await dbConnection.query(`DELETE FROM circle WHERE id = ?`, [circleId]);

  return true;
};

const addFriendsToCircle = async (
  dbConnection,
  selectedFriends,
  circleId,
  name,
) => {
  await Promise.all(
    selectedFriends.map(async (friend) => {
      await dbConnection.query(
        `INSERT IGNORE INTO circle_users (user_id, circle_id) VALUES (?, ?)`,
        [circleId, friend.id],
      );

      notifyUsers(name, friend, 'CIRCLE');
    }),
  );
};

const notifyUsers = async (dbConnection, msg, id, type) => {
  await dbConnection.query(
    'INSERT INTO notification (user_id, message, type) VALUES (?, ?, ?)',
    [id, msg, type],
  );
};

export const createCircle = async (
  _,
  { name, invitedFriends },
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  const { id: owner } = isAuth(authorization);

  if (!name.trim()) throw new Error('Please enter valid name');

  const dbResponse = await dbConnection.query(
    `INSERT INTO circle (name, owner) VALUES (?, ?)`,
    [name, owner],
  );

  await dbConnection.query(
    'INSERT INTO circle_users (user_id, circle_id) VALUES (?, ?)',
    [owner, dbResponse.insertId],
  );

  if (invitedFriends.length) {
    await Promise.all(
      invitedFriends.map(async (friendId) => {
        await dbConnection.query(
          'INSERT INTO circle_invitation (user_id, circle_id) VALUES(?, ?)',
          [friendId, dbResponse.insertId],
        );

        await notifyUsers(
          dbConnection,
          `You have been invited to join circle ${name}`,
          friendId,
          'CIRCLE',
        );
      }),
    );
  }

  return (
    await dbConnection.query('SELECT * FROM circle WHERE id = ?', [
      dbResponse.insertId,
    ])
  )[0];
  //await addFriendsToCircle(dbConnection, selectedFriends, id, name);
};

export const editCircle = async (
  _,
  { id, name, invitedFriends },
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  const { id: owner } = isAuth(authorization);

  const circle = (
    await dbConnection.query('SELECT * FROM circle WHERE id = ?', [id])
  )[0];

  if (circle.owner !== owner)
    throw new Error('You can not edit event that is not yours');

  await dbConnection.query(`UPDATE circle SET name = ? WHERE id = ?`, [
    name,
    id,
  ]);

  if (invitedFriends.length) {
    await Promise.all(
      invitedFriends.map(async (friendId) => {
        await dbConnection.query(
          'INSERT INTO circle_invitation (user_id, circle_id) VALUES(?, ?)',
          [friendId, id],
        );

        await notifyUsers(
          dbConnection,
          `You have been invited to join circle ${name}`,
          friendId,
          'CIRCLE',
        );
      }),
    );
  }

  return (
    await dbConnection.query('SELECT * FROM circle WHERE id = ?', [id])
  )[0];
  //await addFriendsToCircle(dbConnection, selectedFriends, id, name);
};
