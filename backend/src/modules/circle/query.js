import { isAuth } from '../../libs/auth';

export const circle = async (
  _,
  { circleId },
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  isAuth(authorization);

  return (
    await dbConnection.query('SELECT DISTINCT * FROM circle WHERE id = ?', [
      circleId,
    ])
  )[0];
};

export const circles = async (
  _,
  __,
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  const { id } = isAuth(authorization);

  return await dbConnection.query(
    'SELECT DISTINCT * FROM circle WHERE owner = ?',
    [id],
  );
};
