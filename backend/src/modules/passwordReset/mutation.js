const crypto = require('crypto');
const bcrypt = require('bcrypt');

import { sendEmail } from '../../libs/mail';

const saltRounds = 10;
const tokenValidityTime = 1000 * 60 * 60;

export const requestPasswordReset = async (_, { email }, { dbConnection }) => {
  const mail = email.toLowerCase();

  const user = (
    await dbConnection.query('SELECT * FROM user WHERE email = ?', [mail])
  )[0];

  if (!user) throw new Error('No user found with that email');

  await dbConnection.query(
    'DELETE FROM password_reset_token WHERE user_id = ?',
    [user.id],
  );

  const token = await getToken();

  await dbConnection.query(
    'INSERT INTO password_reset_token (user_id, token, token_expiry) VALUES (?, ?, ?);',
    [user.id, token.tokenHash, token.token_expiry],
  );

  await sendResetPasswordEmail(user, token.token);

  return {
    user_id: user.id,
  };
};

export const resetPassword = async (
  _,
  { id, password, confirmPassword, token },
  { dbConnection },
) => {
  if (password !== confirmPassword)
    throw new Error('Your passwords do not match');

  const user = (
    await dbConnection.query('SELECT * FROM user WHERE id = ?', [id])
  )[0];
  if (!user) throw new Error('No user found');

  const userToken = (
    await dbConnection.query(
      'SELECT * FROM password_reset_token WHERE user_id = ?',
      [user.id],
    )
  )[0];
  if (!userToken) throw new Error('No token found for this user');

  isTokenExpired(userToken);
  await isTokenValid(token, userToken);

  const passwordHash = await bcrypt.hash(password, saltRounds);
  await dbConnection.query('UPDATE user SET password = ? WHERE id = ?;', [
    passwordHash,
    user.id,
  ]);

  await dbConnection.query(
    'DELETE FROM password_reset_token WHERE user_id = ?',
    [user.id],
  );

  return user;
};

const isTokenValid = async (token, userToken) => {
  const validToken = await bcrypt.compare(token, userToken.token);
  if (!validToken) throw new Error('Your password token is invalid');
};

const isTokenExpired = (userToken) => {
  const tokenExpired = userToken.token_expiry <= Date.now() - tokenValidityTime;
  if (tokenExpired) throw new Error('Your reset password token has expired');
};

const getToken = async () => {
  const token = crypto.randomBytes(32).toString('hex');
  const tokenHash = await bcrypt.hash(token, saltRounds);
  const token_expiry = Date.now() + tokenValidityTime;
  return { token, tokenHash, token_expiry };
};

const sendResetPasswordEmail = async (user, token) => {
  const url = `${process.env.APP_URL}/reset?type=reset&user=${user.id}&token=${token}`;

  const htmlMessage = `
  <html lang="en" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
<head>
<title></title>
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<!--[if mso]><xml><o:OfficeDocumentSettings><o:PixelsPerInch>96</o:PixelsPerInch><o:AllowPNG/></o:OfficeDocumentSettings></xml><![endif]-->
<!--[if !mso]><!-->
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css"/>
<!--<![endif]-->
<style>
		* {
			box-sizing: border-box;
		}

		body {
			margin: 0;
			padding: 0;
		}

		a[x-apple-data-detectors] {
			color: inherit !important;
			text-decoration: inherit !important;
		}

		#MessageViewBody a {
			color: inherit;
			text-decoration: none;
		}

		p {
			line-height: inherit
		}

		@media (max-width:620px) {
			.icons-inner {
				text-align: center;
			}

			.icons-inner td {
				margin: 0 auto;
			}

			.row-content {
				width: 100% !important;
			}

			.image_block img.big {
				width: auto !important;
			}

			.stack .column {
				width: 100%;
				display: block;
			}
		}
	</style>
</head>
<body style="background-color: #e0e5eb; margin: 0; padding: 0; -webkit-text-size-adjust: none; text-size-adjust: none;">
<table border="0" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #e0e5eb;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-1" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #000000; width: 600px;" width="600">
<tbody>
<tr>
<td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 0px; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="100%">
<table border="0" cellpadding="0" cellspacing="0" class="image_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
<tr>
<td style="width:100%;padding-right:0px;padding-left:0px;">
<div align="center" style="line-height:10px"><img alt="Top background" class="big" src="https://sc185432storageacc.blob.core.windows.net/public/40c07ec7-f513-4f3a-88ad-9ed70109a437.png" style="display: block; height: auto; border: 0; width: 600px; max-width: 100%;" title="Top Background" width="600"/></div>
</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-2" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #6720b0; background-image: url('https://sc185432storageacc.blob.core.windows.net/public/d4160a6c-a9bb-4077-8dcc-a9f359ccc393.png'); background-position: top center; background-repeat: no-repeat; color: #000000; width: 600px;" width="600">
<tbody>
<tr>
<td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-top: 0px; padding-bottom: 35px; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="100%">
<table border="0" cellpadding="10" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
<tr>
<td>
<div style="font-family: 'Trebuchet MS', Tahoma, sans-serif">
<div style="font-size: 14px; font-family: 'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif; mso-line-height-alt: 16.8px; color: #e03add; line-height: 1.2;">
<p style="margin: 0; font-size: 14px; text-align: center;"><span style="font-size:26px;"><strong>JOIN<span style="font-size:8px;"> </span>.<span style="font-size:8px;"> </span>ME</strong></span></p>
</div>
</div>
</td>
</tr>
</table>
<table border="0" cellpadding="10" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
<tr>
<td>
<div style="font-family: Tahoma, Verdana, sans-serif">
<div style="font-size: 12px; font-family: 'Roboto', Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 14.399999999999999px; color: #ffffff; line-height: 1.2;">
<p style="margin: 0; font-size: 14px; text-align: center;"><span style="font-size:16px;">Hi ${user.first_name},</span></p>
</div>
</div>
</td>
</tr>
</table>
<table border="0" cellpadding="10" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
<tr>
<td>
<div style="font-family: 'Trebuchet MS', Tahoma, sans-serif">
<div style="font-size: 12px; font-family: 'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif; mso-line-height-alt: 14.399999999999999px; color: #e03add; line-height: 1.2;">
<p style="margin: 0; font-size: 14px; text-align: center;"><span style="font-size:26px;"><strong><span style="color:#e03add;">Forgot Your Password?</span></strong></span></p>
<p style="margin: 0; font-size: 14px; text-align: center; mso-line-height-alt: 14.399999999999999px;"> </p>
</div>
</div>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="image_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
<tr>
<td style="width:100%;padding-right:0px;padding-left:0px;">
<div align="center" style="line-height:10px"><img alt="Zamek" src="https://sc185432storageacc.blob.core.windows.net/public/zamek.png" style="display: block; height: auto; border: 0; width: 150px; max-width: 100%;" title="Zamek title" width="150"/></div>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
<tr>
<td style="padding-bottom:10px;padding-left:10px;padding-right:10px;padding-top:20px;">
<div style="font-family: Tahoma, Verdana, sans-serif">
<div style="font-size: 12px; mso-line-height-alt: 14.399999999999999px; color: #ffffff; line-height: 1.2; font-family: 'Roboto', Tahoma, Verdana, Segoe, sans-serif;">
<p style="margin: 0; font-size: 12px; mso-line-height-alt: 14.399999999999999px;"> </p>
</div>
</div>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
<tr>
<td style="padding-bottom:20px;padding-left:25px;padding-right:25px;padding-top:10px;">
<div style="font-family: Tahoma, Verdana, sans-serif">
<div style="font-size: 12px; font-family: 'Roboto', Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 18px; color: #555555; line-height: 1.5;">
<p style="margin: 0; font-size: 14px; text-align: center; mso-line-height-alt: 24px;"><span style="color:#ffffff;font-size:16px;">You requested your password to be reset.<br/>To reset your password click the link below.<br/></span></p>
</div>
</div>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="button_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
<tr>
<td style="padding-bottom:20px;padding-left:10px;padding-right:10px;padding-top:10px;text-align:center;">
<div align="center">
<!--[if mso]><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" style="height:62px;width:276px;v-text-anchor:middle;" arcsize="18%" stroke="false" fillcolor="#e03add"><w:anchorlock/><v:textbox inset="0px,0px,0px,0px"><center style="color:#ffffff; font-family:'Trebuchet MS', Tahoma, sans-serif; font-size:24px"><![endif]-->
<a href='${url}' style="text-decoration:none;display:inline-block;color:#ffffff;background-color:#e03add;border-radius:11px;width:auto;border-top:0px solid #2B79A6;border-right:0px solid #2B79A6;border-bottom:4px solid #B93CC0;border-left:0px solid #2B79A6;padding-top:5px;padding-bottom:5px;font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;text-align:center;mso-border-alt:none;word-break:keep-all;"><span style="padding-left:20px;padding-right:20px;font-size:24px;display:inline-block;letter-spacing:normal;"><span style="font-size: 16px; line-height: 2; word-break: break-word; mso-line-height-alt: 32px;"><strong><span data-mce-style="font-size: 24px; line-height: 48px;" style="font-size: 24px; line-height: 48px;">RESET PASSWORD<br/></span></strong></span></span></a>
<!--[if mso]></center></v:textbox></v:roundrect><![endif]-->
</div>
</td>
</tr>
</table>
<table border="0" cellpadding="10" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
<tr>
<td>
<div style="font-family: sans-serif">
<div style="font-size: 14px; mso-line-height-alt: 16.8px; color: #555555; line-height: 1.2; font-family: 'Roboto', Tahoma, Verdana, Segoe, sans-serif;">
<p style="margin: 0; font-size: 14px; text-align: center; mso-line-height-alt: 16.8px;"> </p>
<p style="margin: 0; font-size: 14px; text-align: center;"><span style="color:#ffffff;">Join.me ©2021</span></p>
</div>
</div>
</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-3" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
<tbody>
<tr>
<td>
<table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #000000; width: 600px;" width="600">
<tbody>
<tr>
<td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-top: 0px; padding-bottom: 5px; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="100%">
<table border="0" cellpadding="0" cellspacing="0" class="image_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
<tr>
<td style="width:100%;padding-right:0px;padding-left:0px;">
<div align="right" style="line-height:10px"><img alt="Bottom background" class="big" src="https://sc185432storageacc.blob.core.windows.net/public/be3f3747-6a4e-4ef0-a162-5f5e43101d8d.png" style="display: block; height: auto; border: 0; width: 600px; max-width: 100%;" title="Bottom background" width="600"/></div>
</td>
</tr>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<!-- End -->
</body>
</html>`;

  await sendEmail({
    receiver: user.email,
    subject: 'Password reset',
    htmlMessage,
  });
};
