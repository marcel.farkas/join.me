import { isAuth } from '../../libs/auth';

export const events = async (
  _,
  __,
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  isAuth(authorization);

  return await dbConnection.query(
    'SELECT DISTINCT * FROM event ORDER BY eventDateTime ASC',
  );
};

export const upcomingEvents = async (
  _,
  __,
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  isAuth(authorization);

  return await dbConnection.query(
    'SELECT DISTINCT * FROM event WHERE private = false AND eventDateTime > now() ORDER BY eventDateTime ASC',
  );
};

export const event = async (
  _,
  { id },
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  isAuth(authorization);

  return (
    await dbConnection.query('SELECT DISTINCT * FROM event WHERE id = ?', [id])
  )[0];
};
