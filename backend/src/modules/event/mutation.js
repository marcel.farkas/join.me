const crypto = require('crypto');

import { isAuth } from '../../libs/auth';

const inviteFriendsToEvent = async (
  dbConnection,
  selectedFriends,
  eventId,
  name,
) => {
  //invite specified friends as event participants
  await Promise.all(
    selectedFriends.map(async (friend) => {
      await dbConnection.query(
        `INSERT IGNORE INTO event_invitations (event_id, user_id, accepted) VALUES (?, ?, 0)`,
        [eventId, friend.id],
      );

      notifyUsers(name, friend, 'EVENT');
    }),
  );
};

const notifyUsers = async (dbConnection, name, friend, type) => {
  //send notification to invited friends
  const msg = `You have been invited to join event ${name}`;
  await dbConnection.query(
    'INSERT INTO notification (user_id, message, type) VALUES (?, ?, ?)',
    [friend.id, msg, type],
  );
};

const isParticipatingInEvent = async (dbConnection, eventId, userId) => {
  const result = (
    await dbConnection.query(
      `SELECT event.* 
      FROM event 
      INNER JOIN event_participants ON event.id = event_participants.event_id 
      INNER JOIN user ON event_participants.user_id = user.id 
      WHERE event.id = ? 
      AND user.id = ?`,
      [eventId, userId],
    )
  )[0];
  return result ? true : false;
};

export const createEvent = async (
  _,
  {
    place,
    name,
    isPrivate,
    participantsLimit,
    eventDateTime,
    eventPictureUrl,
    information,
    selectedCircles,
    selectedFriends,
  },
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  const { id: creator_id } = isAuth(authorization);

  if (!place.trim()) throw new Error('Please enter valid event place');
  if (!name.trim()) throw new Error('Please enter valid event name');
  if (isPrivate === undefined)
    throw new Error('Please select either private or public event');
  if (!eventDateTime) throw new Error('Please enter event date and time');
  if (Date.parse(eventDateTime) < Date.now() + 1000 * 2)
    throw new Error('Event date and time can not be set in the past');

  const chatroom_id = crypto.randomBytes(32).toString('hex');

  let updateCols = [
    'place',
    'name',
    'private',
    'eventDateTime',
    'creator_id',
    'chatroom_id',
  ];
  let updateVals = [
    place,
    name,
    isPrivate,
    eventDateTime,
    creator_id,
    chatroom_id,
  ];

  if (participantsLimit) {
    updateCols.push('participantsLimit');
    updateVals.push(participantsLimit);
  }

  if (eventPictureUrl) {
    updateCols.push('eventPictureUrl');
    updateVals.push(eventPictureUrl);
  }

  if (information) {
    updateCols.push('information');
    updateVals.push(information);
  }

  const dbResponse = await dbConnection.query(
    `INSERT INTO event (${updateCols.join(', ')}) VALUES (${updateVals
      .map(() => '?')
      .join(', ')})`,
    [...updateVals],
  );

  const result = await dbConnection.query(`SELECT * FROM event WHERE id = ?`, [
    dbResponse.insertId,
  ]);

  //creator of event joins event as event participant
  await dbConnection.query(
    `INSERT INTO event_participants (event_id, user_id) VALUES (?, ?)`,
    [result[0].id, creator_id],
  );

  await inviteFriendsToEvent(dbConnection, selectedFriends, result[0].id, name);

  //invite users from specified circles as event participants
  await Promise.all(
    selectedCircles.map(async (circle) => {
      await dbConnection.query(
        `INSERT IGNORE INTO event_invitations (user_id, event_id, accepted)
      SELECT circle_users.user_id, ?, 0
      FROM circle_users 
      WHERE circle_id = ?
      AND user_id <> ?`,
        [dbResponse.insertId, circle.id, creator_id],
      );
    }),
  );

  return result[0];
};

export const deleteEvent = async (
  _,
  { eventId },
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  const { id: userId } = isAuth(authorization);

  const event = (
    await dbConnection.query('SELECT * FROM event WHERE id = ?', [eventId])
  )[0];
  if (event.creator_id !== userId)
    throw new Error('You can not delete events that are not yours');

  await dbConnection.query(`DELETE FROM event WHERE id = ?`, [eventId]);

  return true;
};

export const joinEvent = async (
  _,
  { eventId },
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  const { id } = isAuth(authorization);

  const event = (
    await dbConnection.query(`SELECT * FROM event WHERE id = ?`, [eventId])
  )[0];

  if (!event) throw new Error('Event does not exist');

  const isParticipating = await isParticipatingInEvent(
    dbConnection,
    eventId,
    id,
  );

  const isCreator = await dbConnection.query(
    `SELECT creator_id FROM event WHERE id = ?`,
    [eventId],
  );

  if (isParticipating) {
    await dbConnection.query(
      `UPDATE event_invitations SET accepted = 1 WHERE event_id = ? AND user_id = ?`,
      [eventId, id],
    );
    throw new Error('You already joined this event');
  }

  const participantsCount = (
    await dbConnection.query(
      'SELECT COUNT(*) AS count FROM event_participants WHERE event_id = ?',
      [eventId],
    )
  )[0].count;

  if (event.participantsLimit && participantsCount >= event.participantsLimit)
    throw new Error('Event is already full');

  if (!event.private) {
    await dbConnection.query(
      `INSERT INTO event_participants (event_id, user_id) VALUES (?, ?)`,
      [eventId, id],
    );
    await dbConnection.query(
      `UPDATE event_invitations SET accepted = 1 WHERE event_id = ? AND user_id = ?`,
      [eventId, id],
    );
  } else if (isCreator) {
    await dbConnection.query(
      `INSERT INTO event_participants (event_id, user_id) VALUES (?, ?)`,
      [eventId, id],
    );
  }

  return event;
};

export const leaveEvent = async (
  _,
  { eventId },
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  const { id } = isAuth(authorization);

  const event = (
    await dbConnection.query(`SELECT * FROM event WHERE id = ?`, [eventId])
  )[0];

  if (!event) throw new Error('Event does not exist');

  const isParticipating = await isParticipatingInEvent(
    dbConnection,
    eventId,
    id,
  );

  if (!isParticipating)
    throw new Error('You are not participating in this event');

  await dbConnection.query(
    `DELETE FROM event_participants WHERE event_id = ? AND user_id = ?`,
    [eventId, id],
  );

  return event;
};

export const editEvent = async (
  _,
  {
    id,
    place,
    name,
    isPrivate,
    participantsLimit,
    eventDateTime,
    eventPictureUrl,
    information,
    selectedCircles,
    selectedFriends,
  },
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  const { id: creatorId } = isAuth(authorization);

  if (!place.trim()) throw new Error('Please enter event place');
  if (!name.trim()) throw new Error('Please enter event name');
  if (isPrivate === undefined)
    throw new Error('Please select either private or public event');
  if (!eventDateTime) throw new Error('Please enter event date and time');
  if (Date.parse(eventDateTime) < Date.now() + 1000 * 2)
    throw new Error('Event date and time can not be set in the past');

  const chatroom_id = crypto.randomBytes(32).toString('hex');

  let updateCols = [
    'place = ?',
    'name = ?',
    'private = ?',
    'eventDateTime = ?',
    'creator_id = ?',
    'chatroom_id = ?',
  ];
  let updateVals = [
    place,
    name,
    isPrivate,
    eventDateTime,
    creatorId,
    chatroom_id,
  ];

  if (participantsLimit) {
    updateCols.push('participantsLimit = ?');
    updateVals.push(participantsLimit);
  }

  if (eventPictureUrl) {
    updateCols.push('eventPictureUrl = ?');
    updateVals.push(eventPictureUrl);
  }

  if (information) {
    updateCols.push('information = ?');
    updateVals.push(information);
  }

  await dbConnection.query(
    `UPDATE event SET ${updateCols.join(', ')} WHERE id = ?`,
    [...updateVals, id],
  );

  await inviteFriendsToEvent(dbConnection, selectedFriends, id, name);

  //invite users from specified circles as event participants
  await Promise.all(
    selectedCircles.map(async (circle) => {
      await dbConnection.query(
        `INSERT IGNORE INTO event_invitations (user_id, event_id, accepted)
      SELECT circle_users.user_id, ?, 0
      FROM circle_users 
      WHERE circle_id = ?`,
        [dbResponse.insertId, circle.id],
      );
    }),
  );

  return (
    await dbConnection.query('SELECT * FROM event WHERE id = ?', [id])
  )[0];
};
