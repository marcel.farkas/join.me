import { isAuth } from '../../libs/auth';

export const users = async (
  _,
  { ids },
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  isAuth(authorization);

  let query = 'SELECT * FROM user';

  if (ids) {
    query += ` WHERE id IN (?)`;
  }
  return await dbConnection.query(query, [ids]);
};

export const user = async (
  _,
  __,
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  const { id } = isAuth(authorization);

  const user = (
    await dbConnection.query('SELECT * FROM user WHERE id = ?', [id])
  )[0];

  if (!user) throw new Error('No user found with this id');

  return user;
};
