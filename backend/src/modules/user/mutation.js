const bcrypt = require('bcrypt');
const crypto = require('crypto');

import { createToken } from '../../libs/token';
import { isAuth } from '../../libs/auth';
import { sendEmail } from '../../libs/mail';

const saltRounds = 10;
const tokenValidityTime = 1000 * 60 * 60;

export const signin = async (_, { email, password }, { dbConnection }) => {
  const dbResponse = await dbConnection.query(
    'SELECT * FROM user WHERE email = ?',
    [email],
  );

  const user = dbResponse[0];

  if (!user) throw new Error('Email or password is incorrect');

  if (await bcrypt.compare(password, user.password)) {
    if (!user.verified) throw new Error('You need to verify your e-mail first');

    const token = createToken({ id: user.id });

    return {
      user: { ...user },
      token,
    };
  } else {
    throw new Error('Email or password is incorrect');
  }
};

export const signup = async (
  _,
  { first_name, last_name, email, password, confirmPassword, business },
  { dbConnection },
) => {
  const userByEmail = (
    await dbConnection.query('SELECT * FROM user WHERE email = ?', [email])
  )[0];

  if (userByEmail) throw new Error('Email already registered');

  const pwdRegex = new RegExp('(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}');
  if (!pwdRegex.test(password))
    throw new Error(
      `Password must be at least 8 characters long. 
      It must contain a lowercase character, an uppercase character and a number`,
    );

  if (password !== confirmPassword)
    throw new Error('Your passwords do not match');

  const passwordHash = await bcrypt.hash(password, saltRounds);

  const dbUserResponse = await dbConnection.query(
    'INSERT INTO user (first_name, last_name, email, password, business) VALUES (?, ?, ?, ?, ?);',
    [first_name, last_name, email, passwordHash, business],
  );

  const token = crypto.randomBytes(32).toString('hex');
  const tokenHash = await bcrypt.hash(token, saltRounds);
  const token_expiry = Date.now() + tokenValidityTime;

  await dbConnection.query(
    'INSERT INTO email_verification_token (user_id, token, token_expiry) VALUES (?, ?, ?);',
    [dbUserResponse.insertId, tokenHash, token_expiry],
  );

  const url = `${process.env.APP_URL}/confirm?user=${dbUserResponse.insertId}&token=${token}`;

  const htmlMessage = `
  <html lang="en" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
  <head>
  <title></title>
  <meta charset="utf-8"/>
  <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
  <!--[if mso]><xml><o:OfficeDocumentSettings><o:PixelsPerInch>96</o:PixelsPerInch><o:AllowPNG/></o:OfficeDocumentSettings></xml><![endif]-->
  <!--[if !mso]><!-->
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css"/>
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css"/>
  <!--<![endif]-->
  <style>
      * {
        box-sizing: border-box;
      }
  
      body {
        margin: 0;
        padding: 0;
      }
  
      a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: inherit !important;
      }
  
      #MessageViewBody a {
        color: inherit;
        text-decoration: none;
      }
  
      p {
        line-height: inherit
      }
  
      @media (max-width:620px) {
        .icons-inner {
          text-align: center;
        }
  
        .icons-inner td {
          margin: 0 auto;
        }
  
        .row-content {
          width: 100% !important;
        }
  
        .image_block img.big {
          width: auto !important;
        }
  
        .stack .column {
          width: 100%;
          display: block;
        }
      }
    </style>
  </head>
  <body style="background-color: #e0e5eb; margin: 0; padding: 0; -webkit-text-size-adjust: none; text-size-adjust: none;">
  <table border="0" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #e0e5eb;" width="100%">
  <tbody>
  <tr>
  <td>
  <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-1" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
  <tbody>
  <tr>
  <td>
  <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #000000; width: 600px;" width="600">
  <tbody>
  <tr>
  <td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-top: 5px; padding-bottom: 0px; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="100%">
  <table border="0" cellpadding="0" cellspacing="0" class="image_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
  <tr>
  <td style="width:100%;padding-right:0px;padding-left:0px;">
  <div align="center" style="line-height:10px"><img alt="Top background" class="big" src="https://sc185432storageacc.blob.core.windows.net/public/40c07ec7-f513-4f3a-88ad-9ed70109a437.png" style="display: block; height: auto; border: 0; width: 600px; max-width: 100%;" title="Top background" width="600"/></div>
  </td>
  </tr>
  </table>
  </td>
  </tr>
  </tbody>
  </table>
  </td>
  </tr>
  </tbody>
  </table>
  <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-2" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
  <tbody>
  <tr>
  <td>
  <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #6720b0; background-image: url('https://sc185432storageacc.blob.core.windows.net/public/d4160a6c-a9bb-4077-8dcc-a9f359ccc393.png'); background-position: top center; background-repeat: no-repeat; color: #000000; width: 600px;" width="600">
  <tbody>
  <tr>
  <td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-top: 0px; padding-bottom: 35px; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="100%">
  <table border="0" cellpadding="10" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
  <tr>
  <td>
  <div style="font-family: 'Trebuchet MS', Tahoma, sans-serif">
  <div style="font-size: 12px; font-family: 'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif; mso-line-height-alt: 14.399999999999999px; color: #ffffff; line-height: 1.2;">
  <p style="margin: 0; font-size: 14px; text-align: center;"><span style="font-size:26px;"><strong><span style="color:#e03add;">JOIN<span style="font-size:8px;"> </span>.<span style="font-size:8px;"> </span>ME</span></strong></span></p>
  </div>
  </div>
  </td>
  </tr>
  </table>
  <table border="0" cellpadding="10" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
  <tr>
  <td>
  <div style="font-family: Tahoma, Verdana, sans-serif">
  <div style="font-size: 12px; font-family: 'Roboto', Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 14.399999999999999px; color: #ffffff; line-height: 1.2;">
  <p style="margin: 0; font-size: 14px; text-align: center;"><span style="font-size:16px;">Hi ${first_name},</span></p>
  </div>
  </div>
  </td>
  </tr>
  </table>
  <table border="0" cellpadding="10" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
  <tr>
  <td>
  <div style="font-family: 'Trebuchet MS', Tahoma, sans-serif">
  <div style="font-size: 12px; font-family: 'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif; mso-line-height-alt: 14.399999999999999px; color: #ffffff; line-height: 1.2;">
  <p style="margin: 0; font-size: 14px; text-align: center;"><strong><span style="font-size:26px;"><span style="color:#e03add;"> Activate Your Account Now ! </span></span></strong></p>
  <p style="margin: 0; font-size: 14px; text-align: center; mso-line-height-alt: 14.399999999999999px;"> </p>
  </div>
  </div>
  </td>
  </tr>
  </table>
  <table border="0" cellpadding="0" cellspacing="0" class="image_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
  <tr>
  <td style="width:100%;padding-right:0px;padding-left:0px;">
  <div align="center" style="line-height:10px"><img alt="Letter" src="https://sc185432storageacc.blob.core.windows.net/public/email2.png" style="display: block; height: auto; border: 0; width: 150px; max-width: 100%;" title="Letter" width="150"/></div>
  </td>
  </tr>
  </table>
  <table border="0" cellpadding="0" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
  <tr>
  <td style="padding-bottom:20px;padding-left:25px;padding-right:25px;padding-top:10px;">
  <div style="font-family: Tahoma, Verdana, sans-serif">
  <div style="font-size: 12px; font-family: 'Roboto', Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 18px; color: #555555; line-height: 1.5;">
  <p style="margin: 0; font-size: 14px; text-align: center; mso-line-height-alt: 18px;"> </p>
  <p style="margin: 0; font-size: 14px; text-align: center; mso-line-height-alt: 24px;"><span style="color:#ffffff;font-size:16px;">Your account is almost ready.</span></p>
  <p style="margin: 0; font-size: 14px; text-align: center; mso-line-height-alt: 24px;"><span style="color:#ffffff;font-size:16px;">To activate your account click the link below. </span></p>
  </div>
  </div>
  </td>
  </tr>
  </table>
  <table border="0" cellpadding="0" cellspacing="0" class="button_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
  <tr>
  <td style="padding-bottom:20px;padding-left:10px;padding-right:10px;padding-top:10px;text-align:center;">
  <div align="center">
  <!--[if mso]><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" style="height:62px;width:293px;v-text-anchor:middle;" arcsize="18%" stroke="false" fillcolor="#e03add"><w:anchorlock/><v:textbox inset="0px,0px,0px,0px"><center style="color:#ffffff; font-family:'Trebuchet MS', Tahoma, sans-serif; font-size:24px"><![endif]-->
  <a href='${url}' style="text-decoration:none;display:inline-block;color:#ffffff;background-color:#e03add;border-radius:11px;width:auto;border-top:0px solid #2B79A6;border-right:0px solid #2B79A6;border-bottom:4px solid #B93CC0;border-left:0px solid #2B79A6;padding-top:5px;padding-bottom:5px;font-family:'Montserrat', 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif;text-align:center;mso-border-alt:none;word-break:keep-all;"><span style="padding-left:20px;padding-right:20px;font-size:24px;display:inline-block;letter-spacing:normal;"><span style="font-size: 16px; line-height: 2; word-break: break-word; mso-line-height-alt: 32px;"><strong><span data-mce-style="font-size: 24px; line-height: 48px;" style="font-size: 24px; line-height: 48px;">ACTIVATE ACCOUNT <br/></span></strong></span></span></a>
  <!--[if mso]></center></v:textbox></v:roundrect><![endif]-->
  </div>
  </td>
  </tr>
  </table>
  <table border="0" cellpadding="10" cellspacing="0" class="text_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; word-break: break-word;" width="100%">
  <tr>
  <td>
  <div style="font-family: Tahoma, Verdana, sans-serif">
  <div style="font-size: 12px; font-family: 'Roboto', Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 14.399999999999999px; color: #ffffff; line-height: 1.2;">
  <p style="margin: 0; font-size: 14px; text-align: center; mso-line-height-alt: 14.399999999999999px;"> </p>
  <p style="margin: 0; font-size: 14px; text-align: center;">Join.me ©2021</p>
  </div>
  </div>
  </td>
  </tr>
  </table>
  </td>
  </tr>
  </tbody>
  </table>
  </td>
  </tr>
  </tbody>
  </table>
  <table align="center" border="0" cellpadding="0" cellspacing="0" class="row row-3" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
  <tbody>
  <tr>
  <td>
  <table align="center" border="0" cellpadding="0" cellspacing="0" class="row-content stack" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #000000; width: 600px;" width="600">
  <tbody>
  <tr>
  <td class="column" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-weight: 400; text-align: left; vertical-align: top; padding-top: 0px; padding-bottom: 5px; border-top: 0px; border-right: 0px; border-bottom: 0px; border-left: 0px;" width="100%">
  <table border="0" cellpadding="0" cellspacing="0" class="image_block" role="presentation" style="mso-table-lspace: 0pt; mso-table-rspace: 0pt;" width="100%">
  <tr>
  <td style="width:100%;padding-right:0px;padding-left:0px;">
  <div align="right" style="line-height:10px"><img alt="Bottom background" class="big" src="https://sc185432storageacc.blob.core.windows.net/public/be3f3747-6a4e-4ef0-a162-5f5e43101d8d.png" style="display: block; height: auto; border: 0; width: 600px; max-width: 100%;" title="Bottom background" width="600"/></div>
  </td>
  </tr>
  </table>
  </td>
  </tr>
  </tbody>
  </table>
  </td>
  </tr>
  </tbody>
  </table>
  <!-- End -->
  </body>
  </html>
  `;

  await sendEmail({ receiver: email, subject: 'Confirm e-mail', htmlMessage });

  const userObject = {
    id: dbUserResponse.insertId,
    first_name,
    last_name,
    email,
    business,
  };

  return { user: userObject, token };
};

export const confirmEmail = async (_, { id, token }, { dbConnection }) => {
  const user = (
    await dbConnection.query('SELECT * FROM user WHERE id = ?', [id])
  )[0];
  if (!user) throw new Error('No user found');

  if (user.verified) throw new Error('Email has already been confirmed');

  const userToken = (
    await dbConnection.query(
      'SELECT * FROM email_verification_token WHERE user_id = ?',
      [id],
    )
  )[0];

  if (!userToken) throw new Error('No token found for this user');

  const tokenExpired = userToken.token_expiry <= Date.now() - tokenValidityTime;
  if (tokenExpired) throw new Error('Your token has expired');

  const validToken = await bcrypt.compare(token, userToken.token);
  if (!userToken || !validToken) throw new Error('Your token is not valid');

  await dbConnection.query('UPDATE user SET verified = 1 WHERE id = ?;', [id]);

  await dbConnection.query(
    'DELETE FROM email_verification_token WHERE user_id = ?',
    [id],
  );

  return user;
};

export const updateProfile = async (
  _,
  { first_name, last_name, email, description, profilePictureUrl },
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  const { id: userId } = isAuth(authorization);

  if (!first_name || !last_name) throw new Error('Please enter your full name');
  if (!email) throw new Error('Please enter valid email');

  const userById = (
    await dbConnection.query('SELECT * FROM user WHERE id = ?', [userId])
  )[0];

  if (!userById) throw new Error('User not in database');

  let updateCols = [];
  let updateVals = [];

  if (userById.email !== email) {
    const anyUserByEmail = (
      await dbConnection.query('SELECT * FROM user WHERE email = ?', [email])
    )[0];

    if (anyUserByEmail) throw new Error('User with this email already exists');

    updateCols.push('email = ?');
    updateVals.push(email);
  }

  if (profilePictureUrl && profilePictureUrl !== userById.profilePictureUrl) {
    updateCols.push('profilePictureUrl = ?');
    updateVals.push(profilePictureUrl);
  }
  if (first_name && first_name !== userById.first_name) {
    updateCols.push('first_name = ?');
    updateVals.push(first_name);
  }
  if (last_name && last_name !== userById.last_name) {
    updateCols.push('last_name = ?');
    updateVals.push(last_name);
  }
  if (description && description !== userById.description) {
    updateCols.push('description = ?');
    updateVals.push(description);
  }

  if (!updateCols.length) throw new Error('No new changes were made');

  const query = `UPDATE user SET ${updateCols.join(', ')} WHERE id = ?;`;
  await dbConnection.query(query, [...updateVals, userId]);

  return userById;
};

export const changePassword = async (
  _,
  { currPassword, newPassword, newPasswordConfirm },
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  const { id: userId } = isAuth(authorization);

  if (newPassword !== newPasswordConfirm)
    throw new Error('Passwords must match');

  const pwdRegex = new RegExp('(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}');
  if (!pwdRegex.test(newPassword))
    throw new Error(
      `Password must be at least 8 characters long. 
      It must contain a lowercase character, an uppercase character and a number`,
    );

  const userById = (
    await dbConnection.query('SELECT * FROM user WHERE id = ?', [userId])
  )[0];

  if (!userById) throw new Error('No user found');

  if (await bcrypt.compare(currPassword, userById.password)) {
    const newPasswordHash = await bcrypt.hash(newPassword, saltRounds);
    await dbConnection.query('UPDATE user SET password = ? WHERE id = ?;', [
      newPasswordHash,
      userId,
    ]);

    return userById;
  } else {
    throw new Error('Password is incorrect');
  }
};
