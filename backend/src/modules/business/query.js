import { isAuth } from '../../libs/auth';

export const business = async (
  _,
  { id },
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  isAuth(authorization);

  return (
    await dbConnection.query('SELECT * FROM business WHERE id = ?', [id])
  )[0];
};

export const businesses = async (
  _,
  __,
  {
    dbConnection,
    req: {
      headers: { authorization },
    },
  },
) => {
  isAuth(authorization);

  return await dbConnection.query('SELECT * FROM business');
};
