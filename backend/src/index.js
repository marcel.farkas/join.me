import dotenv from 'dotenv-flow';
import express from 'express';
import cors from 'cors';
import { ApolloServer, gql } from 'apollo-server-express';
import { ApolloServerPluginLandingPageGraphQLPlayground } from 'apollo-server-core';

import { getConnection } from './libs/connection';

import rootResolver from './modules/rootResolver';

dotenv.config();

const typeDefs = gql`
  scalar DATETIME

  type User {
    id: Int!
    first_name: String
    last_name: String
    email: String
    business: Boolean
    created_at: String
    verified: Boolean
    description: String
    profilePictureUrl: String
    friends: [User!]!
    joinedEvents: [Event!]!
    upcomingEvents: [Event!]!
    pastEvents: [Event!]!
    createdEvents: [Event!]!
    circleCount: Int!
    friendCount: Int!
    eventCount: Int!
  }

  type Circle {
    id: Int!
    name: String
    owner: User!
    members: [User!]!
    created_at: String
  }

  type Business {
    id: Int!
    name: String!
    ico: Int
    phone: String
    email: String
    address: String
    owner: Int
    created_at: String
  }

  type ResetPasswordToken {
    user_id: Int!
    token: String!
    token_expiry: String
  }

  type AuthUser {
    id: Int!
    first_name: String
    last_name: String
    email: String
    business: Boolean
  }

  type AuthInfo {
    user: AuthUser!
    token: String!
  }

  type Event {
    id: Int!
    creator_id: Int
    created_at: String
    participantsLimit: Int
    participantsCount: Int
    name: String!
    eventDateTime: String
    information: String
    place: String
    private: Boolean
    creator: User
    chatroom_id: String
    participants(limit: Int): [User!]!
    eventPictureUrl: String
  }

  type Invitation {
    event: Event!
    user: User!
    accepted: Boolean!
  }

  type CircleInvitation {
    id: Int!
    user_id: Int
    circle_id: Int
    accepted: Int
  }

  type Notification {
    id: Int!
    user_id: Int!
    message: String!
    type: String!
    created_at: String
  }

  type FeedMessage {
    id: Int!
    event_id: Int!
    message: String!
    created_at: DATETIME!
    creator: User!
  }

  type Query {
    user: User
    users(ids: [Int]): [User!]!
    event(id: Int!): Event
    upcomingEvents: [Event!]!
    events: [Event!]!
    eventCount: Int
    joinedEvents: [Event!]!
    pastEvents: [Event!]!
    createdEvents: [Event!]!
    circleCount: Int
    circle(circleId: Int!): Circle
    circles: [Circle!]!
    userCircles: [Circle!]!
    friendCount: Int
    friends: [User!]!
    business(id: Int!): Business!
    businesses: [Business!]!
    invitations: [Invitation!]!
    userNotifications: [Notification!]!
    notificationsCount: Int!
    eventFeed(id: Int!): [FeedMessage!]!
    circleInvitations: [CircleInvitation!]!
  }

  input InputUser {
    id: Int!
    first_name: String
    last_name: String
    email: String
    business: Boolean
    created_at: String
    verified: Boolean
    description: String
    profilePictureUrl: String
  }

  input InputCircle {
    id: Int!
    name: String
    created_at: String
  }

  type Mutation {
    signin(email: String!, password: String!): AuthInfo!

    signup(
      first_name: String!
      last_name: String!
      email: String!
      password: String!
      confirmPassword: String!
      business: Boolean!
    ): AuthInfo!

    updateProfile(
      first_name: String
      last_name: String
      email: String
      description: String
      profilePictureUrl: String
    ): User

    changePassword(
      currPassword: String!
      newPassword: String!
      newPasswordConfirm: String!
    ): User

    requestPasswordReset(email: String!): ResetPasswordToken!

    resetPassword(
      id: Int!
      password: String!
      confirmPassword: String!
      token: String!
    ): User!

    confirmEmail(id: Int!, token: String!): User!

    createEvent(
      name: String!
      eventPictureUrl: String
      eventDateTime: String!
      place: String!
      information: String
      isPrivate: Boolean!
      participantsLimit: Int
      selectedCircles: [InputCircle]
      selectedFriends: [InputUser]
    ): Event!

    editEvent(
      id: Int!
      name: String!
      eventPictureUrl: String
      eventDateTime: String!
      place: String!
      information: String
      isPrivate: Boolean!
      participantsLimit: Int
      selectedCircles: [InputCircle]
      selectedFriends: [InputUser]
    ): Event!

    joinEvent(eventId: Int!): Event!

    leaveEvent(eventId: Int!): Event!

    declineInvitation(eventId: Int!): Event!

    deleteNotification(id: Int!): Int!

    deleteAllNotifications: Int!

    notifyUsers(id: Int!): Boolean

    addFeedMessage(eventId: Int!, message: String!): FeedMessage!

    addFriend(selectedUserId: Int!): Int

    removeFriend(selectedUserId: Int!): Int

    deleteEvent(eventId: Int!): Boolean

    deleteCircle(circleId: Int!): Boolean

    createCircle(name: String!, invitedFriends: [Int]!): Circle!

    editCircle(id: Int!, name: String!, invitedFriends: [Int]!): Circle!
  }
`;

const main = async () => {
  const app = express();

  app.disable('x-powered-by');
  app.use(cors());

  let dbConnection = null;

  const apolloServer = new ApolloServer({
    typeDefs,
    resolvers: rootResolver,
    context: async ({ req, res }) => {
      if (!dbConnection) {
        dbConnection = await getConnection();
      }
      const auth = req.headers.Authorization || '';

      return {
        req,
        res,
        dbConnection,
        auth,
      };
    },
    plugins: [ApolloServerPluginLandingPageGraphQLPlayground()],
  });

  await apolloServer.start();

  apolloServer.applyMiddleware({ app, cors: false });

  const port = process.env.PORT || 4000;

  app.get('/', (_, res) => res.redirect('/graphql'));

  app.listen(port, () => {
    console.info(`Server started at http://localhost:${port}/graphql`);
  });
};

main();
