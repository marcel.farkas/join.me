const sendgrid = require('@sendgrid/mail');

export const sendEmail = async ({ receiver, subject, htmlMessage }) => {
  sendgrid.setApiKey(process.env.SENDGRID_API_KEY);

  const msg = {
    to: receiver,
    from: process.env.SENDGRID_SENDER_EMAIL,
    subject: subject,
    html: htmlMessage,
  };

  try {
    await sendgrid.send(msg);
  } catch (error) {
    throw new Error(error);
  }
};
