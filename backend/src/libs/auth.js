import { verifyToken } from './token';

export const isAuth = (authorization) => {
  if (!authorization) throw new Error('Not authenticated');

  const token = authorization.split('Bearer ')[1];

  if (!token) throw new Error('Not authenticated');

  try {
    return verifyToken(token);
  } catch (error) {
    throw new Error('Not authenticated');
  }
};
