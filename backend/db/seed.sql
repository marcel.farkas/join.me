-- Seed SQL:
-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- HostiteÄľ: localhost:3306
-- ÄŚas generovania: Pi 14.Jan 2022, 23:06
-- Verzia serveru: 5.7.36-0ubuntu0.18.04.1
-- Verzia PHP: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- DatabĂˇza: `user_team01`
--

-- --------------------------------------------------------

--
-- Ĺ truktĂşra tabuÄľky pre tabuÄľku `business`
--

CREATE TABLE `business` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `ico` int(11) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `owner` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- SĹĄahujem dĂˇta pre tabuÄľku `business`
--

INSERT INTO `business` (`id`, `name`, `ico`, `phone`, `email`, `address`, `owner`, `created_at`) VALUES
(1, 'Party place', 12345678, '+420123456789', 'party@place.cz', 'Praha', 29, '2021-11-13 18:57:20');

-- --------------------------------------------------------

--
-- Ĺ truktĂşra tabuÄľky pre tabuÄľku `chat_room`
--

CREATE TABLE `chat_room` (
  `id` int(11) NOT NULL,
  `event` int(11) DEFAULT NULL,
  `num_chatters` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Ĺ truktĂşra tabuÄľky pre tabuÄľku `circle`
--

CREATE TABLE `circle` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `owner` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- SĹĄahujem dĂˇta pre tabuÄľku `circle`
--

INSERT INTO `circle` (`id`, `name`, `owner`, `created_at`) VALUES
(1, 'OkrĂşhly circle', 40, '2021-11-13 20:57:59'),
(2, 'Ĺ tvorcovĂ˝ circle', 40, '2021-12-05 18:14:35'),
(3, 'TrojuholnĂ­kovĂ˝ circle', 40, '2021-12-05 18:14:35');

-- --------------------------------------------------------

--
-- Ĺ truktĂşra tabuÄľky pre tabuÄľku `circle_users`
--

CREATE TABLE `circle_users` (
  `user_id` int(11) DEFAULT NULL,
  `circle_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- SĹĄahujem dĂˇta pre tabuÄľku `circle_users`
--

INSERT INTO `circle_users` (`user_id`, `circle_id`) VALUES
(40, 1),
(45, 1),
(32, 1),
(40, 2),
(40, 3);

-- --------------------------------------------------------

--
-- Ĺ truktĂşra tabuÄľky pre tabuÄľku `email_verification_token`
--

CREATE TABLE `email_verification_token` (
  `user_id` int(11) NOT NULL,
  `token` varchar(128) NOT NULL,
  `token_expiry` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- SĹĄahujem dĂˇta pre tabuÄľku `email_verification_token`
--

INSERT INTO `email_verification_token` (`user_id`, `token`, `token_expiry`) VALUES
(41, '$2b$10$CIjo.iZp7uviIAK1R/sEw.KpMIEY7B9SuhX7YW0hnz3yuqjCrd..q', 1636746503762),
(42, '$2b$10$hFDhloVrvMGCG4JXYg7DW.VK8KU4ZkSOsbeWmCp.55ejvBqZ4V/1y', 1636759662374),
(44, '$2b$10$9ol9BwtIw0AjYOBvEXk.muXan3Tq3lCE4AxzbU3m7ZtFGCIXhnyh2', 1636937883967),
(54, '$2b$10$o4FE5eaqsCOK5Aq/bNC32.JRLOmSbYjNts/lKg8DPo1ag0ZEKi/LO', 1641828293267);

-- --------------------------------------------------------

--
-- Ĺ truktĂşra tabuÄľky pre tabuÄľku `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `place` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `private` tinyint(1) DEFAULT NULL,
  `participantsLimit` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `eventDateTime` datetime NOT NULL,
  `creator_id` int(11) NOT NULL,
  `eventPictureUrl` varchar(500) DEFAULT NULL,
  `information` varchar(500) DEFAULT NULL,
  `chatroom_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- SĹĄahujem dĂˇta pre tabuÄľku `event`
--

INSERT INTO `event` (`id`, `place`, `name`, `private`, `participantsLimit`, `created_at`, `eventDateTime`, `creator_id`, `eventPictureUrl`, `information`, `chatroom_id`) VALUES
(1, 'El MĂˇgico', 'Beer pong', 0, 50, '2021-11-01 11:58:17', '2022-05-21 04:19:00', 45, '', NULL, '4426f12e8dd96f5181bccb3c891d92f5cb982934043f8f03a5683034ccec7da4'),
(2, 'Magneto bar', 'Amundsen Party Hardggg', 0, 69, '2021-11-01 12:12:44', '2021-09-13 00:00:00', 45, '', NULL, '23ed0aa14919fc7bdbdff8ae8f3bf161aaf2408940a7a5db6f541d04ee441c4d'),
(3, 'Koleje Jarov', 'Erasmus Cultural Evening', 0, 50, '2021-11-01 13:30:26', '2021-11-10 04:19:17', 40, '', NULL, 'b134cf3188b8a7bc256d6d5aadf026ed7f427f605a6677b243ea7b651be12c8f'),
(4, 'El MĂˇgico', 'Legendario ElixĂ­r Night', 0, 50, '2021-11-01 13:30:26', '2022-01-03 04:19:17', 32, '', NULL, 'ceb19bbd6547cc52a6d253c26de29d97e8b168637589a096e312ff61efdcf85b'),
(5, 'El MĂˇgico', 'Mega KĂ˝bl Party', 0, 50, '2021-11-01 13:30:26', '2021-11-10 04:19:17', 40, '', NULL, 'ecc93be3cac21a30c04b01dfd1fff9a8b6f916acf2f2f7889ce177de0281e5f1'),
(6, 'Koleje Jarov', 'Swingers party', 0, 50, '2021-11-01 13:30:26', '2021-11-10 04:19:17', 41, '', NULL, '8a4885075d83d5adf207f8ab207cc0bd2589c7ef82234e5bfd8a1fdadce436d9'),
(7, 'Magneto bar', 'Afterparty SeznamovĂˇku', 0, 50, '2021-11-01 13:30:26', '2022-02-24 04:19:00', 32, '', NULL, '9a490d13f5a594da8a01679d8415bd87b0eac2937437d8829d11214d84199a03'),
(8, 'ModrĂˇ vopice', 'Nikdy Jsem Party', 0, 50, '2021-11-01 13:30:26', '2021-11-10 04:19:17', 40, '', NULL, '107364bf86c2c158dd43b0bf7825c63e1b5363b461f085b35aea3e5dd019a89d'),
(9, 'Klub Blanice', 'Turnaj ve stolnĂ­m fotbĂˇlkuu', 0, 50, '2021-11-01 13:30:26', '2021-11-10 04:19:00', 45, 'https://images.unsplash.com/photo-1574285013029-29296a71930e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=627&q=80', NULL, '181e791766e478c33ca1bb33409fa13d5907320090a6f945785b735240a5ab77'),
(10, 'NĂˇrodnĂ­ Muzeum', 'EPIC Prague', 0, 50, '2021-11-01 13:30:26', '2023-01-19 15:15:00', 40, 'https://www.26house.com/website_event/static/src/img/event_past_0.jpg', NULL, '1b06caca704d10668f5c4e79882bcdaa4723e3bce0ffe204201bbdcafb81e9f1'),
(11, 'RockOpera', 'RockOpera Praha', 0, 41, '2021-11-01 13:30:26', '2021-11-10 04:19:17', 41, '', NULL, 'ab8891113a56f06878cdf8dd46d128f5522e0b83259de12a6f1ce6891adebc5d'),
(12, 'U ĹˇtudĂˇka', 'La Bodeguita del Medio', 0, 150, '2021-11-12 23:00:11', '2022-07-21 00:00:00', 41, 'https://images.unsplash.com/photo-1530103862676-de8c9debad1d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8MXx8fGVufDB8fHx8&w=1000&q=80', NULL, '60812313a93ec8aea7bcde73accf350c1bc5fceade19056add1fe77df158fd4f'),
(23, 'Bar Na vÄ›tvi', 'The best event !!!', 0, 2, '2021-11-15 10:37:09', '2021-11-17 00:00:00', 45, 'https://static.novydenik.com/2021/04/Party-v-nocnim-klubu.-Foto-Venus100elite-Pxhere-1.jpg', 'Fun fun fun', '9443a6fd23f4dac988cec980c2ebdc0c77a0aaf5135109af0cae9b29ae24e228'),
(24, 'O2 Arena', 'Alko bitva', 1, 150, '2021-11-15 12:27:46', '2021-12-05 00:00:00', 40, NULL, 'Info', 'c2bbcdd5f5791674900b1969d529cd5bf58f628b6eb6f3496bd6868aa89e8f41'),
(25, 'ModrĂˇ vopice', 'Pivooo', 0, 2, '2021-11-15 12:31:37', '2021-12-01 00:01:00', 45, 'https://scontent.fprg5-1.fna.fbcdn.net/v/t1.18169-9/13566940_10204476784640180_5370938422310248585_n.jpg?_nc_cat=100&ccb=1-5&_nc_sid=09cbfe&_nc_ohc=HNlEtk5ZRTMAX_xBimA&_nc_ht=scontent.fprg5-1.fna&oh=c072d6a9edb294ccad01030cb803a1d0&oe=61C90DB5', NULL, 'f3469e6ddf773052dba71a6b035fb0dac44ec2d22f4f6d0593f3fce01e48c36d'),
(26, 'Klub Blanice', 'aaaaa', 0, 12, '2021-11-15 16:01:39', '2021-11-17 00:00:00', 45, 'https://www.jaktak.cz/sites/default/files/images/articles/23/mojito-3504486_640.jpg', 'jdfhdjskfh', '79f6c7c8519569b4b3051bc23f3a67ad27ca854fa59aa44a4e41e757d7cf4ce9'),
(29, 'AAA', 'aaaa', 1, NULL, '2021-11-16 21:26:57', '2021-10-25 18:00:00', 40, NULL, NULL, '6d3a1e12cda2a36fdea89061d16543ea03d761e91d11c08b1650e5e4d975f567'),
(30, 'DCBA', 'ABCD', 1, 1, '2021-11-16 22:02:19', '2021-11-27 18:00:00', 40, NULL, 'aaaaa', '4b1a2cfa7191f3f8ce11f4b445bb20f1baba374bd3e7a29ed789c4ed0234b277'),
(31, 'LetnĂˇ', 'Test Event', 1, NULL, '2021-11-20 14:50:21', '2021-11-26 20:00:00', 40, NULL, NULL, 'edb65b79150e49387fd6a5f61df95f95bad0f3af88ac18ced1fe92be8f69a113'),
(32, 'SB 326', 'Dirty Dancing', 0, 25, '2021-11-26 19:28:19', '2021-11-30 12:32:00', 32, 'https://cf.shopee.ph/file/62043fcd736528823b463e419d52303a', 'Dance NYAN', '2655e70c4e087af5d8132d784a6b076e5a6c8551b3dd60cd9bea2f1b569c06c7'),
(33, 'Place', 'Event name', 1, NULL, '2021-11-27 09:51:37', '2021-12-05 18:00:00', 45, NULL, 'Information about the event', '9cceb5ca1d6ce31db3f4615f83d674f30b62e4c8fecf7e1810f7b30087b78160'),
(34, 'Here', 'CatJam', 0, NULL, '2021-11-27 10:01:18', '2021-12-23 05:56:00', 40, NULL, 'cat cat cat cat cat', 'cf72e7488a6516bbfbf6b237ef9a6d8fce769e799849d427be3df160a831f91e'),
(35, 'asasasasas', 'sasasa', 1, NULL, '2021-11-27 23:01:08', '2022-01-12 12:03:00', 47, NULL, 'fdfdfdfd', '6b8600aabd7dac8d58ce6114e3ac7a19a0642621a251246d90d2167424f7c68d'),
(36, 'd', 'd', 1, NULL, '2021-11-27 23:01:23', '2021-11-04 11:01:00', 47, NULL, 'd', '42e9522c4e4ab117731c25113fdd0a2fc6e36f4267512fe78a5e94d505922e1c'),
(37, 'q', 'qa', 0, 1, '2021-11-28 11:18:34', '2021-12-22 00:01:00', 45, 'https://scontent.fprg5-1.fna.fbcdn.net/v/t1.6435-9/cp0/e15/q65/p320x320/56931879_1307851306029578_6011051505233166336_n.jpg?_nc_cat=108&ccb=1-5&_nc_sid=85a577&efg=eyJpIjoidCJ9&_nc_ohc=WNC1ntxNAaQAX8dS5q6&_nc_ht=scontent.fprg5-1.fna&oh=d425d883dc1a1b37c49267b0544a7c82&oe=61C962C6', NULL, '8bb3008b9d58bd64b295841233e6de71df8390dbeae5ded5de99012493c72f10'),
(38, 'a', 'test', 0, NULL, '2021-11-28 18:14:32', '2021-12-01 19:14:00', 32, NULL, 'a', '4470d218bab68e09346234079c0ad5c174385cc4b0b90f0c7a05b7c35196a9fb'),
(39, 'a', 'a', 0, NULL, '2021-11-28 18:16:19', '2021-11-03 19:16:00', 32, NULL, 'a', '8ff97011438952e0b91985aa7e787718bd2c9a43e1c160d8998eee99fe4d6ff4'),
(40, 'j', 'z', 0, NULL, '2021-11-28 18:29:36', '2021-11-17 11:01:00', 32, NULL, NULL, '443564d0bcc4fac54fc7d5d15e437986187091ccba3ab9fd5c62c8f5f314b1df'),
(41, 'Private', 'Private', 0, NULL, '2021-11-28 18:35:01', '2021-12-05 00:00:00', 45, NULL, 'Private', '7ff7744835096d693c27356904b2b4e128431b477fb59bd6d2d360b5e37ae5ec'),
(42, 'CeletnĂˇ', 'ZaÄŤĂˇtek letnĂ­ho semestru 2022', 0, 50, '2021-11-29 09:04:12', '2022-02-11 19:00:00', 48, 'https://www.superteacherworksheets.com/images/blog/back-to-school-pencils14720458621476197675.jpg', 'Party pro super zahĂˇjenĂ­ semestru :)', '78d689e5d2f39b585dd84eeb2936ecf89dea88448ead75ee1840ece15d89a56b'),
(43, 'Koleje Jarov', 'Afterparty zaÄŤĂˇtku letnĂ­ho semestru', 1, NULL, '2021-11-29 09:25:56', '2022-02-11 23:30:00', 48, NULL, NULL, '5a68a42efd13883bd16713f02cf2c9636a2dcc79e724b0af1e4b4f50ec22c19e'),
(44, 'PraĹľskĂ˝ Hrad', 'Defenestrace pĂˇnĹŻ politikĹŻ', 0, 23, '2021-11-29 09:32:10', '2021-12-02 10:31:00', 41, 'https://g.denik.cz/1/7b/defenestrace-divisek_denik-320-16x9.jpg', 'HahahaHehehe', 'fb98fa4f03f66684e670e8519fa598cce02455e38f0437190a55db39cd46d303'),
(45, 'Ta dakde', '1', 1, NULL, '2021-11-29 09:40:20', '2021-12-18 11:00:00', 40, NULL, NULL, '6e8ef5b3cbc90a083c5b28c21b782f55a9fae1d000aca7262d4a20382abb20d0'),
(46, 'a', 'private', 1, NULL, '2021-11-29 10:19:05', '2021-12-03 11:18:00', 32, NULL, 'a', 'ff998aade2ccf686b071c8a1d9a1944b2e992f23854f86e776eb400d9a830b59'),
(47, 'a', 'dsadas', 0, NULL, '2021-11-29 10:22:26', '2021-11-25 11:22:00', 32, 'dsadas', 'a', '32edbb0381c4d7d1c710e1cbcad810d57b7db652974b7458b9515cfb5a094e6c'),
(48, 'a', 'a', 0, NULL, '2021-11-29 10:25:56', '2021-11-10 11:25:00', 32, NULL, 'a', '0c5392bf091a4bdc38db960b2a2bb543e1f3988dd5b531a1fb14b5e58dbe1e54'),
(49, 'a', 'a', 0, NULL, '2021-11-29 10:29:28', '2021-11-11 11:29:00', 32, NULL, NULL, '60fc047fde51976a7d519f0a92a9e68874f628c454782b8592613f5d8d783d93'),
(50, 'Park', 'Doggos', 1, 3, '2021-11-29 10:37:51', '2022-04-20 15:00:00', 45, 'https://i.guim.co.uk/img/media/fe1e34da640c5c56ed16f76ce6f994fa9343d09d/0_174_3408_2046/master/3408.jpg?width=1200&height=900&quality=85&auto=format&fit=crop&s=0d3f33fb6aa6e0154b7713a00454c83d', 'Woof woof wooof', '040eef1631df7f60ea5e4bc6db0a3d992cf5120aa917454244563b29f3665c6b'),
(51, 'a', 'public', 0, NULL, '2021-11-29 10:38:18', '2021-11-24 11:38:00', 32, NULL, NULL, 'ada28c8715c3c411d4dae582dfd2768c6079c07653b44453c60375a695498aa3'),
(52, 'a', 'private', 0, NULL, '2021-11-29 10:38:40', '2021-11-25 11:38:00', 32, NULL, NULL, '954fa75a95736359a2ba9931ce120f8cf1b5c87eea4bf56636ea75fa7d598015'),
(54, 'a', 'test event', 1, NULL, '2021-11-29 10:46:34', '2021-12-03 11:46:00', 32, NULL, NULL, '2fce68915bf94ee93afb3c73c13d03ed2dc106a5e2d9f400410f7924ffa53809'),
(59, 'bar', 'pub ', 1, NULL, '2021-11-29 14:44:32', '2022-05-20 20:00:00', 45, NULL, 'drinks', 'cdd77c3b6c161f6a92145494cea6cf8f164c53167bf27a4b5ce2c874bc0830fb'),
(62, 'Nice place', 'Nice event', 1, NULL, '2021-11-29 14:53:28', '2022-07-22 15:53:00', 45, NULL, NULL, 'c4b7db094608babdd1019be8a025ca1340a1cadca678cb901579f4ac350ef1ca'),
(63, 'Bordel vo Viedni', 'OÄŤkovanie', 1, NULL, '2021-11-29 14:58:15', '2021-12-24 04:20:00', 40, NULL, NULL, '5a8bc5aec7e1ad74b9356d1be9f837193aa45a6991a5d9bde3b334e267994e6d'),
(64, 'Za horami a dolami', 'abab', 1, NULL, '2021-11-29 14:59:59', '2022-12-18 11:11:00', 40, NULL, NULL, '572eb3dc5a46da6f35198af21a2ab33f230b2c12e2951b99cacdbe53697d8c9e'),
(65, 'Aleluja', 'qwe', 1, NULL, '2021-11-29 15:03:37', '2021-12-01 11:12:00', 40, NULL, NULL, 'f81a3ef5c91b15f83d958b02ab17d68d61d914b96af902c803b5d5e5510150ac'),
(66, 'Aleluja', 'qwe', 1, NULL, '2021-11-29 15:04:36', '2021-12-11 11:12:00', 40, NULL, NULL, '12cf8137aaa6e045dd7a8e97ef33cb88db1d3b6aa657699ac4ce5c88087307ff'),
(68, 'U mÄ› doma', 'KolaudaÄŤka', 1, 30, '2021-11-29 16:02:02', '2021-12-05 18:00:00', 48, 'https://lh3.googleusercontent.com/proxy/xC1gH-AvoXbgHNEBlx4catg58DoQJYOPztqz1R9pW1u_DArdDspovxnX33LD1KJZ0ip1pTkf-jHI_dO8EFHlkGRXRJ46JMYUMrfl9vO82w52PUxE9JWe-w', NULL, '4b62d5cea88de34259748f71a4b7f17cf46c1fae7784d94334027c9333b6d542'),
(69, 'Test', 'Test', 1, NULL, '2021-11-29 16:06:10', '2021-11-30 19:08:00', 51, NULL, 'Test', '24a208c2ac40692cabe8b6dbdb3f3a40bdbaf36b251e8c1c2858fbcd49df011e'),
(70, 'a', 'a', 1, NULL, '2021-11-29 16:11:47', '2021-11-27 17:11:00', 45, NULL, NULL, 'f7f7725f7e5121a4ab19a886514396a0e0eaf9ac6bc499291988f4ce9b99c86d'),
(71, 'a', 'a', 0, NULL, '2021-12-06 14:46:55', '2021-12-25 19:46:00', 32, NULL, NULL, '876277ecaf2b27fed2cbf626434e1c840584dc5641268805bfd026f51025d901'),
(72, 'a', 'a', 0, NULL, '2021-12-06 14:47:36', '2021-12-11 15:47:00', 32, NULL, NULL, '19f48d59bc93d2e493985908620d325ec9f6300626282de11f996d14008a0f7c'),
(73, 'a', 'a', 1, NULL, '2021-12-06 14:53:40', '2021-12-16 15:53:00', 32, NULL, NULL, 'eb1888d854b6a885489d635421c14cc1a623bbfdb3dd1bd45f9e9ea5c116189d'),
(74, 'a', 'a', 0, NULL, '2021-12-06 14:57:52', '2021-12-04 15:55:00', 32, NULL, NULL, 'c9b878707ff33aa32b1156754f0dadb1c860f86c099bec72e9a11e0dae39126e'),
(75, 'a', 'aaaa', 0, 69, '2021-12-06 14:58:39', '2021-12-09 15:58:00', 32, 'aa', NULL, 'a36f19d45a1a88424dcefaecce4b401f3c67e49cdecde7f322299336880897f4'),
(76, 'a', 'a', 0, NULL, '2021-12-06 15:02:24', '2021-12-03 16:02:00', 32, NULL, 'a', 'e040bd7e8dce9a9a36d8877dffcf975bbc936da93927a5dee851df7f958c10bc'),
(77, 'a', 'a', 0, NULL, '2021-12-06 15:03:09', '2021-12-16 16:03:00', 32, NULL, NULL, '29e499ec2d4b984f769f44149da92357c368fab7eecf290ffd85acaa815c51c8'),
(78, 'a', 'a', 1, NULL, '2021-12-06 15:05:04', '2021-12-17 16:05:00', 32, NULL, NULL, 'a7e33c1e6f0b3765e590fb667112c0466fbc589cc37ebe96139d49a02ac242a3'),
(79, 'a', 'a', 0, NULL, '2021-12-06 15:05:42', '2021-12-03 16:05:00', 32, NULL, NULL, 'aa31299e370ce9d3967ad45aaf44d5d60a78dcdaf54216e5a09154324b804e1e'),
(80, 'a', 'a', 0, NULL, '2021-12-06 15:09:20', '2021-12-06 18:09:00', 32, NULL, NULL, '978c5aa2a7202791a345616474a6f8b68a2a8dd745a34f5fbd20a9edb2bbbfa4'),
(81, 'a', 'a', 0, NULL, '2021-12-06 15:10:49', '2021-12-09 16:10:00', 32, NULL, NULL, '9816a47e84ceb6df899824d1a5a44c7d33de2e8eea849d80ee6b42c42051aafe'),
(82, 'a', 'a', 1, NULL, '2021-12-06 16:03:26', '2021-12-03 17:03:00', 32, NULL, NULL, 'bb43ba4cbe294005ac1c5978afa9518b167f1272324dbb5106fe7ef0e28f4e84'),
(83, 'a', 'a', 0, NULL, '2021-12-06 16:10:13', '2021-12-11 17:10:00', 32, NULL, NULL, 'd6af1fcd88588954a9da6135703e7105de3ec0e2226f43ac218842c2e01654de'),
(84, 'a', 'a', 0, NULL, '2021-12-06 19:24:04', '2021-12-09 20:20:00', 32, NULL, NULL, '481bf13837cc737bcd2b73084a9cc6f5774136bdd0048bbcbe51ee2ee2096cac'),
(85, 'a', 'a', 0, NULL, '2021-12-06 19:24:49', '2021-12-15 20:24:00', 32, NULL, NULL, '39c19080769c26ed44b355a1122d87499b7d0d074b96ae0088820c1410b638ae'),
(86, 'a', 'a', 1, NULL, '2021-12-06 19:27:22', '2021-12-08 20:27:00', 32, NULL, NULL, 'd678383944ea8879d5a9bd2b36ebe7f159bc337ed40a78fba955583303792fa8'),
(87, 'a', 'a', 0, NULL, '2021-12-06 19:28:14', '2021-12-02 20:28:00', 32, NULL, NULL, '94d266e238d24f0f58e5698941212a52330fa4d6b7860d5f0b33c05d7d41870f'),
(88, 'a', 'a', 1, NULL, '2021-12-06 19:33:26', '2021-12-03 20:33:00', 32, NULL, NULL, '88866c70f067368d62a135c286a00c11247a82c90b27c0817b3d089488fc38ff'),
(89, 'a', 'a', 1, NULL, '2021-12-06 19:41:58', '2021-12-08 20:41:00', 32, NULL, NULL, '9935d46b9c729e68c34afb452c7886ffb5dbf80bb43ebdd2f2f43ec905897d63'),
(90, 'a', 'a', 1, NULL, '2021-12-08 11:39:57', '2021-12-10 12:39:00', 32, NULL, NULL, '617e3c3799977d7947d897d185526115bdb763844e5e46772f156ebafba908e9'),
(91, 'a', 'a', 1, NULL, '2021-12-08 11:58:46', '2021-12-17 12:58:00', 32, NULL, NULL, '4b6128a8145fb8f78f3a967ca7a21a7a241147f20b6ef0297a9de14dbeea1733'),
(92, 'a', 'a', 1, NULL, '2021-12-08 12:03:14', '2021-12-02 13:03:00', 32, NULL, NULL, 'ad771275565f36bef27b04f8c3042e32edce98e4e9d716d80fff27379c3d2c0c'),
(93, 'a', 'a', 1, NULL, '2021-12-08 12:15:12', '2021-12-09 13:15:00', 32, NULL, NULL, '7cad27237d4c2af46b0cc8ccd8fae90f8ee42d56c39fd18a42a056128617645e'),
(94, 'a', 'a', 1, NULL, '2021-12-08 12:19:58', '2021-12-04 13:19:00', 32, NULL, NULL, '298a7bfa635407903fef99866d7cf4d6153274c3686adf14f0ae92ac1e50e973'),
(95, 'a', 'a', 1, NULL, '2021-12-08 12:24:07', '2021-12-10 13:23:00', 32, NULL, NULL, '8dcf0c592596c9040843a400ad7b349d6b30e47c027492657102d483865172e4'),
(96, 'a', 'a', 0, NULL, '2021-12-08 12:26:24', '2021-12-10 13:26:00', 32, NULL, NULL, '6d2af194ac0f0ae4e1417b4a3b9729caaef3bc04b05e0431a57aac2373fa2866'),
(97, 'a', 'a', 0, NULL, '2021-12-08 12:32:07', '2021-12-10 13:32:00', 32, NULL, NULL, '14006c6044876f299537d06b764ee410ab599d6bff12a9b7b9e1fcc0dd4c6540'),
(98, 'a', 'a', 1, NULL, '2021-12-08 12:34:08', '2021-12-09 13:34:00', 32, NULL, NULL, '767a551d3c49bac4a1ed4e57d5dc3a76a3be67af5cacbd842285e62c4531079f'),
(99, 'a', 'a', 1, NULL, '2021-12-08 12:34:48', '2021-12-15 13:34:00', 32, NULL, NULL, 'd093813c76ecb9cc321b108b9c29e8a9d9ee893bdc8f583ea1bd32d64b1ca037'),
(100, 'a', 'a', 1, NULL, '2021-12-08 12:37:06', '2021-12-03 13:37:00', 32, NULL, NULL, '8bbe0078d5516c1dcab0c8836310c1ae87f6c6cbfb97d3330c4ad27393c1e672'),
(101, 'a', 'a', 1, NULL, '2021-12-08 12:38:21', '2021-12-16 13:38:00', 32, NULL, NULL, '74c5dac19643127ece895aebaf2e2bc93240cadffeaa71fa75fbf975a3b73bc7'),
(102, 'a', 'a', 1, NULL, '2021-12-08 12:39:55', '2021-12-10 13:39:00', 32, NULL, NULL, 'd51f978d582d8657f0d9914b7a5c1c8fc3030704b02e9c819b72c06ce2bd91c5'),
(103, 'a', 'a', 1, NULL, '2021-12-08 12:40:31', '2021-12-09 13:40:00', 32, NULL, NULL, 'aa900f32679979217850e047dc7850260f0647565f987371498f2ad4d1d4dcf9'),
(104, 'a', 'a', 1, NULL, '2021-12-08 12:40:56', '2021-12-11 13:40:00', 32, NULL, NULL, '35a74f0b327bd9bc628a7ec149028a261bfc50bbbb39c58c8c62488acfe7e3c2'),
(105, 'a', 'a', 1, NULL, '2021-12-08 12:56:42', '2021-12-17 13:56:00', 32, NULL, NULL, 'dd3327ef6a109a7e4878102d5a1830a5063e37035afb047a842f8c0bda1af90a'),
(106, 'a', 'a', 1, NULL, '2021-12-08 12:58:13', '2021-12-16 13:58:00', 32, NULL, NULL, 'b6d7bb611c5c0c3f8dbaa2b55ec3644987e003294e188eb5f0b8ebf48f048286'),
(107, 'a', 'a', 0, NULL, '2021-12-08 12:59:28', '2021-12-16 13:58:00', 32, NULL, NULL, '8b2a6c32bcc5aba38a92aae2180cf9b769e3e1c2fbda11e8ac11d9d04101d638'),
(108, 'a', 'a', 0, NULL, '2021-12-08 13:00:03', '2021-12-18 14:00:00', 32, NULL, NULL, '0e752148c96550c60fd3b2e4bae968186daca3a808d7a53eda6fbedd7c4c5068'),
(109, 'a', 'a', 1, NULL, '2021-12-08 13:01:29', '2021-12-24 14:01:00', 32, NULL, NULL, '04a1d11c7a24fafb3f1cc4db87a0fad521b7433fc8a4a8b7e6aabf8c7025ea61'),
(110, 'a', 'a', 1, NULL, '2021-12-08 13:06:40', '2021-12-22 14:06:00', 32, NULL, NULL, '80e17089c096c7b29fa861116d0a6483f7d968f6689fa28326c7463e8314db4b'),
(111, 'a', 'a', 1, NULL, '2021-12-08 13:09:32', '2021-12-16 14:09:00', 32, NULL, NULL, '9dc7e9c3bda3a0885b5dc598a5cfc1b981af3f82149c7d20d555e437d2a70176'),
(112, 'a', 'a', 0, NULL, '2021-12-08 13:21:17', '2021-12-24 14:21:00', 32, NULL, NULL, 'b3a733cbeae1eb4e4104e2c716bb3ba0dfac2d48d83f75ae41cff833029914aa'),
(113, 'a', 'a', 0, NULL, '2021-12-08 13:26:46', '2021-12-21 14:26:00', 32, NULL, NULL, '4f15cb0184cb0db8eb4fcc3df35424b137351ee01b7f52a68bfbb06287501b96'),
(114, 'a', 'a', 1, NULL, '2021-12-08 14:56:33', '2021-12-17 15:56:00', 32, NULL, NULL, '634ad550b9fd47a30ff87552770b7c6098c54f547d375a32f750d120a0b70a2b'),
(115, 'a', 'a', 1, NULL, '2021-12-08 15:00:57', '2021-12-17 15:56:00', 32, NULL, NULL, 'f77c19c9d58fc06230946e1eef0da36b3b92beede1287eeb92e125ec8febb141'),
(116, 'a', 'a', 1, NULL, '2021-12-08 15:01:00', '2021-12-17 15:56:00', 32, NULL, NULL, '15f38aef90872df48b8026f1a84d741f52c2e34eede0ba9d016f9a3e7da0efcd'),
(117, 'a', 'a', 0, NULL, '2021-12-08 15:08:59', '2021-12-10 16:08:00', 32, NULL, NULL, 'fb258b1a8a00851849c5f1fddd41287975bea825992489a7a206fbace2948553'),
(118, 'a', 'a', 0, NULL, '2021-12-08 15:44:00', '2021-12-25 16:43:00', 32, NULL, NULL, '7474d89e1dd8bf9439a46a5ece209088d4bbc342ff1e8ccf9b4744a03a451847'),
(119, 'a', 'a', 0, NULL, '2021-12-08 15:44:40', '2021-12-25 16:43:00', 32, NULL, NULL, '65aeb14880ab3e84cb53e30bea877ad156f7858cb0f9cefa22c6a5f66ef35a6a'),
(120, 'a', 'a', 0, NULL, '2021-12-08 15:45:42', '2021-12-25 16:43:00', 32, NULL, NULL, '6a6c3b5365aee7b2bb1ca40f9e378486c51965cba335e5154653bea93ccae531'),
(121, 'a', 'a', 1, NULL, '2021-12-08 16:29:49', '2021-12-08 20:29:00', 32, NULL, NULL, '7359feabbe2fb238b0bf64f856fbd03518b03a9cae7e3a74ce91a701f200fa87'),
(122, 'a', 'a', 1, NULL, '2021-12-08 17:12:38', '2021-12-10 18:12:00', 32, NULL, NULL, '8de1af84f1d73258fb255d38d63908123fc3d137a2cdfd8b8b7c1167d80077e8'),
(123, 'a', 'a', 0, NULL, '2021-12-08 17:16:30', '2021-12-10 18:12:00', 32, NULL, NULL, '5205eaa00899007bb9c114645a5c9e4aea7fd50861aeac0e90e9fdc14b590cd3'),
(124, 'a', 'a', 0, NULL, '2021-12-08 17:18:15', '2021-12-10 18:12:00', 32, NULL, NULL, '5afb6a63e7972d1a38ae354af6d2c05aba790abae32913a663a0ab8b54ceb273'),
(125, 'a', 'a', 0, NULL, '2021-12-08 17:19:32', '2021-12-10 18:12:00', 32, NULL, NULL, '7452168723ea3d2717ba4fdc27fd475375522f6191596b6e3c946caa9c150743'),
(126, 'a', 'a', 0, NULL, '2021-12-08 17:20:20', '2021-12-10 18:12:00', 32, NULL, NULL, '2c5d92cb5516e427309b646b888672668f9bfee0f5aca4b783d444407e75820f'),
(127, 'a', 'a', 0, NULL, '2021-12-08 17:20:40', '2021-12-10 18:12:00', 32, NULL, NULL, '40748c862a32afadfef0711697ed6be6a3cdeae2c808a2b686b5ac7343066d32'),
(128, 'a', 'a', 0, NULL, '2021-12-08 17:21:07', '2021-12-10 18:12:00', 32, NULL, NULL, 'cd5c628067577d4e883e8a7e715b199b5f3d7ff70e6469002925f9affbc1cbf1'),
(129, 'a', 'a', 0, NULL, '2021-12-08 17:21:35', '2021-12-10 18:12:00', 32, NULL, NULL, '2586ffa51520940491d8c02a4f8d4f289798e5ce47c0c97bc703ad2bfcd7f674'),
(130, 'a', 'a', 0, NULL, '2021-12-08 17:21:47', '2021-12-10 18:12:00', 32, NULL, NULL, '98f02da4750a11c046fc88fa693a62d2ec5c3eabacc3ab8a267fcd821e956f0b'),
(131, 'a', 'a', 0, NULL, '2021-12-08 17:22:15', '2021-12-10 18:12:00', 32, NULL, NULL, '5f0db4a7e6962f8e28c0b5ecfedeb499721a6f9368ddc5243bf0de9cba701b83'),
(132, 'a', 'a', 0, NULL, '2021-12-08 17:22:53', '2021-12-10 18:12:00', 32, NULL, NULL, '42de625b944140ef3355503003572b80342462e2d670f345d0c899b87b90d100'),
(133, 'a', 'a', 0, NULL, '2021-12-08 17:24:07', '2021-12-10 18:12:00', 32, NULL, NULL, '8cb2f80e416e8a0e3e685a69bcbe2af532a003a81d08c93892b5581adf88c7d0'),
(134, 'a', 'a', 0, NULL, '2021-12-08 17:24:32', '2021-12-10 18:12:00', 32, NULL, NULL, '6a7131fea77ec652e54d1e4f092da19bc4a586423d2824c9a261de9e83005620'),
(135, 'a', 'a', 0, NULL, '2021-12-08 17:24:37', '2021-12-10 18:12:00', 32, NULL, NULL, '4e63cd5d8c9480049e8bb402156e0006693df00942253260f3ee2b9f06a14ce0'),
(136, 'a', 'a', 0, NULL, '2021-12-08 17:24:40', '2021-12-10 18:12:00', 32, NULL, NULL, '29207d369125a53f3a83015a29dbc9546cbabcf8251434911beb1eafd943642b'),
(137, 'a', 'a', 0, NULL, '2021-12-08 17:24:59', '2021-12-10 18:12:00', 32, NULL, NULL, '6c5f9fd70fdb062b6329482abd6fbbbb8b9d16adeed2cd4be30082eeee9e75b0'),
(138, 'a', 'a', 0, NULL, '2021-12-08 17:25:19', '2021-12-10 18:12:00', 32, NULL, NULL, 'cb9c60c672135daaa2060c673683800c224016c8df0c70c59060304fe7821af8'),
(139, 'a', 'a', 0, NULL, '2021-12-08 17:25:32', '2021-12-10 18:12:00', 32, NULL, NULL, '370805149c9559c57e3a693740a24921fcd26907e02a93f6890b44dd8df76291'),
(140, 'a', 'a', 0, NULL, '2021-12-08 17:25:34', '2021-12-10 18:12:00', 32, NULL, NULL, '76ad589ab3eba8ca4cdf3d3dae4ba3aa691a4f2cc9a13cdf6083fa5f58562fac'),
(141, 'a', 'a', 0, NULL, '2021-12-08 17:26:03', '2021-12-10 18:12:00', 32, NULL, NULL, '4541afa0093509b70250376cf792b309990ac05ba9afcafb98cd452121e1d646'),
(142, 'a', 'a', 0, NULL, '2021-12-08 17:26:05', '2021-12-10 18:12:00', 32, NULL, NULL, '372afac3f6ec1745532d27590b391564b76ea69d6939e3b73624e4f1978555a7'),
(143, 'a', 'a', 0, NULL, '2021-12-08 17:26:17', '2021-12-10 18:12:00', 32, NULL, NULL, '8d91b132666ad74176055d9bac718c3b946869afd9a8cf257330c71c62c2b766'),
(144, 'a', 'a', 0, NULL, '2021-12-08 17:26:24', '2021-12-10 18:12:00', 32, NULL, NULL, '8e4ac21a19ff2658ca74fb354bb9b5ad4f4ea30c3348c59ab0158bf8eccaf42d'),
(145, 'a', 'a', 0, NULL, '2021-12-08 17:26:38', '2021-12-10 18:12:00', 32, NULL, NULL, '7b0de29bb52e9d7527320172d7743236b3eb81d32f2158a8f0e8d98d4703bb6c'),
(146, 'a', 'a', 0, NULL, '2021-12-08 17:26:44', '2021-12-10 18:12:00', 32, NULL, NULL, 'acffa39d179fca410b1676d82cd390e7806a39d51571f4b4fd1cfb951585536a'),
(147, 'a', 'a', 0, NULL, '2021-12-08 17:28:05', '2021-12-10 18:12:00', 32, NULL, NULL, '7ba44d24223a74c6d28b84e228cc3d9de6fc357df4cec6e103d6e8b8001d3924'),
(148, 'a', 'a', 0, NULL, '2021-12-08 17:28:11', '2021-12-10 18:12:00', 32, NULL, NULL, '19045b94b26999694ef5338de0ab841f474e9a16927ef2888fc236d3965fc8dd'),
(149, 'a', 'a', 0, NULL, '2021-12-08 17:28:42', '2021-12-10 18:12:00', 32, NULL, NULL, '1f8d4bde4b6602a7552f04e7d0886034eae40677510ee7828266776edea9bc78'),
(150, 'a', 'a', 0, NULL, '2021-12-08 17:29:13', '2021-12-10 18:12:00', 32, NULL, NULL, '17b08a139e049b79a78a3a66d9eb9499ff8777dc566af386bf530733dadc54b7'),
(151, 'a', 'a', 0, NULL, '2021-12-08 17:29:28', '2021-12-24 18:12:00', 32, NULL, NULL, 'b773d038e48dc79c0495b491c828d492a9794ffe957b26833b894a40867a8979'),
(152, 'a', 'a', 1, NULL, '2021-12-08 17:49:17', '2021-12-17 18:49:00', 32, NULL, NULL, '2da8fac49f2d1333e598ac3e76e0b951a6c308636a7c7b037fb2bfa87d367f10'),
(153, 'a', 'a', 1, NULL, '2021-12-08 18:07:23', '2021-12-23 19:07:00', 32, NULL, NULL, 'f6f9a77501f9d8124590690431c843e7e3f2938973abee7ec477f0d96f049271'),
(154, 'a', 'a', 1, NULL, '2021-12-08 18:16:31', '2021-12-24 19:16:00', 32, NULL, NULL, '17c34ab04c11d5cfeaab5913d559f98b6e49780ec4a7810849251a32d0eef267'),
(155, 'a', 'a', 1, NULL, '2021-12-08 18:16:51', '2021-12-09 19:16:00', 32, NULL, NULL, '812301f299b766d03a503feff66769e7d975fa1ebe9fd6bccdf47893da9501a9'),
(156, 'a', 'a', 1, NULL, '2021-12-08 18:19:17', '2021-12-17 19:19:00', 32, NULL, NULL, '2e7291ba099c51a80155478e2271c78a5eef8116cea7abe7c5610c84c0bdf44c'),
(157, 'a', 'a', 1, 69, '2021-12-08 18:19:52', '2021-12-09 19:19:00', 32, 'a', NULL, '5e63efa7b54a7ce909b4dff28c5c3b424a93f18d1315618c84b9ecbd4d60190b'),
(158, 'a', 'a', 1, NULL, '2021-12-08 18:23:52', '2021-12-30 19:23:00', 32, NULL, NULL, 'ca163aa5d4f27d03f39cbaa5c0bf772d396699133476a7931c33893ba6bda180'),
(159, 'a', 'a', 1, NULL, '2021-12-08 18:35:41', '2021-12-09 19:35:00', 32, NULL, NULL, '0cae9189f73de07cddff265d1149c598a9281f30d4977ea713d6f3011aefc446'),
(160, ' ', ' ', 1, NULL, '2021-12-08 18:36:09', '2021-12-01 19:36:00', 49, NULL, NULL, '0b9a4c93969670528f18bf8a9afc92ab6fb814da69634cb8f651ae0cbf7e2212'),
(161, ' ', ' ', 1, 999, '2021-12-08 18:39:24', '2101-12-08 19:36:00', 49, NULL, NULL, 'fdcad2ef7d179207101e6599080e71a0156c1b585815798368de619ce2ae50e9'),
(162, 'a', 'a', 1, NULL, '2021-12-09 13:01:01', '2021-12-18 14:00:00', 32, NULL, NULL, 'aa05c7a760105064f1836cc2803a33f5ff080586d69862c6505e06db455fda49'),
(163, 'a', 'a', 1, NULL, '2021-12-09 13:08:21', '2021-12-18 14:08:00', 32, NULL, 'a', 'e88bbb7739ac8a87555971b0f5e760982cab7ff3dad1c123105ffaae9f0866bf'),
(164, 'a', 'a', 1, NULL, '2021-12-09 13:36:39', '2021-12-25 14:36:00', 32, NULL, NULL, '8fd0acb842a51f561f8ab5c0a1b8ae607e011438e7c6a95f9e18987ac2db8c55'),
(165, 'test', 'test', 1, 69, '2021-12-09 14:01:43', '2021-12-31 15:01:00', 32, 'lol', NULL, '3eece59a9a0a7350797c652d5e346d6515ef241ddfe1ff3e5817d610f9e9c480'),
(166, 'aaaa', 'aaaaa', 1, NULL, '2021-12-09 14:10:28', '2021-12-23 15:10:00', 32, NULL, NULL, 'bc17407ba70e1f76e25209efc01554fdcbb1f82b88afe18548d7211ad566b90b'),
(167, 'dasdas', 'sdafasd', 1, NULL, '2021-12-09 14:10:54', '2021-12-16 15:10:00', 32, NULL, NULL, '84519ff96f16765537279d04b4905e602b22b08a93475d3d3283e50b053ecaed'),
(168, 'a', 'a', 1, NULL, '2021-12-09 14:14:34', '2021-12-24 15:14:00', 32, NULL, NULL, 'e1ca0b3352b398b83bab89b5dc231fd340bcc547a4cdd0573c603079c36502fc'),
(169, 'dsadas', 'dsadas', 1, NULL, '2021-12-09 14:14:47', '2021-12-16 15:14:00', 32, NULL, NULL, 'd20a1133fe715c9b2236cc66831042b6d2743f878bfcc9a2fb15605f04194311'),
(170, 'a', 'a', 1, NULL, '2021-12-09 14:17:04', '2021-12-25 15:16:00', 32, NULL, NULL, '137fe2f0646bc5350a3b609c982674e85c7a7b6d7fb2048132634f8271d2c500'),
(171, 'a', 'a', 1, NULL, '2021-12-09 14:17:41', '2021-12-23 15:17:00', 32, NULL, NULL, 'a4e56c8c970f0b0e1ff72842d7a8d915e8281f0e177eaed3997b745fd07dd0b2'),
(172, 'a', 'a', 1, NULL, '2021-12-09 14:18:06', '2021-12-30 15:18:00', 32, NULL, NULL, 'e8e03e0deba05512f008bde2d7164bae899899ce76d69419465d789b9a69c677'),
(173, 'a', 'a', 1, NULL, '2021-12-09 14:19:01', '2021-12-17 15:18:00', 32, NULL, NULL, 'd2b70aeb5928bba1278a69ef6acb514b2b47f2529c3f5f0652d2d868482903e4'),
(174, 'a', 'a', 1, NULL, '2021-12-09 14:19:10', '2021-12-17 15:18:00', 32, NULL, NULL, '13580eebdba1122b249dd458ee0fbd61df397227c906d4e4f430b5f03cfbcaf7'),
(175, 'a', 'a', 1, NULL, '2021-12-09 14:19:12', '2021-12-17 15:18:00', 32, NULL, NULL, '9835fa8bf76f8fef05367984c171ac6eedfd56306a66df6e292ad4e698d952da'),
(176, 'a', 'a', 1, NULL, '2021-12-09 14:19:58', '2021-12-16 15:19:00', 32, NULL, NULL, '8bb18989e5f5ec107c14d04358e22ceb65bdceb32d31ac5d4e6d228717b2ca42'),
(177, 'a', 'a', 1, NULL, '2021-12-09 14:20:17', '2021-12-18 15:20:00', 32, NULL, NULL, '753783fce9b04560b75b07adc99022a5404700e05ec7fc4267b4104c4e59741b'),
(178, 'a', 'a', 1, NULL, '2021-12-09 14:20:46', '2021-12-25 15:20:00', 32, NULL, NULL, 'd0fb777d516d36fde4d7bc1d9adb197efd408ab2019ae33c29bbcc7f78130458'),
(179, 'a', 'a', 1, NULL, '2021-12-09 14:21:35', '2021-12-18 15:21:00', 32, NULL, NULL, 'bf64c0e0faf3fc6aecf6fd9dfa23138171b8571776b297867b9f75ef0eb69dbe'),
(180, 'a', 'a', 0, NULL, '2021-12-09 14:22:22', '2021-12-31 15:22:00', 32, NULL, NULL, '6eb0cc4c86d4e1f26e02c8b3142be3f6a2bc81d90d6e2901d4666e03fc9f5a8d'),
(181, 'a', 'a', 1, NULL, '2021-12-09 14:25:48', '2021-12-08 15:25:00', 32, NULL, NULL, '1ca624631eeb4abc05c2c271b02d714ba5cf8fcb5a23de5911a295f36797b50c'),
(182, 'a', 'a', 1, NULL, '2021-12-09 14:26:37', '2021-12-24 15:26:00', 32, NULL, NULL, 'd1433a48e7bcc51977f9239910127e4818563b4568659fb0808fe0d8d2ca6e01'),
(183, 'a', 'a', 1, NULL, '2021-12-09 14:32:51', '2021-12-23 15:32:00', 32, NULL, NULL, '61aa5831a8b26c766b5693660f2efe2df7897c36396f4ddda89c21c596d0db82'),
(184, 'A', 'a', 1, NULL, '2021-12-09 14:34:59', '2021-12-24 15:34:00', 32, NULL, NULL, 'b297896ffc4b9328dffd7ce87ee93626d16e71958871b0ed96f3ea6a67d14e9d'),
(185, 'a', 'a', 1, NULL, '2021-12-09 14:35:09', '2021-12-25 15:35:00', 32, NULL, NULL, '749103cc2726976768924b571451c30add6b1929ffc5951546015dfbd3d52b3c'),
(186, 'place', 'jmeno', 1, NULL, '2021-12-15 17:06:42', '2021-12-23 12:52:00', 45, NULL, NULL, '8c0dd595d6a36d275607550aadbf08080d9788ec43236fae54114a6af71f8d06'),
(187, 'aaaa', 'test event 2022-01-10', 1, 69, '2022-01-10 11:36:26', '2022-01-27 12:36:00', 32, NULL, 'bbbb', '3ca686b8e3d43e1b23fab1cb40936df691787de4573ae89d6c63abce306f0ade'),
(188, 'aaaa', 'test event 2022-01-10', 1, 69, '2022-01-10 11:40:33', '2022-01-27 12:36:00', 32, NULL, 'bbbb', 'f72b3fe765b7c3b9d2bab589ab2adeed77fd78162386a8ad95e2faba82066b77'),
(189, 'a', 'a', 1, NULL, '2022-01-10 11:41:47', '2022-01-22 12:41:00', 32, NULL, NULL, 'c3993a749366134acf5d1217660e1a7e5e3a6525df2d84bd11151dad55af5425'),
(190, 'a', 'a', 1, NULL, '2022-01-10 11:41:55', '2022-01-16 12:41:00', 32, NULL, NULL, '843248bd3d426fa0152399c252fbb2185469015364a490998e1631a8de328f81'),
(191, 'a', 'a', 1, NULL, '2022-01-10 11:42:00', '2022-01-16 12:41:00', 32, NULL, NULL, 'b0ae57fd016a2d81d77208b88f80a3db4080c4e7d67f12f556714a77d5a26928'),
(192, 'a', 'a', 1, NULL, '2022-01-10 11:42:02', '2022-01-16 12:41:00', 32, NULL, NULL, 'a6b639a8005f20d6f85bb8fb5b23a1339f83fdadae836a14de55de9abea6c18e'),
(193, 'aaaa', 'test event 2022-01-10', 1, 69, '2022-01-10 11:42:36', '2022-01-27 12:36:00', 32, NULL, 'bbbb', '5fe0466ff0e34f506c5126775b8e21ae79fe33a8c5cfc986697b7805b978b76f'),
(194, 'aaaa', 'test event 2022-01-10', 1, 69, '2022-01-10 11:43:46', '2022-01-27 12:36:00', 32, NULL, 'bbbb', '11419f0f4fd15964950d6aac1f640c4fdad0467fc56082645184fe1648c97fd6'),
(195, 'a', 'a', 0, 69, '2022-01-10 12:22:06', '2022-01-06 13:22:00', 32, NULL, 'a', '5abf7c0316cd5ac17516e011bc66eac5b4bc44d57111bb0ed4925223600162dd'),
(196, 'a', 'test1-38-pm', 0, NULL, '2022-01-10 12:38:14', '2022-02-03 13:38:00', 32, NULL, 'a', '20bb54f071997cbcd5c41b427900683ca3ffd3a179b1f4939ff522a509b826ba'),
(197, 'a', 'a', 0, NULL, '2022-01-10 13:40:44', '2022-01-15 14:40:00', 32, NULL, NULL, '4e1f3cc0cb07860b1613702316eedc46a1766b7db5dea2177c6814b17abe7fa8'),
(198, 'KuchynÄ›', 'MĂˇma mele maso', 1, 23, '2022-01-11 18:32:04', '2022-01-23 19:30:00', 32, 'https://1gr.cz/fotky/pes/09/054/pcl/WAG2b74fc_slabikar_1.jpg', 'maso mele mĂˇmu', '37177590b0cc84429c856e11b6a38262c7c1c58a7782e110ee8daab57af436cf'),
(199, 'g', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 1, NULL, '2022-01-11 21:39:01', '2022-01-11 23:59:00', 45, NULL, 'g', '54ebd21241c7b3010962517b09eea22506247fd9ead2e6096cb2530f2baaab53'),
(200, 'z', 'z', 0, NULL, '2022-01-11 21:41:16', '2022-01-11 23:58:00', 45, 'z', NULL, 'f2507eeda6126a9af595cd960974fdab3502969742f02bb0d50f4297d04ed676'),
(201, 'aa', 'a', 0, NULL, '2022-01-12 16:44:16', '2022-01-12 17:50:00', 45, NULL, NULL, '676892d57e4a845090c32d02c4e0b21d715509862bd514422ceac9e5d3c4d7d3'),
(202, 'bb', 'bb', 1, NULL, '2022-01-12 16:44:51', '2022-01-12 17:50:00', 45, NULL, NULL, '0232f471b0812cba36c9566aa67b85482df4cda89ac1714c1eee6a792389740c'),
(203, 'cc', 'cc', 0, NULL, '2022-01-12 16:45:28', '2022-01-12 17:51:00', 45, NULL, NULL, '1fb9d6c5673f2e42b8b7a0cc8d9d681f9522aacb4205c62251c2420eb108dcf7'),
(205, 'Here', 'Private', 0, NULL, '2022-01-13 17:12:28', '2022-01-14 00:00:00', 45, NULL, NULL, '8d6d33aefed5815aaaf58b06a68f9321ddfffede5ad5dacfc7d4ab88f811d679');

-- --------------------------------------------------------

--
-- Ĺ truktĂşra tabuÄľky pre tabuÄľku `event_circle`
--

CREATE TABLE `event_circle` (
  `event_id` int(11) NOT NULL,
  `circle_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Ĺ truktĂşra tabuÄľky pre tabuÄľku `event_invitations`
--

CREATE TABLE `event_invitations` (
  `user_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `accepted` tinyint(1) NOT NULL DEFAULT '0',
  `invitation_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- SĹĄahujem dĂˇta pre tabuÄľku `event_invitations`
--

INSERT INTO `event_invitations` (`user_id`, `event_id`, `accepted`, `invitation_id`) VALUES
(29, 159, 0, 1),
(33, 159, 0, 2),
(34, 159, 0, 3),
(35, 159, 0, 4),
(36, 159, 0, 5),
(40, 69, 0, 16),
(45, 69, 0, 17),
(32, 69, 0, 18),
(29, 162, 0, 19),
(33, 162, 0, 20),
(34, 162, 0, 21),
(35, 162, 0, 22),
(36, 162, 0, 23),
(33, 163, 0, 25),
(29, 163, 0, 26),
(34, 163, 0, 27),
(35, 163, 0, 28),
(36, 163, 0, 29),
(29, 164, 0, 31),
(33, 164, 0, 32),
(34, 164, 0, 33),
(35, 164, 0, 34),
(36, 164, 0, 35),
(29, 75, 0, 55),
(33, 75, 0, 56),
(34, 75, 0, 57),
(35, 75, 0, 58),
(36, 75, 0, 59),
(40, 75, 0, 61),
(45, 75, 0, 62),
(32, 75, 0, 63),
(29, 183, 0, 66),
(33, 183, 0, 67),
(34, 183, 0, 68),
(40, 183, 0, 69),
(45, 183, 0, 70),
(32, 183, 0, 71),
(29, 157, 0, 78),
(33, 157, 0, 79),
(34, 157, 0, 80),
(35, 157, 0, 81),
(36, 157, 0, 82),
(40, 157, 0, 83),
(45, 157, 0, 84),
(32, 157, 0, 85),
(29, 184, 0, 86),
(33, 184, 0, 87),
(45, 184, 0, 89),
(32, 184, 0, 90),
(29, 7, 0, 91),
(40, 7, 0, 92),
(45, 7, 0, 93),
(32, 7, 1, 94),
(34, 194, 0, 95),
(35, 194, 0, 96),
(36, 194, 0, 97),
(40, 194, 0, 98),
(45, 194, 0, 99),
(32, 194, 1, 100),
(36, 195, 0, 101),
(35, 195, 0, 102),
(34, 195, 0, 103),
(40, 195, 0, 104),
(45, 195, 0, 105),
(32, 195, 0, 106),
(34, 196, 0, 107),
(35, 196, 0, 108),
(36, 196, 0, 109),
(48, 196, 0, 110),
(40, 196, 0, 111),
(45, 196, 1, 112),
(32, 196, 1, 113),
(34, 197, 0, 114),
(40, 197, 1, 115),
(45, 197, 1, 116);

-- --------------------------------------------------------

--
-- Ĺ truktĂşra tabuÄľky pre tabuÄľku `event_participants`
--

CREATE TABLE `event_participants` (
  `event_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- SĹĄahujem dĂˇta pre tabuÄľku `event_participants`
--

INSERT INTO `event_participants` (`event_id`, `user_id`) VALUES
(1, 29),
(2, 29),
(2, 32),
(11, 45),
(23, 29),
(23, 32),
(2, 40),
(12, 45),
(1, 49),
(1, 41),
(1, 44),
(1, 32),
(7, 32),
(10, 47),
(34, 45),
(4, 45),
(9, 45),
(35, 47),
(36, 47),
(37, 45),
(25, 32),
(38, 32),
(39, 32),
(40, 32),
(4, 32),
(41, 45),
(1, 33),
(10, 40),
(12, 40),
(42, 48),
(12, 41),
(43, 48),
(45, 40),
(42, 50),
(11, 41),
(6, 41),
(46, 32),
(44, 45),
(47, 32),
(48, 32),
(49, 32),
(51, 32),
(52, 32),
(54, 32),
(32, 32),
(62, 45),
(63, 40),
(64, 40),
(65, 40),
(66, 40),
(44, 32),
(44, 48),
(68, 48),
(38, 41),
(25, 51),
(32, 51),
(69, 51),
(70, 45),
(44, 41),
(71, 32),
(72, 32),
(73, 32),
(74, 32),
(75, 32),
(76, 32),
(77, 32),
(78, 32),
(79, 32),
(80, 32),
(81, 32),
(82, 32),
(83, 32),
(84, 32),
(85, 32),
(86, 32),
(87, 32),
(88, 32),
(89, 32),
(90, 32),
(92, 32),
(93, 32),
(94, 32),
(95, 32),
(96, 32),
(97, 32),
(98, 32),
(99, 32),
(100, 32),
(101, 32),
(102, 32),
(103, 32),
(104, 32),
(106, 32),
(107, 32),
(108, 32),
(109, 32),
(110, 32),
(111, 32),
(112, 32),
(113, 32),
(114, 32),
(116, 32),
(117, 32),
(118, 32),
(119, 32),
(120, 32),
(121, 32),
(122, 32),
(123, 32),
(124, 32),
(125, 32),
(126, 32),
(127, 32),
(128, 32),
(129, 32),
(130, 32),
(131, 32),
(132, 32),
(133, 32),
(134, 32),
(135, 32),
(136, 32),
(137, 32),
(138, 32),
(139, 32),
(140, 32),
(141, 32),
(142, 32),
(143, 32),
(144, 32),
(145, 32),
(146, 32),
(147, 32),
(148, 32),
(149, 32),
(150, 32),
(151, 32),
(152, 32),
(153, 32),
(160, 49),
(164, 32),
(165, 32),
(166, 32),
(167, 32),
(169, 32),
(170, 32),
(171, 32),
(172, 32),
(176, 32),
(177, 32),
(178, 32),
(179, 32),
(180, 32),
(181, 32),
(182, 32),
(183, 32),
(185, 32),
(113, 52),
(186, 45),
(85, 45),
(85, 49),
(187, 32),
(188, 32),
(189, 32),
(193, 32),
(194, 32),
(195, 32),
(196, 32),
(198, 32),
(10, 45),
(34, 32),
(42, 32),
(1, 45),
(201, 45),
(202, 45),
(203, 45),
(197, 45),
(196, 45),
(205, 45);

-- --------------------------------------------------------

--
-- Ĺ truktĂşra tabuÄľky pre tabuÄľku `feed`
--

CREATE TABLE `feed` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `creator` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- SĹĄahujem dĂˇta pre tabuÄľku `feed`
--

INSERT INTO `feed` (`id`, `event_id`, `message`, `created_at`, `creator`) VALUES
(1, 10, 'feed1', '2022-01-07 10:38:06', 40),
(2, 10, 'feed2', '2022-01-07 10:38:06', 40),
(3, 10, 'feed3', '2022-01-07 10:38:31', 40),
(4, 10, 'feed4', '2022-01-09 13:21:16', 40),
(5, 10, 'feed5', '2022-01-09 13:21:16', 40),
(6, 10, 'feed6', '2022-01-09 13:21:29', 40),
(7, 10, 'feed7', '2022-01-09 13:21:29', 40),
(20, 10, 'feed8', '2022-01-09 16:36:34', 40),
(21, 10, 'feed9', '2022-01-09 16:37:15', 40),
(22, 64, 'aaa', '2022-01-09 16:57:39', 40),
(23, 12, 'qwer', '2022-01-09 16:59:08', 34),
(25, 10, 'event10', '2022-01-09 17:18:57', 40),
(26, 1, '!inportant', '2022-01-10 19:33:37', 45),
(27, 189, 'as', '2022-01-12 22:14:15', 32);

-- --------------------------------------------------------

--
-- Ĺ truktĂşra tabuÄľky pre tabuÄľku `friends`
--

CREATE TABLE `friends` (
  `user_id` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- SĹĄahujem dĂˇta pre tabuÄľku `friends`
--

INSERT INTO `friends` (`user_id`, `friend_id`) VALUES
(29, 32),
(32, 34),
(32, 35),
(32, 36),
(40, 36),
(40, 41),
(40, 32),
(45, 36),
(45, 32),
(32, 48),
(32, 41),
(32, 33),
(32, 44),
(32, 49),
(32, 29),
(32, 40);

-- --------------------------------------------------------

--
-- Ĺ truktĂşra tabuÄľky pre tabuÄľku `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `user` int(11) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `chat_room` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Ĺ truktĂşra tabuÄľky pre tabuÄľku `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `type` enum('EVENT','CIRCLE','CHAT') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- SĹĄahujem dĂˇta pre tabuÄľku `notification`
--

INSERT INTO `notification` (`id`, `user_id`, `message`, `type`, `created_at`) VALUES
(22, 40, 'asaggagags', 'EVENT', '2021-12-15 13:18:12'),
(23, 40, 'hdhdgdgdh', 'CIRCLE', '2021-12-15 13:18:12'),
(26, 45, 'New feed message for event EPIC Prague', 'EVENT', '2022-01-09 16:18:57'),
(28, 34, 'You have been invited to join event test event 2022-01-10', 'EVENT', '2022-01-10 11:43:47'),
(29, 35, 'You have been invited to join event test event 2022-01-10', 'EVENT', '2022-01-10 11:43:47'),
(30, 36, 'You have been invited to join event test event 2022-01-10', 'EVENT', '2022-01-10 11:43:47'),
(31, 36, 'You have been invited to join event a', 'EVENT', '2022-01-10 12:22:06'),
(32, 35, 'You have been invited to join event a', 'EVENT', '2022-01-10 12:22:06'),
(33, 34, 'You have been invited to join event a', 'EVENT', '2022-01-10 12:22:06'),
(34, 34, 'You have been invited to join event test1-38-pm', 'EVENT', '2022-01-10 12:38:15'),
(35, 35, 'You have been invited to join event test1-38-pm', 'EVENT', '2022-01-10 12:38:15'),
(36, 36, 'You have been invited to join event test1-38-pm', 'EVENT', '2022-01-10 12:38:15'),
(37, 48, 'You have been invited to join event test1-38-pm', 'EVENT', '2022-01-10 12:38:15'),
(38, 34, 'You have been invited to join event a', 'EVENT', '2022-01-10 13:40:44'),
(39, 29, 'New feed message for event Beer pong', 'EVENT', '2022-01-10 18:33:37'),
(40, 49, 'New feed message for event Beer pong', 'EVENT', '2022-01-10 18:33:37'),
(41, 41, 'New feed message for event Beer pong', 'EVENT', '2022-01-10 18:33:37'),
(42, 44, 'New feed message for event Beer pong', 'EVENT', '2022-01-10 18:33:37'),
(44, 45, 'New feed message for event Beer pong', 'EVENT', '2022-01-10 18:33:37'),
(45, 33, 'New feed message for event Beer pong', 'EVENT', '2022-01-10 18:33:37');

-- --------------------------------------------------------

--
-- Ĺ truktĂşra tabuÄľky pre tabuÄľku `password_reset_token`
--

CREATE TABLE `password_reset_token` (
  `user_id` int(11) NOT NULL,
  `token` varchar(128) NOT NULL,
  `token_expiry` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- SĹĄahujem dĂˇta pre tabuÄľku `password_reset_token`
--

INSERT INTO `password_reset_token` (`user_id`, `token`, `token_expiry`) VALUES
(29, '$2b$10$oMcx8dyzknLje9bnviHqluncawZ0mo6QTSWrWkbmBQkhcSGDVu60K', 1635711370987),
(32, '$2b$10$jegzbKWx1ZESeoyCz6HQzebsKJyFNZVRWktLvyt6yA9kg6Bn0y7fS', 1637067143161),
(47, '$2b$10$j8BoH9w1BOUwiGjlf4AbYeVsNzVSxDBUM8zCBlVGocDuajC8ScwCu', 1636995530370);

-- --------------------------------------------------------

--
-- Ĺ truktĂşra tabuÄľky pre tabuÄľku `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `business` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `profilePictureUrl` varchar(512) DEFAULT 'https://www.w3schools.com/howto/img_avatar.png',
  `description` varchar(2000) DEFAULT 'Your profile description.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- SĹĄahujem dĂˇta pre tabuÄľku `user`
--

INSERT INTO `user` (`id`, `first_name`, `last_name`, `email`, `password`, `business`, `created_at`, `verified`, `profilePictureUrl`, `description`) VALUES
(29, 'John', 'Smith', 'test@test.cz', '$2b$10$9kMKEIlXFh.OOnMQv3yf0u3DkenAb6B8L6aDBrwHVew.qz6g5vjX.', 0, '2021-10-31 19:15:09', 1, NULL, 'My profile description.'),
(32, 'Janko', 'HraĹˇko', 'chis00@vse.cz', '$2b$10$JSXPawwF6BWK3IruocNIb.t7rBVXMPnFt6g7X.NI3M1GWzBOcvho6', 0, '2021-11-01 09:01:04', 1, 'https://static.onecms.io/wp-content/uploads/sites/34/2018/05/12170411/cat-kitten-138468381.jpg', NULL),
(33, 'LukĂˇĹˇ', 'SuĹˇila', 'dsadasdadasdas', NULL, NULL, '2021-11-01 14:58:08', 1, NULL, 'Your profile description.'),
(34, 'Alena', 'Ĺ˝eleznĂˇ', 'dsad', NULL, NULL, '2021-11-01 14:58:35', 1, NULL, 'Your profile description.'),
(35, 'Lado', 'HeĹˇĂ­k', 'aaa@bbb.cz', NULL, NULL, '2021-11-01 14:58:35', 1, NULL, 'Your profile description.'),
(36, 'KlĂˇra', 'FojtĂ­kovĂˇ', 'fdsfds', NULL, NULL, '2021-11-01 14:58:35', 1, NULL, 'Your profile description.'),
(40, 'Marcel', 'FarkaĹˇ', 'farm05@vse.cz', '$2b$10$0sKH18H0cLqnZbRBdiKDXu5awqRtMcCdut3pHrwnGrcoIKWXE.cma', 0, '2021-11-12 18:25:32', 1, 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRwbxQS54h58KMMNXAGl1DE3rTdS6h_7n2VgjKgTL3jUZwHZOYVfeSBmNOJ7FG87vdlUSE&usqp=CAU', 'Your profile description!!!'),
(41, 'Pavel', 'KoĹĄĂˇtko', 'novd13@vse.cz', '$2b$10$el.Q1U10cu.tGDithIUaBuM4XeCwvbD0JTnhBrHgymxs75Rdf88Ga', 0, '2021-11-12 18:48:23', 1, 'https://i.guim.co.uk/img/media/26392d05302e02f7bf4eb143bb84c8097d09144b/446_167_3683_2210/master/3683.jpg?width=1200&height=1200&quality=85&auto=format&fit=crop&s=49ed3252c0b2ffb49cf8b508892e452d', 'Helloooo wooordlll make if full of kitteeeennnssss'),
(42, 'Pepe', 'Havaj', 'mojemama@hello.world', '$2b$10$NwV4x4ViqI0SN.dwTUhV7OkbK0.ueMCPhTSuZnl0iJTWi1EVEHRge', 0, '2021-11-12 22:27:42', 1, 'https://media.wired.com/photos/5cdefb92b86e041493d389df/2:1/w_1500,h_750,c_limit/Culture-Grumpy-Cat-487386121.jpg', 'Your profile description.'),
(43, 'K', 'Kk', 'hroj06@vse.cz', '$2b$10$cNSgF.tLB2kfNRliK6g0GejluMEbIRh25fvjtLoWRVFmINQj6bqFa', 1, '2021-11-14 23:34:12', 1, NULL, 'Your profile description.'),
(44, 'Bruce', 'Wayne', 'marm30@vse.cz', '$2b$10$D16sQEkI.OkQ6IAE8hHesevcbAuRYkD5E.PPazVEzhMcOo7iwkLPC', 0, '2021-11-14 23:58:03', 0, NULL, 'Your profile description.'),
(45, 'Lucie', 'MarkovĂˇ', 'lucienne.markova@gmail.com', '$2b$10$080XyoEVrBKMx.186djrbe6gaKEehbl.vuKypuTNlorgVdY1zjMZ6', 0, '2021-11-15 10:22:13', 1, 'https://scontent.fprg5-1.fna.fbcdn.net/v/t1.18169-9/13566940_10204476784640180_5370938422310248585_n.jpg?_nc_cat=100&ccb=1-5&_nc_sid=09cbfe&_nc_ohc=hLQY6DpRaB4AX_bXVUm&_nc_ht=scontent.fprg5-1.fna&oh=00_AT_4UJQaJCG_u-Q_HRlVPr2L0M2CR0-TymiK_JglruhlZw&oe=62006CB5', 'Hellooooo fjdslifjoiwe fndihfkjsdnf se fwesojdfhskjdfh Ă­waeiuf sdkjhfwieoufh sdkjfn wweurhseodf ksnenrwĂ­euf sdf'),
(46, 'Luc', 'Mar', 'markova.666@seznam.cz', '$2b$10$n5KyagCHm1v9KGsWMOfDnuvLiGJXH8gdggsKY4VS.9VgyfKKqxryW', 0, '2021-11-15 11:28:33', 1, NULL, 'Your profile description.'),
(47, 'Lulu', 'Mama', 'marl22@vse.cz', '$2b$10$xBe6J2KKXrBIg.HJ40UNUeQEArqaLoDHoGo9KkyVs2hOhVhgtGofS', 0, '2021-11-15 11:30:31', 1, NULL, 'Your profile description.'),
(48, 'Miisa', 'Krivankova', 'krim12@vse.cz', '$2b$10$BotOQYf9w0gT6sVQuFlt2etfVJ1zMdErEi1AAb6Rb0h0DPbAE2Q4W', 0, '2021-11-17 08:36:59', 1, 'https://thumbs.dreamstime.com/z/disney-vector-illustration-mickey-mouse-isolated-white-background-who-laughs-closeup-character-smiles-big-eyes-165067930.jpg', 'Just keep smiling:)'),
(49, 'alert(Â´zdenka', 'fialovĂˇÂ´)', 'fialova@cngroup.dk', '$2b$10$HiNLfzXxzltgReJ7U2JDcu8OEkCa9v1csK6KIuwto6RVjvtJVTaCG', 0, '2021-11-18 11:51:12', 1, 'https://www.w3schools.com/howto/img_avatar.png', 'Your profile de'),
(50, 'Daniel', 'LaĹˇko', 'miisa.krivankova@gmail.com', '$2b$10$lWfRsXectxTG5Qq3GhW8cec5Wd3gpGIHR8lamevFQRvZwlBD.rK8y', 0, '2021-11-29 09:43:27', 1, 'https://i.colnect.net/f/4278/058/Lasko-Zlatorog-svetlo.jpg', 'Party man!'),
(51, 'Adam', 'JedliÄŤka', 'adajedlicka@gmail.com', '$2b$10$vYyRE9GXrRj3tmqWJ1BV1uYFw88M.LU.RDTLytWNZauttO65qTzjy', 0, '2021-11-29 16:03:57', 1, 'https://www.w3schools.com/howto/img_avatar.png', 'Your profile description.'),
(52, 'Daniel', 'Nemec', 'deny.nemec@gmail.com', '$2b$10$tEp8vd9Xq6FBHGZIgnpiHOC.bBULo0WU0mUy3kljKxT4KzfsX8mvK', 0, '2021-12-14 16:44:30', 1, 'https://www.w3schools.com/howto/img_avatar.png', 'Your profile description.'),
(53, 'Daniel', 'Nemec', 'nemec.daniel@seznam.cz', '$2b$10$gfsz/W0nJzCiQp/hOrWkh.B13uYJw6b3ZdFJVE2sXnSQivncQz3MS', 0, '2021-12-14 17:03:10', 1, 'https://www.w3schools.com/howto/img_avatar.png', 'Your profile description.'),
(54, 'Ayaya', 'Ayaya', 'jezeveckarel@gmail.com', '$2b$10$G4qZHUPJSHRRdaAGZvJFs.GdelchEa22uMSO03sSvYG0UzHQ5z9je', 0, '2022-01-10 14:24:39', 0, 'https://www.w3schools.com/howto/img_avatar.png', 'Your profile description.');

--
-- KÄľĂşÄŤe pre exportovanĂ© tabuÄľky
--

--
-- Indexy pre tabuÄľku `business`
--
ALTER TABLE `business`
  ADD PRIMARY KEY (`id`),
  ADD KEY `owner` (`owner`);

--
-- Indexy pre tabuÄľku `chat_room`
--
ALTER TABLE `chat_room`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event` (`event`);

--
-- Indexy pre tabuÄľku `circle`
--
ALTER TABLE `circle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `owner` (`owner`);

--
-- Indexy pre tabuÄľku `circle_users`
--
ALTER TABLE `circle_users`
  ADD KEY `circle_id` (`circle_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexy pre tabuÄľku `email_verification_token`
--
ALTER TABLE `email_verification_token`
  ADD PRIMARY KEY (`user_id`,`token`),
  ADD UNIQUE KEY `token` (`token`);

--
-- Indexy pre tabuÄľku `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `chatroom_id` (`chatroom_id`);

--
-- Indexy pre tabuÄľku `event_circle`
--
ALTER TABLE `event_circle`
  ADD UNIQUE KEY `event_id` (`event_id`),
  ADD UNIQUE KEY `circle_id` (`circle_id`);

--
-- Indexy pre tabuÄľku `event_invitations`
--
ALTER TABLE `event_invitations`
  ADD PRIMARY KEY (`invitation_id`),
  ADD UNIQUE KEY `unique user+event` (`user_id`,`event_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `hehe` (`event_id`) USING BTREE;

--
-- Indexy pre tabuÄľku `event_participants`
--
ALTER TABLE `event_participants`
  ADD KEY `user_id` (`user_id`),
  ADD KEY `event_id` (`event_id`);

--
-- Indexy pre tabuÄľku `feed`
--
ALTER TABLE `feed`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `event_id` (`event_id`),
  ADD KEY `creator` (`creator`);

--
-- Indexy pre tabuÄľku `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`),
  ADD KEY `chat_room` (`chat_room`);

--
-- Indexy pre tabuÄľku `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `user_id_fk` (`user_id`);

--
-- Indexy pre tabuÄľku `password_reset_token`
--
ALTER TABLE `password_reset_token`
  ADD PRIMARY KEY (`user_id`,`token`),
  ADD UNIQUE KEY `token` (`token`);

--
-- Indexy pre tabuÄľku `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT pre exportovanĂ© tabuÄľky
--

--
-- AUTO_INCREMENT pre tabuÄľku `business`
--
ALTER TABLE `business`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pre tabuÄľku `chat_room`
--
ALTER TABLE `chat_room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuÄľku `circle`
--
ALTER TABLE `circle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pre tabuÄľku `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=206;
--
-- AUTO_INCREMENT pre tabuÄľku `event_invitations`
--
ALTER TABLE `event_invitations`
  MODIFY `invitation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;
--
-- AUTO_INCREMENT pre tabuÄľku `feed`
--
ALTER TABLE `feed`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT pre tabuÄľku `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuÄľku `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT pre tabuÄľku `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- Obmedzenie pre exportovanĂ© tabuÄľky
--

--
-- Obmedzenie pre tabuÄľku `business`
--
ALTER TABLE `business`
  ADD CONSTRAINT `business_ibfk_1` FOREIGN KEY (`owner`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Obmedzenie pre tabuÄľku `chat_room`
--
ALTER TABLE `chat_room`
  ADD CONSTRAINT `chat_room_ibfk_1` FOREIGN KEY (`event`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Obmedzenie pre tabuÄľku `circle`
--
ALTER TABLE `circle`
  ADD CONSTRAINT `circle_ibfk_1` FOREIGN KEY (`owner`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Obmedzenie pre tabuÄľku `circle_users`
--
ALTER TABLE `circle_users`
  ADD CONSTRAINT `circle_users_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `circle_users_ibfk_2` FOREIGN KEY (`circle_id`) REFERENCES `circle` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Obmedzenie pre tabuÄľku `event_invitations`
--
ALTER TABLE `event_invitations`
  ADD CONSTRAINT `event_invitations_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `event_invitations_ibfk_2` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Obmedzenie pre tabuÄľku `event_participants`
--
ALTER TABLE `event_participants`
  ADD CONSTRAINT `event_participants_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `event_participants_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Obmedzenie pre tabuÄľku `feed`
--
ALTER TABLE `feed`
  ADD CONSTRAINT `feed_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`),
  ADD CONSTRAINT `feed_ibfk_2` FOREIGN KEY (`creator`) REFERENCES `user` (`id`);

--
-- Obmedzenie pre tabuÄľku `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `message_ibfk_1` FOREIGN KEY (`chat_room`) REFERENCES `chat_room` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `message_ibfk_2` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Obmedzenie pre tabuÄľku `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
