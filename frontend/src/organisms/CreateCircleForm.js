import React from 'react';
import { Form, Formik } from 'formik';
import * as yup from 'yup';

import { Button, CheckboxButton } from 'src/atoms';
import { FormikField } from 'src/molecules';

const initialValues = {
  name: '',
};

const schema = yup.object().shape({
  name: yup.string().required().label('Circle name'),
});

export function CreateCircleForm({
  onSubmit,
  loading,
  friendsData,
  selectedFriendsChange,
}) {
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={schema}
      validateOnBlur={false}
      onSubmit={onSubmit}
    >
      <Form>
        <FormikField
          id="name"
          label="Circle name"
          name="name"
          type="text"
          autoFocus="autofocus"
          autoComplete="off"
          autoCorrect="off"
          autoCapitalize="off"
        />
        <h3>Invite your friends</h3>
        <div
          style={{
            overflowY: 'auto',
            display: 'flex',
            width: '80%',
            margin: 'auto',
            flexDirection: 'column',
          }}
        >
          {friendsData.friends.map((friend) => (
            <CheckboxButton
              key={friend.id}
              text={`${friend.first_name} ${friend.last_name}`}
              name="invitedFriends"
              value={friend.id}
              handleChange={(checked, value) =>
                selectedFriendsChange(checked, value)
              }
            />
          ))}
        </div>
        <Button type="submit" disabled={loading}>
          Create
        </Button>
      </Form>
    </Formik>
  );
}
