import React, { useCallback, useState } from 'react';
import { gql, useMutation, useQuery } from '@apollo/client';

import { Dialog, CreateCircleForm } from 'src/organisms';
import { showToast } from 'src/utils/showToast';

const CREATE_CIRCLE_MUTATION = gql`
  mutation CreateCircle($name: String!, $invitedFriends: [Int]!) {
    createCircle(name: $name, invitedFriends: $invitedFriends) {
      id
      name
    }
  }
`;

const FRIENDS_QUERY = gql`
  query Friends {
    friends {
      id
      first_name
      last_name
      profilePictureUrl
    }
  }
`;

export function CreateCircleDialog({
  isOpen,
  contentLabel,
  closeDialog,
  refetchData,
}) {
  const { data: friendsData } = useQuery(FRIENDS_QUERY);
  const [selectedFriends, setSelectedFriends] = useState([]);

  const [createCircle] = useMutation(CREATE_CIRCLE_MUTATION, {
    onCompleted: ({ name }) => {
      closeDialog();
      refetchData();
      showToast({
        type: 'SUCCESS',
        msg: `Circle ${name} created successfully`,
      });
    },
    onError: (error) => showToast({ type: 'ERROR', msg: error.message }),
  });

  const handleFriendChange = (checked, value) => {
    if (checked) {
      setSelectedFriends((prev) => [...prev, value]);
    } else {
      setSelectedFriends((prev) => prev.filter((item) => item !== value));
    }
  };

  const handleCreateCircleFormSubmit = useCallback(
    (variables) => {
      let current;
      setSelectedFriends((currentState) => {
        current = currentState;
        return currentState;
      });

      createCircle({
        variables: {
          ...variables,
          invitedFriends: current,
        },
      });
    },
    [createCircle],
  );

  return (
    <>
      <Dialog
        isOpen={isOpen}
        closeDialog={() => {
          closeDialog();
        }}
        contentLabel={contentLabel}
        header="Create circle"
      >
        <CreateCircleForm
          onSubmit={handleCreateCircleFormSubmit}
          friendsData={friendsData}
          selectedFriendsChange={(checked, value) =>
            handleFriendChange(checked, value)
          }
        />
      </Dialog>
    </>
  );
}
