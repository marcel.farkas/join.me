import React from 'react';
import { gql, useMutation } from '@apollo/client';

import Img from 'src/images/Background.png';
import {
  Button,
  CalendarIcon,
  ParticipantsIcon,
  PinIcon,
  ProfilePicture,
} from 'src/atoms';
import { showToast } from 'src/utils/showToast';
import { ProfilePictureLinkWrapper } from 'src/molecules';

const JOIN_EVENT_MUTATION = gql`
  mutation JoinEvent($eventId: Int!) {
    joinEvent(eventId: $eventId) {
      id
      name
    }
  }
`;

const LEAVE_EVENT_MUTATION = gql`
  mutation LeaveEvent($eventId: Int!) {
    leaveEvent(eventId: $eventId) {
      id
      name
    }
  }
`;

const DECLINE_EVENT_MUTATION = gql`
  mutation DeclineInvitation($eventId: Int!) {
    declineInvitation(eventId: $eventId) {
      id
      name
    }
  }
`;

export function EventDetails({
  event,
  loading,
  error,
  joinedEvents,
  refetchData,
  isInvPage,
  currentUserId,
}) {
  const [joinEvent] = useMutation(JOIN_EVENT_MUTATION, {
    onCompleted: ({ joinEvent: { name } }) => {
      if (isInvPage)
        showToast({
          type: 'SUCCESS',
          msg: `You accepted invitation to event ${name}`,
        });
      else showToast({ type: 'SUCCESS', msg: `You joined event ${name}` });
      refetchData();
    },
    onError: (error) => showToast({ type: 'ERROR', msg: error.message }),
  });

  const [leaveEvent] = useMutation(LEAVE_EVENT_MUTATION, {
    onCompleted: ({ leaveEvent: { name } }) => {
      showToast({ type: 'SUCCESS', msg: `You left event ${name}` });
      refetchData();
    },
    onError: (error) => showToast({ type: 'ERROR', msg: error.message }),
  });

  const [declineInvitation] = useMutation(DECLINE_EVENT_MUTATION, {
    onCompleted: ({ declineInvitation: { name } }) => {
      showToast({
        type: 'SUCCESS',
        msg: `You declined invitation to event ${name}`,
      });
      refetchData();
    },
    onError: (error) => {
      showToast({ type: 'ERROR', msg: error.message });
      refetchData();
    },
  });

  const handleJoinButtonClick = (eventId) => {
    joinEvent({ variables: { eventId } });
  };

  const handleLeaveButtonClick = (eventId) => {
    leaveEvent({ variables: { eventId } });
  };

  const handleDeclineButtonClick = (eventId) => {
    declineInvitation({ variables: { eventId } });
  };

  const AcceptDeclineBtn = (eventId) => {
    if (isInvPage) {
      return (
        <div>
          <Button onClick={() => handleJoinButtonClick(eventId)}>Accept</Button>
          <Button onClick={() => handleDeclineButtonClick(eventId)}>
            Decline
          </Button>
        </div>
      );
    }
  };

  console.log('event', event);

  const JoinBtn = ({ eventId, isPrivate }) => {
    if (joinedEvents) {
      const joinedEventsIds = joinedEvents.map((event) => event.id);

      if (joinedEventsIds.includes(eventId)) {
        return (
          <Button
            className="btn_pink"
            onClick={() => handleLeaveButtonClick(eventId)}
          >
            Leave
          </Button>
        );
      }
    }

    if (isInvPage) return;

    return (
      <Button
        className="btn_pink"
        onClick={() => handleJoinButtonClick(eventId)}
      >
        {currentUserId === event.creator_id
          ? 'Join'
          : isPrivate
          ? 'Send request'
          : 'Join'}
      </Button>
    );
  };

  return (
    <div>
      <h1>{event.name}</h1>
      <div>
        <img src={event.eventPictureUrl || Img} alt="Event" />
      </div>

      <span className="baba">
        <span className="createdby">Created by:&nbsp;</span>
        <div className="createdby2">
          <ProfilePicture
            key={event.creator.id}
            profilePictureUrl={event.creator.profilePictureUrl}
            loading={loading}
            error={error}
            imageSize={10}
            className="profil"
          />
          <span className="bold createdby">
            &nbsp;
            {event.creator.first_name} {event.creator.last_name}
          </span>
        </div>
      </span>
      <p>
        <CalendarIcon />
        {new Date(parseFloat(event.eventDateTime)).toLocaleString()}
      </p>
      <p>
        <PinIcon />
        {event.place}
      </p>
      <div className="participants">
        <ParticipantsIcon />
        <div className="people">
          {event.participants.length ? (
            event.participants.map((participant) => (
              <ProfilePictureLinkWrapper
                key={participant.id}
                profilePictureUrl={participant.profilePictureUrl}
                loading={loading}
                error={error}
                imageSize={10}
                className="profil"
                user={participant.id}
                currentUserId={currentUserId}
              />
            ))
          ) : (
            <span>No participants</span>
          )}
        </div>
      </div>
      <div>
        <p>
          <span className="bold">Description: </span>
          <p>{event.information}</p>
        </p>
      </div>
      {event && JoinBtn({ eventId: event.id, isPrivate: event.private })}
      {isInvPage && AcceptDeclineBtn(event.id)}
    </div>
  );
}
