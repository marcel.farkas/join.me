import React from 'react';

import { Dialog, EventDetails } from 'src/organisms';

export function EventDetailsDialog({
  isOpen,
  contentLabel,
  event,
  loading,
  error,
  setIsEventDetailsDialogOpen,
  joinedEvents,
  refetchData,
  isInvPage,
  currentUserId,
}) {
  return (
    <>
      <Dialog
        className="event_detail"
        isOpen={isOpen}
        closeDialog={() => setIsEventDetailsDialogOpen(false)}
        contentLabel={contentLabel}
      >
        <EventDetails
          event={event}
          loading={loading}
          error={error}
          joinedEvents={joinedEvents}
          refetchData={refetchData}
          isInvPage={isInvPage}
          currentUserId={currentUserId}
        />
      </Dialog>
    </>
  );
}
