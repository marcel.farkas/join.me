import React, { useState, useEffect, useRef } from 'react';
import { gql, useQuery, useMutation } from '@apollo/client';

import { Dialog } from 'src/organisms';
import { SendMessageForm } from 'src/molecules';

const DATA_QUERY = gql`
  query ($id: Int!) {
    eventFeed(id: $id) {
      id
      event_id
      message
      created_at
    }

    user {
      id
    }

    event(id: $id) {
      creator_id
    }
  }
`;

const ADD_FEED_MESSAGE_MUTATION = gql`
  mutation AddFeedMessage($eventId: Int!, $message: String!) {
    addFeedMessage(eventId: $eventId, message: $message) {
      id
    }
  }
`;

export function EventFeedDialog({
  isOpen,
  closeDialog,
  contentLabel,
  eventId,
}) {
  const feedBottomRef = useRef(null);
  const { data: feedData, refetch: refetchData } = useQuery(DATA_QUERY, {
    variables: { id: eventId },
  });
  const [isMyEvent, setIsMyEvent] = useState(false);

  const [addFeedMessage] = useMutation(ADD_FEED_MESSAGE_MUTATION);

  useEffect(() => {
    feedBottomRef.current?.scrollIntoView({ behavior: 'smooth' });
  });

  useEffect(() => {
    setIsMyEvent(feedData?.user?.id === feedData?.event?.creator_id);
  }, [feedData]);

  const handleSendMessage = async (evt) => {
    evt.preventDefault();

    const text = evt.target.messageInput.value;
    await addFeedMessage({ variables: { eventId, message: text } });
    refetchData();
    evt.target.reset();
  };

  return (
    <>
      <Dialog
        className="feed_dialog"
        isOpen={isOpen}
        closeDialog={closeDialog}
        contentLabel={contentLabel}
        header="Event feed"
      >
        <div style={{ overflow: 'auto', width: '100%' }}>
          {feedData &&
            feedData.eventFeed.map((data) => (
              <div
                key={data.id}
                style={{
                  backgroundColor: '#b524ff',
                  color: 'white',
                  padding: '10px 5px 10px 5px',
                  marginBottom: '10px',
                  borderRadius: '10px',
                  marginRight: '4px',
                }}
              >
                {data.message}
                <div style={{ fontSize: '12px', textAlign: 'right' }}>
                  {`${new Date(
                    data.created_at,
                  ).toLocaleDateString()} ${new Date(
                    data.created_at,
                  ).toLocaleTimeString()}`}
                </div>
              </div>
            ))}
          <div
            ref={feedBottomRef}
            style={{
              height: '0',
              width: '0',
              float: 'left',
              clear: 'both',
            }}
          ></div>
        </div>
        {isMyEvent && <SendMessageForm handleSendMessage={handleSendMessage} />}
      </Dialog>
    </>
  );
}
