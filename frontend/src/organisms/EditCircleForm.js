import React from 'react';
import { Form, Formik } from 'formik';
import * as yup from 'yup';

import { Button, CheckboxButton } from 'src/atoms';
import { FormikField } from 'src/molecules';
//import { showToast } from 'src/utils/showToast';

const schema = yup.object().shape({
  name: yup.string().required().label('Circle name'),
});

/*const onFriendAdd = (friend, selectedFriends) =>
  selectedFriends.indexOf(friend) === -1
    ? (selectedFriends.push(friend),
      showToast({
        type: 'SUCCESS',
        msg: `User ${friend.first_name} ${friend.last_name} added`,
      }))
    : (selectedFriends.pop(friend),
      showToast({
        type: 'SUCCESS',
        msg: `User ${friend.first_name} ${friend.last_name} removed`,
      }));*/

export function EditCircleForm({
  onSubmit,
  loading,
  initialData: { circle },
  friendsData,
  selectedFriendsChange,
}) {
  return (
    <Formik
      initialValues={{
        ...circle,
      }}
      validationSchema={schema}
      validateOnBlur={false}
      onSubmit={onSubmit}
    >
      <Form>
        <FormikField
          id="name"
          label="Circle name"
          name="name"
          type="text"
          autoFocus="autofocus"
          autoComplete="autocomplete"
          autoCorrect="off"
          autoCapitalize="off"
        />
        <h3>Invite your friends</h3>
        <div
          style={{
            overflowY: 'auto',
            display: 'flex',
            width: '80%',
            margin: 'auto',
            flexDirection: 'column',
          }}
        >
          {friendsData.friends.map((friend) => (
            <CheckboxButton
              key={friend.id}
              text={`${friend.first_name} ${friend.last_name}`}
              name="invitedFriends"
              value={friend.id}
              handleChange={(checked, value) =>
                selectedFriendsChange(checked, value)
              }
            />
          ))}
        </div>
        <Button type="submit" disabled={loading}>
          Save changes
        </Button>
      </Form>
    </Formik>
  );
}
