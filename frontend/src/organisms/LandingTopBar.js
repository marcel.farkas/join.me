import React from 'react';
import { Button } from 'src/atoms/';

export function LandingTopBar({ openDialog }) {
  return (
    <div>
      <div id="JOINME" className="mont left">
        JOIN.ME
      </div>
      <div className="mont right">
        <Button
          className="btn_lp mont"
          animated
          onClick={() => openDialog('signIn')}
        >
          Sign in
        </Button>
      </div>
    </div>
  );
}
