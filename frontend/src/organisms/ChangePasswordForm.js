import React from 'react';
import { Form, Formik } from 'formik';
import * as yup from 'yup';

import { Button } from 'src/atoms';
import { FormikField } from 'src/molecules';

const initialValues = {
  currPassword: '',
  newPassword: '',
  newPasswordConfirm: '',
};

const schema = yup.object().shape({
  currPassword: yup.string(),
  newPassword: yup
    .string()
    .matches(
      '(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}',
      'Password must be at least 8 characters long. It must contain a lowercase character, an uppercase character and a number.',
    ),
  newPasswordConfirm: yup
    .string()
    .oneOf([yup.ref('newPassword'), null], 'Passwords must match')
    .required()
    .label('Confirm password'),
});

export function ChangePasswordForm({ changePassword }) {
  return (
    <Formik
      initialValues={{ ...initialValues }}
      validationSchema={schema}
      validateOnBlur={false}
      onSubmit={(variables, { resetForm }) => {
        changePassword(variables);
        resetForm();
      }}
    >
      <Form key="password-change">
        <h3>Change Password</h3>

        <FormikField
          id="currPassword"
          label="Current Password"
          name="currPassword"
          type="password"
          autoComplete="off"
          autoCorrect="off"
          autoCapitalize="off"
        />
        <FormikField
          id="newPassword"
          label="New Password"
          name="newPassword"
          type="password"
          autoComplete="off"
          autoCorrect="off"
          autoCapitalize="off"
        />
        <FormikField
          id="newPasswordConfirm"
          label="Confirm password"
          name="newPasswordConfirm"
          type="password"
          autoComplete="off"
          autoCorrect="off"
          autoCapitalize="off"
        />
        <Button type="submit">Change Password</Button>
      </Form>
    </Formik>
  );
}
