import React from 'react';

import { Dialog, EventCalendar } from 'src/organisms';
import 'react-big-calendar/lib/css/react-big-calendar.css';

export function EventCalendarDialog({
  isOpen,
  contentLabel,
  setIsEventCalendarDialogOpen,
  events,
  currentUserId,
  refetchData,
  loading,
  error,
}) {
  return (
    <>
      <Dialog
        className="calendar"
        isOpen={isOpen}
        closeDialog={() => setIsEventCalendarDialogOpen(false)}
        contentLabel={contentLabel}
        header="Calendar"
      >
        <EventCalendar
          events={events}
          currentUserId={currentUserId}
          refetchData={refetchData}
          loading={loading}
          error={error}
        />
      </Dialog>
    </>
  );
}
