import React, { useState, useEffect, useCallback } from 'react';
import { gql, useMutation, useQuery } from '@apollo/client';
import ReactPaginate from 'react-paginate';

import { Button } from 'src/atoms';
import { showToast } from 'src/utils/showToast';
import { Notification } from '../molecules/Notification';

const USER_NOTIFICATIONS_QUERY = gql`
  query UserNotifications {
    userNotifications {
      id
      message
      type
    }
  }
`;

const DELETE_NOTIFICATION_MUTATION = gql`
  mutation DeleteNotification($id: Int!) {
    deleteNotification(id: $id)
  }
`;

const DELETE_ALL_MUTATION = gql`
  mutation DeleteAllNotifications {
    deleteAllNotifications
  }
`;

const itemsPerPage = 10;

export function NotificationList() {
  const { data, refetch } = useQuery(USER_NOTIFICATIONS_QUERY);
  const [currentItems, setCurrentItems] = useState(null);
  const [pageCount, setPageCount] = useState(0);
  const [itemOffset, setItemOffset] = useState(0);

  useEffect(() => {
    if (data) {
      const endOffset = itemOffset + itemsPerPage;
      setCurrentItems(data.userNotifications.slice(itemOffset, endOffset));
      setPageCount(Math.ceil(data.userNotifications.length / itemsPerPage));
    }
  }, [itemOffset, itemsPerPage, data]);

  const handlePageClick = (event) => {
    const newOffset =
      (event.selected * itemsPerPage) % data.userNotifications.length;
    setItemOffset(newOffset);
  };

  const [deleteNotification] = useMutation(DELETE_NOTIFICATION_MUTATION, {
    onCompleted: () => {
      refetch();
    },
    onError: () =>
      showToast({ type: 'ERROR', msg: 'Notification could not be deleted' }),
  });

  const handleDeleteNotification = useCallback(
    (id) => {
      deleteNotification({ variables: { id } });
    },
    [deleteNotification],
  );

  const [deleteAll] = useMutation(DELETE_ALL_MUTATION, {
    onCompleted: () => {
      refetch();
    },
    onError: () =>
      showToast({ type: 'ERROR', msg: 'Notifications could not be deleted' }),
  });

  const handleDeleteAll = useCallback(() => {
    deleteAll();
  }, [deleteAll]);

  return (
    <div className="notif_page">
      <div style={{ color: 'white', fontSize: '2em', marginTop: '10px' }}>
        Notifications
      </div>
      {currentItems?.length ? (
        <Button className="btn clear_all" onClick={handleDeleteAll}>
          Clear all
        </Button>
      ) : null}
      <div>
        {currentItems &&
          currentItems.map((item) => (
            <Notification
              key={item.id}
              data={item}
              deleteNotification={handleDeleteNotification}
            />
          ))}
        <ReactPaginate
          breakLabel="..."
          nextLabel=">"
          previousLabel="<"
          onPageChange={handlePageClick}
          pageRangeDisplayed={10}
          pageCount={pageCount}
          renderOnZeroPageCount={null}
          containerClassName={'pagination'}
          activeClassName={'active'}
        />
      </div>
    </div>
  );
}
