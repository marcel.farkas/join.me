import React, { useState, useEffect } from 'react';
import ReactPaginate from 'react-paginate';

import { PublicEvent } from 'src/molecules/';

export function PublicEventList({
  data,
  joinedEvents,
  itemsPerPage,
  refetchData,
  currentUserId,
}) {
  const [currentItems, setCurrentItems] = useState(null);
  const [pageCount, setPageCount] = useState(0);
  const [itemOffset, setItemOffset] = useState(0);

  useEffect(() => {
    if (data) {
      const endOffset = itemOffset + itemsPerPage;
      setCurrentItems(data.events.slice(itemOffset, endOffset));
      setPageCount(Math.ceil(data.events.length / itemsPerPage));
    }
  }, [itemOffset, itemsPerPage, data]);

  const handlePageClick = (event) => {
    const newOffset = (event.selected * itemsPerPage) % data.events.length;
    setItemOffset(newOffset);
  };

  return (
    <>
      <div className="event_page">
        {currentItems &&
          currentItems.map((item) => (
            <PublicEvent
              key={item.id}
              event={item}
              joinedEvents={joinedEvents}
              refetchData={refetchData}
              currentUserId={currentUserId}
            />
          ))}
        <ReactPaginate
          breakLabel="..."
          nextLabel=">"
          previousLabel="<"
          onPageChange={handlePageClick}
          pageRangeDisplayed={10}
          pageCount={pageCount}
          renderOnZeroPageCount={null}
          containerClassName={'pagination'}
          activeClassName={'active'}
        />
      </div>
    </>
  );
}
