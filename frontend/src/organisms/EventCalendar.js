import React, { useState } from 'react';
import { Calendar, momentLocalizer } from 'react-big-calendar';
import moment from 'moment';
import { EventDetailsDialog } from 'src/organisms';
export function EventCalendar({
  events,
  currentUserId,
  refetchData,
  loading,
  error,
}) {
  const localizer = momentLocalizer(moment);
  const [openedEvent, setOpenedEvent] = useState(undefined);
  const [isEventDetailsDialogOpen, setIsEventDetailsDialogOpen] =
    useState(false);

  const openEvent = (event) => {
    setOpenedEvent(event);
    setIsEventDetailsDialogOpen(true);
  };

  return (
    <div className="App">
      <Calendar
        localizer={localizer}
        defaultDate={new Date()}
        defaultView="month"
        views={['month', 'day', 'agenda']}
        events={events}
        style={{ height: '80vh' }}
        titleAccessor="name"
        startAccessor={(event) => new Date(parseFloat(event.eventDateTime))}
        endAccessor={(event) => new Date(parseFloat(event.eventDateTime))}
        onSelectEvent={(event) => openEvent(event)}
      />
      <EventDetailsDialog
        isOpen={isEventDetailsDialogOpen}
        setIsEventDetailsDialogOpen={setIsEventDetailsDialogOpen}
        event={openedEvent}
        loading={loading}
        error={error}
        refetchData={refetchData}
        joinedEvents={events}
        isInvPage={false}
        currentUserId={currentUserId}
      />
    </div>
  );
}
