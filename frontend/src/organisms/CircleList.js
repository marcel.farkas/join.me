import React, { useState, useEffect } from 'react';
import { gql, useQuery } from '@apollo/client';
import ReactPaginate from 'react-paginate';

import { Button } from 'src/atoms';
import { Circle } from 'src/molecules';
import { CreateCircleDialog } from 'src/organisms';
import { EditCircleDialog } from './EditCircleDialog';

const USER_CIRCLES_QUERY = gql`
  query UserCircles {
    userCircles {
      id
      name
      owner {
        id
      }
      members {
        id
        profilePictureUrl
      }
    }
  }
`;

export function CircleList() {
  const { data, refetch } = useQuery(USER_CIRCLES_QUERY);
  const [currentItems, setCurrentItems] = useState(null);
  const [pageCount, setPageCount] = useState(0);
  const [itemOffset, setItemOffset] = useState(0);
  const [circleId, setCircleId] = useState(null);
  const [isCreateCircleDialogOpen, setIsCreateCircleDialogOpen] =
    useState(false);
  const [isEditCicleDialogOpen, setIsEditCircleDialogOpen] = useState(false);

  const itemsPerPage = 5;

  useEffect(() => {
    if (data) {
      const endOffset = itemOffset + itemsPerPage;
      setCurrentItems(data.userCircles.slice(itemOffset, endOffset));
      setPageCount(Math.ceil(data.userCircles.length / itemsPerPage));
    }
  }, [itemOffset, itemsPerPage, data]);

  const handlePageClick = (event) => {
    const newOffset = (event.selected * itemsPerPage) % data.userCircles.length;
    setItemOffset(newOffset);
  };

  const handleEditDialogOpen = (circleId) => {
    setCircleId(circleId);
    setIsEditCircleDialogOpen(true);
  };

  return (
    <div className="circle_page">
      <div style={{ color: 'white', fontSize: '2em', marginTop: '10px' }}>
        Circles
      </div>
      <Button
        className="btn create_circle"
        onClick={() => setIsCreateCircleDialogOpen(true)}
      >
        Create circle
      </Button>
      <div>
        {currentItems &&
          currentItems.map((circle) => (
            <Circle
              key={circle.id}
              circle={circle}
              refetchData={refetch}
              openEditDialog={handleEditDialogOpen}
            />
          ))}
        <ReactPaginate
          breakLabel="..."
          nextLabel=">"
          previousLabel="<"
          onPageChange={handlePageClick}
          pageRangeDisplayed={10}
          pageCount={pageCount}
          renderOnZeroPageCount={null}
          containerClassName={'pagination'}
          activeClassName={'active'}
        />
        <CreateCircleDialog
          isOpen={isCreateCircleDialogOpen}
          closeDialog={() => setIsCreateCircleDialogOpen(false)}
          refetchData={refetch}
        />
        {circleId && (
          <EditCircleDialog
            isOpen={isEditCicleDialogOpen}
            circleId={circleId}
            closeDialog={() => {
              setIsEditCircleDialogOpen(false);
              setCircleId(null);
            }}
            refetchData={refetch}
          />
        )}
      </div>
    </div>
  );
}
