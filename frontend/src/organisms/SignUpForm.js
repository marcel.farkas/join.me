import React from 'react';
import { Form, Formik } from 'formik';
import * as yup from 'yup';

import { Button } from 'src/atoms';
import { FormikField, FormikSelect } from 'src/molecules';

const initialValues = {
  first_name: '',
  last_name: '',
  email: '@',
  password: '',
  confirmPassword: '',
  business: 'no',
};

const schema = yup.object().shape({
  first_name: yup
    .string()
    .matches('[A-ZÁ-Ž]([a-zá-ž]){1,20}', 'Please enter a valid name.')
    .required('First name is required in order to register.')
    .label('First name'),
  last_name: yup
    .string()
    .matches('[A-ZÁ-Ž]([a-zá-ž]){1,20}', 'Please enter a valid name.')
    .required('Last name is required in order to register.')
    .label('Last name'),
  email: yup
    .string()
    .email('Please enter a valid email.')
    .required('E-mail is required in order to register.')
    .label('E-mail'),
  password: yup
    .string()
    .matches(
      '(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}',
      'Password must be at least 8 characters long. It must contain a lowercase character, an uppercase character and a number.',
    )
    .label('Password')
    .required('Password is required in order to register.'),
  confirmPassword: yup
    .string()
    .oneOf([yup.ref('password'), null], 'Passwords must match')
    .required()
    .label('Confirm password'),
  business: yup.string().oneOf(['yes', 'no']).required().label('Business'),
});

export function SignUpForm({ onSubmit, errorMessage, loading }) {
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={schema}
      validateOnBlur={false}
      onSubmit={onSubmit}
    >
      <Form method="POST">
        <FormikField
          id="first_name"
          label="First name"
          name="first_name"
          type="text"
          autoFocus
          autoComplete="on"
          autoCorrect="off"
          autoCapitalize="off"
        />
        <FormikField
          id="last_name"
          label="Last name"
          name="last_name"
          type="text"
          autoComplete="on"
          autoCorrect="off"
          autoCapitalize="off"
        />
        <FormikField
          id="email"
          label="E-mail"
          name="email"
          type="email"
          autoComplete="on"
          autoCorrect="off"
          autoCapitalize="off"
        />
        <FormikField
          id="password"
          label="Password"
          name="password"
          type="password"
          autoComplete="off"
          autoCorrect="off"
          autoCapitalize="off"
        />
        <FormikField
          id="confirmPassword"
          label="Confirm password"
          name="confirmPassword"
          type="password"
          autoComplete="off"
          autoCorrect="off"
          autoCapitalize="off"
        />
        <FormikSelect
          as="select"
          id="business"
          label="Business"
          name="business"
          type="text"
          autoComplete="off"
          autoCorrect="off"
          autoCapitalize="off"
        >
          <option value="yes">Yes</option>
          <option value="no">No</option>
        </FormikSelect>
        <Button type="submit" disabled={loading}>
          Sign up
        </Button>
      </Form>
    </Formik>
  );
}
