import React, { useState, useEffect } from 'react';
import ReactPaginate from 'react-paginate';

import { Event } from 'src/molecules/';

export function EventListwithFilter({
  data,
  joinedEvents,
  itemsPerPage,
  showJoinBtn,
  showEditBtn,
  showChatBtn,
  refetchData,
  isInvPage,
  currentUserId,
}) {
  const [currentItems, setCurrentItems] = useState(null);
  const [pageCount, setPageCount] = useState(0);
  const [itemOffset, setItemOffset] = useState(0);
  const [input, setInput] = useState('');
  const [filteredData, setFilteredData] = useState(data.events);

  const handlePageClick = (event) => {
    const newOffset = (event.selected * itemsPerPage) % data.events.length;
    setItemOffset(newOffset);
  };

  useEffect(() => {
     if (data && input.length < 3) {
      const endOffset = itemOffset + itemsPerPage;
      setCurrentItems(data.events.slice(itemOffset, endOffset));
      setPageCount(Math.ceil(data.events.length / itemsPerPage));
    }
    else if (filteredData && input.length > 2){
      const endOffset = itemOffset + itemsPerPage;
      setCurrentItems(filteredData.slice(itemOffset, endOffset));
      setPageCount(Math.ceil(filteredData.length / itemsPerPage));
    }
  }, [itemOffset, data, input, filteredData]);

  useEffect(() => {
    if (data && input.length > 2) {
      filterData(input);}
    handlePageClick({ selected: 0 });
  }, [input]);

  useEffect(() => {
    handlePageClick({ selected: 0 });
  }, [data.events.length])

  const filterData = (input) => {
    var lowercasedFilter = input.toLowerCase();
    var fData = data.events.filter((item) => {
      return (
        item.name.toLowerCase().search(lowercasedFilter) !== -1 ||
        item.place.toLowerCase().search(lowercasedFilter) !== -1 ||
        (item.creator.first_name + ' ' + item.creator.last_name)
          .toLowerCase()
          .search(lowercasedFilter) !== -1
          
      );
    });
    setFilteredData(fData);
  };

  return (
    <>
      <div>
        <input
          className="search"
          placeholder="Search events"
          value={input}
          onInput={(e) => setInput(e.target.value)}
        />
      </div>
      <div className="event_page">
        {currentItems &&
          currentItems.map((item) => (
            <Event
              key={item.id}
              event={item}
              joinedEvents={joinedEvents}
              showJoinBtn={showJoinBtn}
              showChatBtn={showChatBtn}
              showEditBtn={showEditBtn}
              refetchData={refetchData}
              isInvPage={isInvPage}
              currentUserId={currentUserId}
            />
          ))}
        {pageCount > 1 ? (
          <ReactPaginate
            breakLabel="..."
            nextLabel=">"
            previousLabel="<"
            onPageChange={handlePageClick}
            pageRangeDisplayed={3}
            marginPagesDisplayed={1}
            pageCount={pageCount}
            renderOnZeroPageCount={null}
            containerClassName={'pagination'}
            activeClassName={'active'}
          />
        ) : (
          ''
        )}
      </div>
    </>
  );
}
