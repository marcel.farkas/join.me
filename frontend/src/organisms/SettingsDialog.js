import React, { useCallback } from 'react';
import { gql, useMutation, useQuery } from '@apollo/client';

import { Dialog, ChangePasswordForm, ProfileForm } from 'src/organisms';
import { showToast } from 'src/utils/showToast';

const USER_QUERY = gql`
  query UserDetail {
    user {
      first_name
      last_name
      description
      email
      profilePictureUrl
    }
  }
`;

const UPDATE_PROFILE_MUTATION = gql`
  mutation UpdateProfile(
    $first_name: String
    $last_name: String
    $description: String
    $email: String
    $profilePictureUrl: String
  ) {
    updateProfile(
      first_name: $first_name
      last_name: $last_name
      email: $email
      description: $description
      profilePictureUrl: $profilePictureUrl
    ) {
      id
      email
    }
  }
`;

const CHANGE_PASSWORD_MUTATION = gql`
  mutation ChangePassword(
    $newPassword: String!
    $newPasswordConfirm: String!
    $currPassword: String!
  ) {
    changePassword(
      newPassword: $newPassword
      newPasswordConfirm: $newPasswordConfirm
      currPassword: $currPassword
    ) {
      id
      email
    }
  }
`;

export function SettingsDialog({
  isOpen,
  contentLabel,
  setIsSettingsDialogOpen,
}) {
  const initialData = useQuery(USER_QUERY);

  const [updateProfile] = useMutation(UPDATE_PROFILE_MUTATION, {
    onCompleted: () => {
      initialData.refetch();
      showToast({ type: 'SUCCESS', msg: 'Changes saved' });
    },
    onError: (error) => showToast({ type: 'ERROR', msg: error.message }),
  });

  const handleProfileUpdate = useCallback(
    (variables) => {
      const fullname = variables.name.split(' ');
      const first_name = fullname[0];
      const last_name = fullname[1];

      updateProfile({
        variables: {
          ...variables,
          first_name,
          last_name,
          profilePictureUrl: variables.profilePicUrl,
        },
      });
    },
    [updateProfile],
  );

  const [changePassword] = useMutation(CHANGE_PASSWORD_MUTATION, {
    onCompleted: () => {
      showToast({ type: 'SUCCESS', msg: 'Password succesfully changed' });
    },
    onError: (error) => showToast({ type: 'ERROR', msg: error.message }),
  });

  const handlePasswordChange = useCallback(
    (variables) => {
      changePassword({ variables });
    },
    [changePassword],
  );

  return (
    <>
      <Dialog
        isOpen={isOpen}
        closeDialog={() => setIsSettingsDialogOpen(false)}
        contentLabel={contentLabel}
        header="Profile settings"
      >
        <ProfileForm
          profileUpdate={handleProfileUpdate}
          initialData={initialData}
        />
        <ChangePasswordForm changePassword={handlePasswordChange} />
      </Dialog>
    </>
  );
}
