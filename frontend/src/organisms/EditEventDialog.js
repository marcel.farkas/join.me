import React, { useCallback, useState } from 'react';
import { gql, useMutation, useQuery } from '@apollo/client';

import { Dialog, EditEventForm, InviteDialog } from 'src/organisms';
import { showToast } from 'src/utils/showToast';

const EVENT_QUERY = gql`
  query EventDetail($eventId: Int!) {
    event(id: $eventId) {
      name
      eventPictureUrl
      eventDateTime
      place
      information
      private
      participantsLimit
    }
  }
`;

const EDIT_EVENT_MUTATION = gql`
  mutation EditEvent(
    $id: Int!
    $name: String!
    $eventPictureUrl: String
    $eventDateTime: String!
    $place: String!
    $information: String
    $isPrivate: Boolean!
    $participantsLimit: Int
    $selectedCircles: [InputCircle]
    $selectedFriends: [InputUser]
  ) {
    editEvent(
      id: $id
      name: $name
      eventPictureUrl: $eventPictureUrl
      eventDateTime: $eventDateTime
      place: $place
      information: $information
      isPrivate: $isPrivate
      participantsLimit: $participantsLimit
      selectedCircles: $selectedCircles
      selectedFriends: $selectedFriends
    ) {
      id
      name
    }
  }
`;

export function EditEventDialog({
  isOpen,
  contentLabel,
  setIsEditEventDialogOpen,
  event,
  refetchData,
  isInviteDialogOpen,
  setIsInviteDialogOpen,
  selectedFriends,
  selectedCircles,
}) {
  const [isPrivate, setIsPrivate] = useState(event.private);
  const initialData = useQuery(EVENT_QUERY, {
    variables: { eventId: event.id },
  });

  const [editEvent, editEventState] = useMutation(EDIT_EVENT_MUTATION, {
    onCompleted: () => {
      setIsEditEventDialogOpen(false);
      selectedCircles.length = 0;
      selectedFriends.length = 0;
      refetchData();
      showToast({ type: 'SUCCESS', msg: 'Event edited successfully' });
    },
    onError: (error) => showToast({ type: 'ERROR', msg: error.message }),
  });

  const handleEditEventFormSubmit = useCallback(
    (variables) => {
      editEvent({
        variables: {
          ...variables,
          id: event.id,
          isPrivate,
          participantsLimit: variables.participantsLimit || null,
          selectedCircles: selectedCircles.map(function (circle) {
            return circle;
          }),
          selectedFriends: selectedFriends.map(function (friend) {
            return friend;
          }),
        },
      });
    },
    [editEvent, event.id, selectedFriends, selectedCircles, isPrivate],
  );

  return (
    <>
      <Dialog
        isOpen={isOpen}
        closeDialog={() => {
          setIsEditEventDialogOpen(false);
          selectedCircles.length = 0;
          selectedFriends.length = 0;
        }}
        contentLabel={contentLabel}
        header="Edit event"
      >
        <EditEventForm
          loading={editEventState.loading}
          errorMessage={editEventState.error}
          onSubmit={handleEditEventFormSubmit}
          initialData={initialData}
          isOn={!isPrivate}
          handleToggle={() => setIsPrivate(!isPrivate)}
          isInviteDialogOpen={isInviteDialogOpen}
          setIsInviteDialogOpen={setIsInviteDialogOpen}
        />
      </Dialog>
      <InviteDialog
        isInviteDialogOpen={isInviteDialogOpen}
        setIsInviteDialogOpen={setIsInviteDialogOpen}
        refetchData={refetchData}
        selectedCircles={selectedCircles}
        selectedFriends={selectedFriends}
      />
    </>
  );
}
