import React from 'react';
import { gql, useQuery } from '@apollo/client';

import { InviteForm } from 'src/organisms';

const CIRCLES_QUERY = gql`
  query Circle {
    userCircles {
      id
      name
      created_at
    }
  }
`;

const FRIENDS_QUERY = gql`
  query Friends {
    friends {
      id
      first_name
      last_name
      profilePictureUrl
    }
  }
`;

export function InviteDialog({
  contentLabel,
  isInviteDialogOpen,
  setIsInviteDialogOpen,
  selectedCircles,
  selectedFriends,
}) {
  const circleData = useQuery(CIRCLES_QUERY);
  const friendData = useQuery(FRIENDS_QUERY);

  return (
    <InviteForm
      selectedCircles={selectedCircles}
      selectedFriends={selectedFriends}
      circleData={circleData}
      friendData={friendData}
      isOpen={isInviteDialogOpen}
      closeDialog={() => setIsInviteDialogOpen(false)}
      contentLabel={contentLabel}
      header="Invite people"
    />
  );
}
