import React, { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

import { Dialog, PasswordResetForm } from 'src/organisms';

const PASSWORD_RESET_MUTATION = gql`
  mutation ResetPassword(
    $id: Int!
    $password: String!
    $confirmPassword: String!
    $token: String!
  ) {
    resetPassword(
      id: $id
      password: $password
      confirmPassword: $confirmPassword
      token: $token
    ) {
      id
      email
    }
  }
`;

export function PasswordResetDialog({
  userId,
  token,
  isOpen,
  closeDialog,
  contentLabel,
  showToast,
  openDialog,
}) {
  const [passwordReset, passwordResetState] = useMutation(
    PASSWORD_RESET_MUTATION,
    {
      onCompleted: () => {
        closeDialog('passwordReset');
        showToast({ type: 'SUCCESS', msg: 'Your password has been reset!' });
        openDialog('signIn');
      },
      onError: () => {
        showToast({
          type: 'ERROR',
          msg: 'ERROR: Your password could not be reset.',
        });
      },
    },
  );

  const handleResetPasswordFormSubmit = useCallback(
    (variables) => {
      passwordReset({ variables });
    },
    [passwordReset],
  );

  return (
    <Dialog
      isOpen={isOpen}
      closeDialog={() => closeDialog('passwordReset')}
      contentLabel={contentLabel}
      className="dialog-resetPassword"
      header="Join.me account"
    >
      <div>Please choose your new password.</div>
      <br />
      <PasswordResetForm
        userId={userId}
        token={token}
        loading={passwordResetState.loading}
        errorMessage={passwordResetState.error}
        onSubmit={handleResetPasswordFormSubmit}
      />
    </Dialog>
  );
}
