import React, { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

import { LinkButton } from 'src/atoms';
import { Dialog, SignUpForm } from 'src/organisms';

const SIGN_UP_MUTATION = gql`
  mutation SignUp(
    $first_name: String!
    $last_name: String!
    $email: String!
    $password: String!
    $confirmPassword: String!
    $business: Boolean!
  ) {
    signup(
      first_name: $first_name
      last_name: $last_name
      email: $email
      password: $password
      confirmPassword: $confirmPassword
      business: $business
    ) {
      user {
        id
        email
      }
      token
    }
  }
`;

export function SignUpDialog({
  isOpen,
  openDialog,
  closeDialog,
  contentLabel,
  showToast,
}) {
  const [signUp, signUpState] = useMutation(SIGN_UP_MUTATION, {
    onCompleted: () => {
      closeDialog('signUp');
      showToast({ type: 'SUCCESS', msg: 'Sign up successful!' });
    },
    onError: () =>
      showToast({ type: 'ERROR', msg: 'ERROR: Could not sign you up.' }),
  });

  const handleSignUpFormSubmit = useCallback(
    (variables) => {
      const business = variables.business === 'yes' ? true : false;
      signUp({ variables: { ...variables, business } });
    },
    [signUp],
  );

  return (
    <Dialog
      isOpen={isOpen}
      closeDialog={() => closeDialog('signUp')}
      contentLabel={contentLabel}
      header="Join.me account"
    >
      <div>Create new account.</div>
      <br />
      <SignUpForm
        loading={signUpState.loading}
        errorMessage={signUpState.error}
        onSubmit={handleSignUpFormSubmit}
      />
      <LinkButton onClick={() => openDialog('signIn')}>Sign in</LinkButton>
    </Dialog>
  );
}
