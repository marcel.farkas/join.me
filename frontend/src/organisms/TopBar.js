import React from 'react';
import { Redirect, useHistory } from 'react-router';
import { gql, useQuery } from '@apollo/client';

import {
  Button,
  HomeIcon,
  InvitationIcon,
  CircleIcon,
  NotificationIcon,
  ProfileIcon,
} from 'src/atoms/';
import { route } from 'src/Routes';
import { useAuth } from 'src/utils/auth';
import { NavLink } from 'react-router-dom';
import classNames from 'classnames';

const NOTIFICATIONS_COUNT_QUERY = gql`
  query NotificationsCount {
    notificationsCount
  }
`;

export function TopBar() {
  const { token, signout } = useAuth();
  const history = useHistory();
  const { data } = useQuery(NOTIFICATIONS_COUNT_QUERY);

  if (!token) return <Redirect to={route.landingPage()} />;

  return (
    <div className="topBar">
      <div id="JOINME" className="mont left">
        JOIN.ME
      </div>
      <div className="right mont">
        <Button
          className="btn_lp mont"
          animated
          onClick={() => {
            signout();
            history.push(route.landingPage());
          }}
        >
          Log out
        </Button>
      </div>
      <div className="topBar-middle">
        <ul>
          <li>
            <NavLink
              to={route.homePage()}
              activeClassName="active"
              className="btn_icon"
            >
              <HomeIcon />
            </NavLink>
          </li>
          <li>
            <NavLink
              to={route.invitationsPage()}
              activeClassName="active"
              className="btn_icon"
            >
              <InvitationIcon />
            </NavLink>
          </li>
          <li>
            <NavLink
              to={route.circlesPage()}
              activeClassName="active"
              className="btn_icon"
            >
              <CircleIcon />
            </NavLink>
          </li>
          <li className="relative">
            <NavLink
              to={route.notificationsPage()}
              activeClassName="active"
              className="btn_icon"
            >
              <NotificationIcon />
            </NavLink>
            <div
              className={classNames({
                hidden: data?.notificationsCount < 1,
                redDot: data?.notificationsCount > 0,
              })}
            >
              <p>{data && data.notificationsCount}</p>
            </div>
          </li>
          <li>
            <NavLink
              to={route.profilePage()}
              activeClassName="active"
              className="btn_icon dude"
            >
              <ProfileIcon />
            </NavLink>
          </li>
        </ul>
      </div>
    </div>
  );
}
