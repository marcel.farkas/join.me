import React, { useState, useEffect, useRef, useCallback } from 'react';
import { gql, useQuery, useMutation } from '@apollo/client';
import { initializeApp } from 'firebase/app';
import { getAuth, signInAnonymously } from 'firebase/auth';
import {
  collection,
  query,
  orderBy,
  onSnapshot,
  getFirestore,
  doc,
  setDoc,
  serverTimestamp,
} from 'firebase/firestore';

import { Dialog } from 'src/organisms';
import classNames from 'classnames';
import { ProfilePicture } from 'src/atoms';
import { SendMessageForm } from 'src/molecules';

const app = initializeApp({
  apiKey: 'AIzaSyCe_QPZnyaTQyfRH_tN4vspAtncN4BMkig',
  authDomain: 'join-me-e7b6a.firebaseapp.com',
  projectId: 'join-me-e7b6a',
  storageBucket: 'join-me-e7b6a.appspot.com',
  messagingSenderId: '12964773469',
  appId: '1:12964773469:web:4710f457007a86c54817b4',
});
const auth = getAuth(app);
const db = getFirestore(app);

const USER_QUERY = gql`
  query User {
    user {
      id
    }
  }
`;

const USERS_QUERY = gql`
  query Users($ids: [Int]) {
    users(ids: $ids) {
      id
      first_name
      last_name
      profilePictureUrl
    }
  }
`;

const EVENT_QUERY = gql`
  query Event($id: Int!) {
    event(id: $id) {
      chatroom_id
    }
  }
`;

const NOTIFY_USERS_MUTATION = gql`
  mutation NotifyUsers($id: Int!) {
    notifyUsers(id: $id)
  }
`;

export function EventChatDialog({
  isOpen,
  closeDialog,
  contentLabel,
  eventId,
}) {
  const chatBottomRef = useRef(null);
  const [messages, setMessages] = useState([]);
  const [userId, setUserId] = useState(null);
  const [senders, setSenders] = useState([]);
  const [chatroomId, setChatroomId] = useState(null);
  const { data: userData } = useQuery(USER_QUERY);
  const { data: eventData } = useQuery(EVENT_QUERY, {
    variables: { id: eventId },
  });
  const { data: sendersData } = useQuery(USERS_QUERY, {
    variables: { ids: senders },
  });

  const [notifyUsers] = useMutation(NOTIFY_USERS_MUTATION);

  const handleNotifyUsers = useCallback(() => {
    notifyUsers({ variables: { id: eventId } });
  }, [notifyUsers, eventId]);

  const handleSendMessage = async (evt) => {
    evt.preventDefault();

    if (userId) {
      const text = evt.target.messageInput.value;
      if (chatroomId && text) {
        await setDoc(doc(collection(db, `messages-${chatroomId}`)), {
          text: text,
          createdAt: serverTimestamp(),
          userId: userId,
        });
        handleNotifyUsers();
        evt.target.reset();
      }
    }
  };

  const fetchMessages = async () => {
    await signInAnonymously(auth);
    const messagesRef = collection(db, `messages-${chatroomId}`);
    const q = query(messagesRef, orderBy('createdAt'));
    onSnapshot(q, (querySnapshot) => {
      const data = [];
      querySnapshot.forEach((doc) => {
        const createdAt = new Date(
          doc.data().createdAt?.toDate() || Date.now(),
        ).toLocaleString();
        data.push({
          ...doc.data(),
          id: doc.id,
          createdAt: createdAt,
        });
      });
      setMessages(data);
    });
  };

  useEffect(() => {
    if (userData) {
      setUserId(userData.user.id);
    }
  }, [userData]);

  useEffect(() => {
    if (eventData) {
      setChatroomId(eventData.event.chatroom_id);
      fetchMessages();
    }
  }, [eventData, chatroomId]);

  useEffect(() => {
    if (messages) {
      setSenders([...new Set(messages.map((msg) => msg.userId))]);
      chatBottomRef.current?.scrollIntoView({ behavior: 'smooth' });
    }
  }, [messages]);

  return (
    <>
      <Dialog
        className="chat_dialog"
        isOpen={isOpen}
        closeDialog={closeDialog}
        contentLabel={contentLabel}
        header="Event chat"
      >
        <div className="chat" style={{ overflow: 'auto' }}>
          {messages.map((msg) => {
            const isMyMessage = msg.userId === userId;
            const sender = sendersData?.users.find(
              (user) => user.id === msg.userId,
            );
            return (
              <div key={msg.id}>
                <ProfilePicture
                  profilePictureUrl={sender?.profilePictureUrl}
                  imageSize={20}
                  alt={`${sender?.first_name} ${sender?.last_name}`}
                  className={isMyMessage ? 'hideSender' : 'chat_pic'}
                />
                <div
                  className={classNames('message', {
                    myMessage: isMyMessage,
                  })}
                >
                  <span>{msg.text}</span>
                  <span className="timestamp">{msg.createdAt}</span>
                </div>
              </div>
            );
          })}
          <div
            ref={chatBottomRef}
            style={{
              height: '0',
              width: '0',
              float: 'left',
              clear: 'both',
            }}
          ></div>
        </div>
        <SendMessageForm handleSendMessage={handleSendMessage} />
      </Dialog>
    </>
  );
}
