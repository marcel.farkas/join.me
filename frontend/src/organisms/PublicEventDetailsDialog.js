import React from 'react';

import { Dialog, PublicEventDetails } from 'src/organisms';

export function PublicEventDetailsDialog({
  isOpen,
  contentLabel,
  event,
  loading,
  error,
  setIsEventDetailsDialogOpen,
  joinedEvents,
  refetchData,
  currentUserId,
}) {
  return (
    <>
      <Dialog
        className="event_detail"
        isOpen={isOpen}
        closeDialog={() => setIsEventDetailsDialogOpen(false)}
        contentLabel={contentLabel}
      >
        <PublicEventDetails
          event={event}
          loading={loading}
          error={error}
          joinedEvents={joinedEvents}
          refetchData={refetchData}
          currentUserId={currentUserId}
        />
      </Dialog>
    </>
  );
}
