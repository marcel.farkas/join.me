import React, { useCallback, useState } from 'react';
import { gql, useMutation } from '@apollo/client';

import { Dialog, CreateEventForm, InviteDialog } from 'src/organisms';
import { showToast } from 'src/utils/showToast';

const CREATE_EVENT_MUTATION = gql`
  mutation CreateEvent(
    $name: String!
    $eventPictureUrl: String
    $eventDateTime: String!
    $place: String!
    $information: String
    $isPrivate: Boolean!
    $participantsLimit: Int
    $selectedCircles: [InputCircle]
    $selectedFriends: [InputUser]
  ) {
    createEvent(
      name: $name
      eventPictureUrl: $eventPictureUrl
      eventDateTime: $eventDateTime
      place: $place
      information: $information
      isPrivate: $isPrivate
      participantsLimit: $participantsLimit
      selectedCircles: $selectedCircles
      selectedFriends: $selectedFriends
    ) {
      id
      name
    }
  }
`;

export function CreateEventDialog({
  isOpen,
  contentLabel,
  setIsCreateEventDialogOpen,
  refetchData,
  isInviteDialogOpen,
  setIsInviteDialogOpen,
  selectedCircles,
  selectedFriends,
}) {
  const [isPrivate, setIsPrivate] = useState(false);
  const [createEvent, createEventState] = useMutation(CREATE_EVENT_MUTATION, {
    onCompleted: () => {
      setIsCreateEventDialogOpen(false);
      selectedCircles.length = 0;
      selectedFriends.length = 0;
      refetchData();
      showToast({ type: 'SUCCESS', msg: 'Event created successfully' });
    },
    onError: (error) => showToast({ type: 'ERROR', msg: error.message }),
  });

  const handleCreateEventFormSubmit = useCallback(
    (variables) => {
      createEvent({
        variables: {
          ...variables,
          isPrivate,
          participantsLimit: variables.participantsLimit || null,
          selectedCircles: selectedCircles.map(function (circle) {
            return circle;
          }),
          selectedFriends: selectedFriends.map(function (friend) {
            return friend;
          }),
        },
      });
    },
    [createEvent, selectedCircles, selectedFriends, isPrivate],
  );

  return (
    <>
      <Dialog
        isOpen={isOpen}
        closeDialog={() => {
          setIsCreateEventDialogOpen(false);
          selectedCircles.length = 0;
          selectedFriends.length = 0;
        }}
        contentLabel={contentLabel}
        header="Create event"
      >
        <CreateEventForm
          loading={createEventState.loading}
          errorMessage={createEventState.error}
          onSubmit={handleCreateEventFormSubmit}
          isOn={!isPrivate}
          handleToggle={() => setIsPrivate(!isPrivate)}
          isInviteDialogOpen={isInviteDialogOpen}
          setIsInviteDialogOpen={setIsInviteDialogOpen}
        />
      </Dialog>
      <InviteDialog
        selectedCircles={selectedCircles}
        selectedFriends={selectedFriends}
        isInviteDialogOpen={isInviteDialogOpen}
        setIsInviteDialogOpen={setIsInviteDialogOpen}
        refetchData={refetchData}
      />
    </>
  );
}
