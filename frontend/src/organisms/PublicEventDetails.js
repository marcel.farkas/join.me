import React from 'react';
import { NavLink } from 'react-router-dom';
import Img from 'src/images/Background.png';
import {
  CalendarIcon,
  ParticipantsIcon,
  PinIcon,
  ProfilePicture,
} from 'src/atoms';
import { ProfilePictureLinkWrapper } from 'src/molecules';

export function PublicEventDetails({ event, loading, error, currentUserId }) {
  return (
    <div>
      <h1>{event.name}</h1>
      <div>
        <img src={event.eventPictureUrl || Img} alt="Event" />
      </div>

      <span className="baba">
        <span className="createdby">Created by:&nbsp;</span>
        <div className="createdby2">
          <ProfilePicture
            key={event.creator.id}
            profilePictureUrl={event.creator.profilePictureUrl}
            loading={loading}
            error={error}
            imageSize={10}
            className="profil"
          />
          <span className="bold createdby">
            &nbsp;
            {event.creator.first_name} {event.creator.last_name}
          </span>
        </div>
      </span>
      <p>
        <CalendarIcon />
        {new Date(parseFloat(event.eventDateTime)).toLocaleString()}
      </p>
      <p>
        <PinIcon />
        {event.place}
      </p>
      <div className="participants">
        <ParticipantsIcon />
        <div className="people">
          {event.participants.length ? (
            event.participants.map((participant) => (
              <NavLink to={`/profile/${participant.id}`}>
                <ProfilePictureLinkWrapper
                  key={participant.id}
                  profilePictureUrl={participant.profilePictureUrl}
                  loading={loading}
                  error={error}
                  imageSize={10}
                  className="profil"
                  user={participant.id}
                  currentUserId={currentUserId}
                />
              </NavLink>
            ))
          ) : (
            <span>No participants</span>
          )}
        </div>
      </div>
      <div>
        <p>
          <span className="bold">Description: </span>
          <p>{event.information}</p>
        </p>
      </div>
    </div>
  );
}
