import React from 'react';
import { Form, Formik } from 'formik';
import * as yup from 'yup';

import { Button, ProfilePicture } from 'src/atoms';
import { FormikField } from 'src/molecules';

const schema = yup.object().shape({
  profilePicUrl: yup.string(),
  name: yup
    .string()
    .matches('[A-ZÁ-Ž]([a-zá-ž]){1,20}', 'Please enter a valid name.')
    .required('Name cannot stay empty.')
    .label('Name'),
  email: yup
    .string()
    .email('Please enter a valid email.')
    .required('E-mail is required.')
    .label('E-mail'),
});

export function ProfileForm({
  profileUpdate,
  initialData: {
    data: { user },
    loading,
  },
}) {
  return (
    <>
      <div className="setpic">
        <ProfilePicture
          className="profil"
          profilePictureUrl={user.profilePictureUrl}
          loading={loading}
        />
      </div>
      <Formik
        initialValues={{
          ...user,
          profilePicUrl: user.profilePictureUrl,
          name: `${user.first_name} ${user.last_name}`,
        }}
        validationSchema={schema}
        enableReinitialize={true}
        validateOnBlur={false}
        onSubmit={profileUpdate}
      >
        <Form key="profile-update">
          <div>
            <FormikField
              id="profilePicUrl"
              label="Profile Picture URL"
              name="profilePicUrl"
              type="text"
              autoFocus
              autoComplete="off"
              autoCorrect="off"
              autoCapitalize="off"
            />
            <FormikField
              id="name"
              label="Name"
              name="name"
              type="text"
              autoFocus
              autoComplete="off"
              autoCorrect="off"
              autoCapitalize="off"
            />
            <FormikField
              id="description"
              label="Profile Description"
              name="description"
              type="text"
              autoComplete="off"
              autoCorrect="off"
              autoCapitalize="off"
            ></FormikField>
            <FormikField
              id="email"
              label="E-mail"
              name="email"
              type="email"
              autoComplete="on"
              autoCorrect="off"
              autoCapitalize="off"
            />
            <Button type="submit">Save Changes</Button>
          </div>
        </Form>
      </Formik>
    </>
  );
}
