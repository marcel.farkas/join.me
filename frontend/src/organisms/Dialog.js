import React, { useEffect } from 'react';
import Modal from 'react-modal';
import classNames from 'classnames';

Modal.setAppElement('#root');

export function Dialog({
  isOpen,
  closeDialog,
  contentLabel,
  children,
  className,
  header,
}) {
  useEffect(() => {
    if (isOpen) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = 'unset';
    }
  }, [isOpen]);

  return (
    <Modal
      isOpen={isOpen}
      className={classNames('dialog', className)}
      contentLabel={contentLabel}
    >
      <button className="btn_x right" onClick={closeDialog}>
        x
      </button>
      <div className="content">
        <h2>{header}</h2>
        {children}
      </div>
    </Modal>
  );
}
