import React, { useCallback } from 'react';
import { gql, useMutation } from '@apollo/client';

import { Dialog, PasswordResetRequestForm } from 'src/organisms';

const REQUEST_PASSWORD_RESET_MUTATION = gql`
  mutation RequestPasswordReset($email: String!) {
    requestPasswordReset(email: $email) {
      user_id
    }
  }
`;

export function PasswordResetRequestDialog({
  isOpen,
  closeDialog,
  contentLabel,
  showToast,
}) {
  const [passwordResetRequest, passwordResetRequestState] = useMutation(
    REQUEST_PASSWORD_RESET_MUTATION,
    {
      onCompleted: () => {
        closeDialog('passwordResetRequest');
        showToast({
          type: 'INFO',
          msg: 'Password reset link sent to your e-mail address!',
        });
      },
      onError: () => {
        showToast({
          type: 'ERROR',
          msg: 'ERROR: Password reset link could not be sent to your e-mail address.',
        });
      },
    },
  );

  const handleResetPasswordRequestFormSubmit = useCallback(
    (variables) => {
      passwordResetRequest({ variables });
    },
    [passwordResetRequest],
  );

  return (
    <Dialog
      isOpen={isOpen}
      closeDialog={() => closeDialog('passwordResetRequest')}
      contentLabel={contentLabel}
      header="Join.me account"
    >
      <div style={{ textAlign: 'center' }}>
        Please enter your e-mail address.
        <br /> After that, an email will be sent to you with further
        instructions.
      </div>
      <br />
      <PasswordResetRequestForm
        loading={passwordResetRequestState.loading}
        errorMessage={passwordResetRequestState.error}
        onSubmit={handleResetPasswordRequestFormSubmit}
      />
    </Dialog>
  );
}
