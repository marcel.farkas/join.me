import React, { useEffect } from 'react';
import { gql, useMutation } from '@apollo/client';

import { Button } from 'src/atoms';
import { Dialog } from 'src/organisms';

const CONFIRM_EMAIL_MUTATION = gql`
  mutation ConfirmEmail($id: Int!, $token: String!) {
    confirmEmail(id: $id, token: $token) {
      id
      email
    }
  }
`;

export function ConfirmEmailDialog({
  userId,
  token,
  isOpen,
  openDialog,
  closeDialog,
  contentLabel,
}) {
  const [confirmEmail, confirmEmailState] = useMutation(
    CONFIRM_EMAIL_MUTATION,
    {
      onCompleted: () => {},
      onError: (error) => console.log(error),
    },
  );

  useEffect(() => {
    if (userId && token) {
      confirmEmail({ variables: { id: userId, token } });
    }
  }, [userId, token, confirmEmail]);

  return (
    <Dialog
      isOpen={isOpen}
      closeDialog={() => closeDialog('confirmEmail')}
      contentLabel={contentLabel}
      className="email_conf"
      header="Email confirmation"
    >
      {confirmEmailState.loading ? (
        <div>Confirming your email...</div>
      ) : (
        <>
          <div>
            {confirmEmailState.error
              ? `${confirmEmailState.error.message}.`
              : 'Your email has been confirmed. You may log in now.'}
          </div>
          <br />
          <Button onClick={() => openDialog('signIn')}>Sign in</Button>
        </>
      )}
    </Dialog>
  );
}
