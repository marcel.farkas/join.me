import React, { useState, useEffect } from 'react';
import ReactPaginate from 'react-paginate';

import { Event } from 'src/molecules/';
import { EventChatDialog, EventFeedDialog } from 'src/organisms';

export function EventList({
  data,
  joinedEvents,
  itemsPerPage,
  showJoinBtn,
  showEditBtn,
  showChatBtn,
  showFeedBtn,
  showDeleteBtn,
  refetchData,
  isInvPage,
  isInviteDialogOpen,
  setIsInviteDialogOpen,
  selectedFriends,
  selectedCircles,
  currentUserId,
}) {
  const [currentItems, setCurrentItems] = useState(null);
  const [pageCount, setPageCount] = useState(0);
  const [itemOffset, setItemOffset] = useState(0);
  const [isEventFeedDialogOpen, setIsEventFeedDialogOpen] = useState(false);
  const [eventId, setEventId] = useState(null);
  const [isEventChatDialogOpen, setIsEventChatDialogOpen] = useState(false);
  const [input, setInput] = useState('');
  const [filteredData, setFilteredData] = useState(data.events);

  useEffect(() => {
    if (data && input.length < 3) {
     const endOffset = itemOffset + itemsPerPage;
     setCurrentItems(data.events.slice(itemOffset, endOffset));
     setPageCount(Math.ceil(data.events.length / itemsPerPage));
   }
   else if (filteredData && input.length > 2){
     const endOffset = itemOffset + itemsPerPage;
     setCurrentItems(filteredData.slice(itemOffset, endOffset));
     setPageCount(Math.ceil(filteredData.length / itemsPerPage));
   }
 }, [itemOffset, data, input, filteredData]);

 useEffect(() => {
  if (data && input.length > 2) {
    filterData(input);}
  handlePageClick({ selected: 0 });
}, [input]);

  useEffect(() => {
    handlePageClick({ selected: 0 });
  }, [data.events.length])

  const handlePageClick = (event) => {
    const newOffset = (event.selected * itemsPerPage) % data.events.length;
    setItemOffset(newOffset);
  };

  const filterData = (input) => {
    var lowercasedFilter = input.toLowerCase();
    var fData = data.events.filter((item) => {
      return (
        item.name.toLowerCase().search(lowercasedFilter) !== -1 ||
        item.place.toLowerCase().search(lowercasedFilter) !== -1 ||
        (item.creator.first_name + ' ' + item.creator.last_name)
          .toLowerCase()
          .search(lowercasedFilter) !== -1
          
      );
    });
    setFilteredData(fData);
  };

  const handleFeedDialogOpen = (eventId) => {
    setEventId(eventId);
    setIsEventFeedDialogOpen(true);
  };

  const handleChatDialogOpen = (eventId) => {
    setEventId(eventId);
    setIsEventChatDialogOpen(true);
  };

  return (
    <>
    <div>
        <input
          className="search"
          placeholder="Search events"
          value={input}
          onInput={(e) => setInput(e.target.value)}
        />
      </div>
      <div class="event_page">
        {currentItems &&
          currentItems.map((item) => (
            <Event
              key={item.id}
              event={item}
              joinedEvents={joinedEvents}
              showJoinBtn={showJoinBtn}
              showChatBtn={showChatBtn}
              showEditBtn={showEditBtn}
              showFeedBtn={showFeedBtn}
              showDeleteBtn={showDeleteBtn}
              refetchData={refetchData}
              isInvPage={isInvPage}
              isInviteDialogOpen={isInviteDialogOpen}
              setIsInviteDialogOpen={setIsInviteDialogOpen}
              selectedCircles={selectedCircles}
              selectedFriends={selectedFriends}
              openFeedDialog={handleFeedDialogOpen}
              openChatDialog={handleChatDialogOpen}
              currentUserId={currentUserId}
            />
          ))}

        {eventId && (
          <EventChatDialog
            isOpen={isEventChatDialogOpen}
            eventId={eventId}
            closeDialog={() => {
              setIsEventChatDialogOpen(false);
              setEventId(null);
            }}
            contentLabel="Chat"
          />
        )}
        {eventId && (
          <EventFeedDialog
            isOpen={isEventFeedDialogOpen}
            eventId={eventId}
            closeDialog={() => {
              setIsEventFeedDialogOpen(false);
              setEventId(null);
            }}
            contentLabel="Feed"
            />
        )}
        {pageCount > 1 && (
          <ReactPaginate
            breakLabel="..."
            nextLabel=">"
            previousLabel="<"
            onPageChange={handlePageClick}
            pageRangeDisplayed={3}
            marginPagesDisplayed={1}
            pageCount={pageCount}
            renderOnZeroPageCount={null}
            containerClassName={'pagination'}
            activeClassName={'active'}
          />
        )}
      </div>
    </>
  );
}
