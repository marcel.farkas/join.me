import React from 'react';
import { Dialog } from '.';
import { InviteList } from 'src/molecules';
import { showToast } from 'src/utils/showToast';

const onCircleAdd = (circle, selectedCircles) =>
  selectedCircles.indexOf(circle) === -1
    ? (selectedCircles.push(circle),
      showToast({ type: 'SUCCESS', msg: `Circle ${circle.name} added` }))
    : (selectedCircles.pop(circle),
      showToast({ type: 'SUCCESS', msg: `Circle ${circle.name} removed` }));

const onFriendAdd = (friend, selectedFriends) =>
  selectedFriends.indexOf(friend) === -1
    ? (selectedFriends.push(friend),
      showToast({
        type: 'SUCCESS',
        msg: `User ${friend.first_name} ${friend.last_name} added`,
      }))
    : (selectedFriends.pop(friend),
      showToast({
        type: 'SUCCESS',
        msg: `User ${friend.first_name} ${friend.last_name} removed`,
      }));

export function InviteForm({
  isOpen,
  closeDialog,
  contentLabel,
  header,
  selectedCircles,
  selectedFriends,
  circleData,
  friendData,
}) {
  return (
    <>
      <Dialog
        isOpen={isOpen}
        closeDialog={closeDialog}
        contentLabel={contentLabel}
        header={header}
      >
        <InviteList
          circleData={circleData}
          friendData={friendData}
          selectedCircles={selectedCircles}
          selectedFriends={selectedFriends}
          onCircleAdd={onCircleAdd}
          onFriendAdd={onFriendAdd}
        />
      </Dialog>
    </>
  );
}
