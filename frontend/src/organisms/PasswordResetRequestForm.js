import React from 'react';
import { Form, Formik } from 'formik';
import * as yup from 'yup';

import { Button } from 'src/atoms';
import { FormikField } from 'src/molecules';

const initialValues = {
  email: '',
};

const schema = yup.object().shape({
  email: yup.string().email().required().label('Email'),
});

export function PasswordResetRequestForm({ onSubmit, errorMessage, loading }) {
  return (
    <Formik
      onSubmit={onSubmit}
      initialValues={initialValues}
      validationSchema={schema}
      validateOnBlur={false}
    >
      <Form method="POST">
        <FormikField
          id="email"
          name="email"
          label="E-mail"
          type="email"
          autoFocus
          autoComplete="on"
          autoCorrect="off"
          autoCapitalize="off"
        />
        <Button type="submit" disabled={loading}>
          Send reset link
        </Button>
      </Form>
    </Formik>
  );
}
