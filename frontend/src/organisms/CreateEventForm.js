import React from 'react';
import { Form, Formik } from 'formik';
import * as yup from 'yup';

import { Button, Switch, Label } from 'src/atoms';
import { FormikField } from 'src/molecules/';

const initialValues = {
  name: '',
  eventPictureUrl: '',
  eventDateTime: '',
  place: '',
  information: '',
  private: true,
  participantsLimit: '',
};

const schema = yup.object().shape({
  name: yup.string().required().label('Event name'),
  eventPictureUrl: yup.string().label('Event picture URL'),
  eventDateTime: yup.date().required().label('Date and time'),
  place: yup.string().required().label('Place'),
  information: yup.string().label('Information about the event'),
  private: yup
    .bool()
    .required()
    .label('Would you like to make this event public?'),
  participantsLimit: yup
    .number()
    .positive()
    .label('Would you like to make this event public?'),
});

export function CreateEventForm({
  onSubmit,
  loading,
  isOn,
  handleToggle,
  setIsInviteDialogOpen,
}) {
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={schema}
      validateOnBlur={false}
      onSubmit={onSubmit}
    >
      <Form>
        <FormikField
          id="name"
          label="Event name"
          name="name"
          type="text"
          autoFocus="autofocus"
          autoComplete="off"
          autoCorrect="off"
          autoCapitalize="off"
        />
        <FormikField
          id="eventPictureUrl"
          label="Event picture URL"
          name="eventPictureUrl"
          type="text"
          autoComplete="off"
          autoCorrect="off"
          autoCapitalize="off"
        />
        <FormikField
          id="eventDateTime"
          label="Date and time"
          name="eventDateTime"
          type="datetime-local"
          autoComplete="off"
          autoCorrect="off"
          autoCapitalize="off"
        />
        <FormikField
          id="place"
          label="Place"
          name="place"
          type="text"
          autoComplete="off"
          autoCorrect="off"
          autoCapitalize="off"
        />
        <FormikField
          id="information"
          label="Information about the event"
          name="information"
          type="text"
          autoComplete="off"
          autoCorrect="off"
          autoCapitalize="off"
        />
        <Label htmlFor="private">Make the event public?</Label>
        <Switch
          id="private"
          label="Sharing settings"
          name="private"
          type="text"
          isOn={isOn}
          handleToggle={handleToggle}
        />
        {isOn ? (
          <br />
        ) : (
          <Button
            className="btn sendto"
            onClick={() => setIsInviteDialogOpen(true)}
          >
            Send to
          </Button>
        )}
        <FormikField
          id="participantsLimit"
          label="Maximum number of participants"
          name="participantsLimit"
          type="number"
          autoComplete="off"
          autoCorrect="off"
          autoCapitalize="off"
        />
        <Button type="submit" disabled={loading}>
          Create
        </Button>
      </Form>
    </Formik>
  );
}
