import React from 'react';
import { Form, Formik } from 'formik';
import * as yup from 'yup';

import { Button } from 'src/atoms';
import { FormikField } from 'src/molecules';

const schema = yup.object().shape({
  password: yup
    .string()
    .required()
    .label('Password')
    .matches(
      '(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}',
      'Password must be at least 8 characters long. It must contain a lowercase character, an uppercase character and a number.',
    ),
  confirmPassword: yup
    .string()
    .required()
    .oneOf([yup.ref('password'), null], 'Passwords must match')
    .label('Confirm password'),
});

export function PasswordResetForm({ onSubmit, userId, token, loading }) {
  const initialValues = {
    id: userId,
    token: token,
    password: '',
    confirmPassword: '',
  };

  return (
    <Formik
      onSubmit={onSubmit}
      initialValues={initialValues}
      validationSchema={schema}
      validateOnBlur={false}
    >
      <Form method="POST">
        <FormikField id="id" name="id" type="hidden" />
        <FormikField id="token" name="token" type="hidden" />
        <FormikField
          id="password"
          name="password"
          label="Password"
          type="password"
          autoFocus
          autoComplete="off"
          autoCorrect="off"
          autoCapitalize="off"
        />
        <FormikField
          id="confirmPassword"
          name="confirmPassword"
          label="Confirm password"
          type="password"
          autoComplete="off"
          autoCorrect="off"
          autoCapitalize="off"
        />
        <Button type="submit" disabled={loading}>
          Reset password
        </Button>
      </Form>
    </Formik>
  );
}
