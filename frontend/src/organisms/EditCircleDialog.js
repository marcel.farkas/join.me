import React, { useCallback, useEffect, useState } from 'react';
import { gql, useMutation, useQuery } from '@apollo/client';

import { Dialog } from 'src/organisms';
import { showToast } from 'src/utils/showToast';
import { EditCircleForm } from './EditCircleForm';

const CIRCLE_QUERY = gql`
  query CircleDetail($circleId: Int!) {
    circle(circleId: $circleId) {
      name
    }
  }
`;

const EDIT_CIRCLE_MUTATION = gql`
  mutation EditCircle($id: Int!, $name: String!, $invitedFriends: [Int]!) {
    editCircle(id: $id, name: $name, invitedFriends: $invitedFriends) {
      id
      name
    }
  }
`;

const FRIENDS_QUERY = gql`
  query Friends {
    friends {
      id
      first_name
      last_name
      profilePictureUrl
    }
  }
`;

export function EditCircleDialog({
  isOpen,
  circleId,
  contentLabel,
  closeDialog,
  //selectedFriends,
  refetchData,
}) {
  const { data: friendsData } = useQuery(FRIENDS_QUERY);
  const { data } = useQuery(CIRCLE_QUERY, {
    variables: { circleId },
  });
  const [selectedFriends, setSelectedFriends] = useState([]);

  const handleFriendChange = (checked, value) => {
    if (checked) {
      setSelectedFriends((prev) => [...prev, value]);
    } else {
      setSelectedFriends((prev) => prev.filter((item) => item !== value));
    }
  };

  const [editCircle, editCircleState] = useMutation(EDIT_CIRCLE_MUTATION, {
    onCompleted: () => {
      closeDialog();
      refetchData();
      showToast({ type: 'SUCCESS', msg: 'Circle edited successfully' });
    },
    onError: (error) => showToast({ type: 'ERROR', msg: error.message }),
  });

  const handleEditCircleFormSubmit = useCallback(
    (variables) => {
      let current;
      setSelectedFriends((currentState) => {
        current = currentState;
        return currentState;
      });

      editCircle({
        variables: {
          ...variables,
          id: circleId,
          invitedFriends: current,
        },
      });
    },
    [editCircle],
  );

  return (
    <Dialog
      isOpen={isOpen}
      closeDialog={() => {
        closeDialog();
      }}
      contentLabel={contentLabel}
      header="Edit circle"
    >
      {data && (
        <EditCircleForm
          onSubmit={handleEditCircleFormSubmit}
          initialData={data}
          loading={editCircleState.loading}
          friendsData={friendsData}
          selectedFriendsChange={(checked, value) =>
            handleFriendChange(checked, value)
          }
        />
      )}
    </Dialog>
  );
}
