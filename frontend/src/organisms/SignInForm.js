import React from 'react';
import { Form, Formik } from 'formik';
import * as yup from 'yup';

import { Button } from 'src/atoms';
import { FormikField } from 'src/molecules/';

const initialValues = {
  email: '@',
  password: '',
};

const schema = yup.object().shape({
  email: yup
    .string()
    .email()
    .required('E-mail is required in order to sign in.')
    .label('Email'),
  password: yup
    .string()
    .required('Password is required in order to sign in.')
    .label('Password'),
});

export function SignInForm({ loading, className, onSubmit, children }) {
  return (
    <Formik
      onSubmit={onSubmit}
      initialValues={initialValues}
      validationSchema={schema}
      validateOnBlur={false}
    >
      <Form className={className}>
        <FormikField
          id="email"
          name="email"
          label="Email"
          type="email"
          autoFocus="autofocus"
          autoComplete="on"
          autoCorrect="off"
          autoCapitalize="off"
        />
        <FormikField
          id="password"
          name="password"
          label="Password"
          type="password"
          autoComplete="off"
          autoCorrect="off"
          autoCapitalize="off"
        />
        <Button type="submit" disabled={loading}>
          Sign In
        </Button>
        {children}
      </Form>
    </Formik>
  );
}
