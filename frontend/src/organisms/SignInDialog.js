import React, { useCallback } from 'react';
import { useHistory } from 'react-router';
import { gql, useMutation } from '@apollo/client';

import { LinkButton } from 'src/atoms';
import { useAuth } from 'src/utils/auth';
import { Dialog, SignInForm } from 'src/organisms';
import { route } from 'src/Routes';

const SIGN_IN_MUTATION = gql`
  mutation SignIn($email: String!, $password: String!) {
    signin(email: $email, password: $password) {
      user {
        id
        email
      }
      token
    }
  }
`;

export function SignInDialog({
  isOpen,
  openDialog,
  closeDialog,
  contentLabel,
  showToast,
}) {
  const auth = useAuth();
  const history = useHistory();

  const [signIn, signInState] = useMutation(SIGN_IN_MUTATION, {
    onCompleted: ({ signin: { user, token } }) => {
      auth.signin({ token, user });
      closeDialog('signIn');
      history.push(route.homePage());
    },
    onError: () =>
      showToast({ type: 'ERROR', msg: 'ERROR: Could not sign you in.' }),
  });

  const handleSignInFormSubmit = useCallback(
    (variables) => {
      signIn({ variables });
    },
    [signIn],
  );

  return (
    <Dialog
      isOpen={isOpen}
      closeDialog={() => closeDialog('signIn')}
      contentLabel={contentLabel}
      header="Join.me account"
    >
      <div style={{ textAlign: 'center' }}>
        Please sign in into your account.
      </div>
      <br />
      <SignInForm
        loading={signInState.loading}
        errorMessage={signInState.error}
        onSubmit={handleSignInFormSubmit}
      />
      <LinkButton onClick={() => openDialog('passwordResetRequest')}>
        Reset password
      </LinkButton>
      <LinkButton onClick={() => openDialog('signUp')}>Sign up</LinkButton>
    </Dialog>
  );
}
