import React from 'react';
import { EventListwithFilter, TopBar } from 'src/organisms';

export function HomeTemplate({
  data,
  joinedEvents,
  refetchData,
  currentUserId,
}) {
  return (
    <>
      <TopBar />
      <div className="dim">
        {data && (
          <EventListwithFilter
            data={{
              events: data?.upcomingEvents.filter((item) => !item.public),
            }}
            joinedEvents={joinedEvents}
            itemsPerPage={4}
            showJoinBtn
            refetchData={refetchData}
            currentUserId={currentUserId}
          />
        )}
      </div>
    </>
  );
}
