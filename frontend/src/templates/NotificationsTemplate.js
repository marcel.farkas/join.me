import React from 'react';

import { NotificationList, TopBar } from 'src/organisms';

export function NotificationsTemplate() {
  return (
    <>
      <TopBar />

      <div className="dim">
        <NotificationList />
      </div>
    </>
  );
}
