import React from 'react';

import { CircleList, TopBar } from 'src/organisms';

export function CirclesTemplate() {
  return (
    <>
      <TopBar />
      <div className="dim">
        <CircleList />
      </div>
    </>
  );
}
