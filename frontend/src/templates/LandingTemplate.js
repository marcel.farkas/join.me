import React from 'react';

import { Button } from 'src/atoms';
import {
  LandingTopBar,
  SignInDialog,
  SignUpDialog,
  PasswordResetRequestDialog,
  PasswordResetDialog,
  ConfirmEmailDialog,
} from 'src/organisms/';

export function LandingTemplate({
  userId,
  token,
  isSignInDialogOpen,
  isSignUpDialogOpen,
  isPasswordResetRequestDialogOpen,
  isPasswordResetDialogOpen,
  isConfirmEmailDialogOpen,
  openDialog,
  closeDialog,
  showToast,
}) {
  return (
    <div id="Landing_Page">
      <LandingTopBar openDialog={(name) => openDialog(name)} />
      <div>
        <h1>INVITE YOUR FRIENDS TO JOIN YOU</h1>
        <p>
          Inviting your friends has never been that easy. Create an event, send
          invitation to a circle of friends and meet in a chat room. Want other
          people to discover your event? We can help you with that. You create,
          we promote.
        </p>
        <Button
          className="btn_lp center mont"
          animated
          onClick={() => openDialog('signUp')}
        >
          Get started!
        </Button>
      </div>
      <SignInDialog
        isOpen={isSignInDialogOpen}
        openDialog={openDialog}
        closeDialog={closeDialog}
        contentLabel="Sign in dialog"
        showToast={showToast}
      />
      <SignUpDialog
        isOpen={isSignUpDialogOpen}
        openDialog={openDialog}
        closeDialog={closeDialog}
        contentLabel="Sign up dialog"
        showToast={showToast}
      />
      <PasswordResetRequestDialog
        isOpen={isPasswordResetRequestDialogOpen}
        closeDialog={closeDialog}
        contentLabel="Request password reset dialog"
        showToast={showToast}
      />
      <PasswordResetDialog
        userId={userId}
        token={token}
        isOpen={isPasswordResetDialogOpen}
        openDialog={openDialog}
        closeDialog={closeDialog}
        contentLabel="Password reset dialog"
        showToast={showToast}
      />
      <ConfirmEmailDialog
        userId={userId}
        token={token}
        isOpen={isConfirmEmailDialogOpen}
        openDialog={openDialog}
        closeDialog={closeDialog}
        contentLabel="Confirm email dialog"
        showToast={showToast}
      />
    </div>
  );
}
