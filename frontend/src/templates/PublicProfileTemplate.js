import React, { useState } from 'react';

import {
  ProfileDescription,
  ProfileName,
  ProfilePicture,
  Button,
} from 'src/atoms';
import { TopBar, PublicEventList } from 'src/organisms';

export function PublicProfileTemplate({
  profileData,
  loading,
  error,
  refetchData,
  currentUserId,
  handleOnFriendBtnClick,
  isFriend,
}) {
  const [active, setActive] = useState('upcomingEvents');

  if (loading) return 'loading...';
  if (error) return 'error loading data.';

  const publicEvents = profileData?.user[active]?.filter(
    (event) => event.private === false,
  );

  return (
    <>
      <TopBar />
      <div className="dim">
        <div className="wrapper_p">
          <ProfilePicture
            className="profil"
            profilePictureUrl={profileData.user.profilePictureUrl}
            loading={loading}
            error={error}
          />
          <div className="p_info">
            <div className="myname">
              <ProfileName
                profileData={profileData}
                loading={loading}
                error={error}
                refetchData={refetchData}
              />
            </div>
            <ProfileDescription
              profileData={profileData}
              loading={loading}
              error={error}
              refetchData={refetchData}
            />
          </div>
          <div className="add_friend">
            <ul>
              <li>
                <Button
                  onClick={() => {
                    handleOnFriendBtnClick(profileData.user.id);
                  }}
                >
                  {!isFriend ? 'Add Friend' : 'Remove Friend'}
                </Button>
              </li>
            </ul>
          </div>
          <div className="menu">
            <ul>
              <li>
                <Button onClick={() => setActive('upcomingEvents')}>
                  Joined events
                </Button>
              </li>
              <li>
                <Button onClick={() => setActive('pastEvents')}>
                  Past events
                </Button>
              </li>
            </ul>
          </div>
        </div>
        {profileData && (
          <PublicEventList
            data={{ events: publicEvents }}
            itemsPerPage={4}
            joinedEvents={profileData.user.joinedEvents}
            refetchData={refetchData}
            currentUserId={currentUserId}
          />
        )}
      </div>
    </>
  );
}
