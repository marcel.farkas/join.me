import React from 'react';

import { TopBar, EventList } from 'src/organisms';
import '../switch.css';

export function InvitationsTemplate({
  invitationData,
  loading,
  error,
  refetchData,
  currentUserId,
}) {
  const isInvPage = true;
  const eventsArray = [];
  let acceptedEvents = [];

  invitationData?.invitations.map((element) => eventsArray.push(element.event));

  acceptedEvents = eventsArray.map((event, index) => {
    return {
      ...event,
      accepted: invitationData?.invitations[index].accepted,
    };
  });

  if (loading) return <div>loading...</div>;

  if (error) return <div>error...</div>;

  return (
    <>
      <TopBar />
      <div className="dim">
        <div className="inv_header">Invitations</div>
        {eventsArray && (
          <EventList
            data={{ events: acceptedEvents }}
            itemsPerPage={4}
            showJoinBtn
            isInvPage={isInvPage}
            refetchData={refetchData}
            currentUserId={currentUserId}
          />
        )}
      </div>
    </>
  );
}
