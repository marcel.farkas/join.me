import React, { useState } from 'react';

import {
  ProfileDescription,
  ProfileName,
  ContactCount,
  CircleCount,
  EventCount,
  ProfilePicture,
  Button,
  SettingsIcon,
  CalendarIcon,
} from 'src/atoms';

import {
  TopBar,
  EventList,
  CreateEventDialog,
  SettingsDialog,
  EventCalendarDialog,
} from 'src/organisms';

let selectedCircles = [];
let selectedFriends = [];

export function ProfileTemplate({
  profileData,
  loading,
  error,
  refetchData,
  isCreateEventDialogOpen,
  setIsCreateEventDialogOpen,
  isEventCalendarDialogOpen,
  setIsEventCalendarDialogOpen,
  isSettingsDialogOpen,
  setIsSettingsDialogOpen,
  isInviteDialogOpen,
  setIsInviteDialogOpen,
  currentUserId,
}) {
  const [active, setActive] = useState('createdEvents');

  if (!profileData) return <div>Loading...</div>;

  return (
    <>
      <TopBar />
      <div className="dim">
        <div className="wrapper_p">
          <ProfilePicture
            className="profil"
            profilePictureUrl={profileData.user.profilePictureUrl}
            loading={loading}
            error={error}
          />
          <div className="p_info">
            <div className="myname">
              <ProfileName
                profileData={profileData}
                loading={loading}
                error={error}
                refetchData={refetchData}
              />
              <Button
                className="right settings"
                onClick={() => setIsSettingsDialogOpen(true)}
              >
                <span>Settings</span>
                <SettingsIcon />
              </Button>
              <Button
                className="right cal_icon"
                onClick={() => setIsEventCalendarDialogOpen(true)}
              >
                <CalendarIcon />
              </Button>
            </div>
            <ProfileDescription
              profileData={profileData}
              loading={loading}
              error={error}
              refetchData={refetchData}
            />
            <ul>
              <ContactCount
                profileData={profileData}
                loading={loading}
                error={error}
              />
              <CircleCount
                profileData={profileData}
                loading={loading}
                error={error}
              />
              <EventCount
                profileData={profileData}
                loading={loading}
                error={error}
              />
            </ul>
          </div>
          <div className="menu ml0">
            <ul>
              <li>
                <Button onClick={() => setActive('createdEvents')}>
                  My events
                </Button>
              </li>
              <li>
                <Button onClick={() => setActive('upcomingEvents')}>
                  Joined events
                </Button>
              </li>
              <li>
                <Button onClick={() => setActive('pastEvents')}>
                  Past events
                </Button>
              </li>

              <li>
                <Button onClick={() => setIsCreateEventDialogOpen(true)}>
                  Create event
                </Button>
              </li>
            </ul>
          </div>
        </div>
        {profileData && (
          <EventList
            data={{ events: profileData.user[active] }}
            itemsPerPage={4}
            showJoinBtn={active === 'upcomingEvents'}
            showChatBtn={active === 'upcomingEvents'}
            showEditBtn={active === 'createdEvents'}
            showFeedBtn={
              active === 'upcomingEvents' || active === 'createdEvents'
            }
            showDeleteBtn={active === 'createdEvents'}
            joinedEvents={profileData.user.joinedEvents}
            refetchData={refetchData}
            isInviteDialogOpen={isInviteDialogOpen}
            setIsInviteDialogOpen={setIsInviteDialogOpen}
            selectedCircles={selectedCircles}
            selectedFriends={selectedFriends}
            currentUserId={currentUserId}
          />
        )}
        <SettingsDialog
          isOpen={isSettingsDialogOpen}
          setIsSettingsDialogOpen={setIsSettingsDialogOpen}
        />
        <EventCalendarDialog
          events={profileData.user.joinedEvents}
          isOpen={isEventCalendarDialogOpen}
          setIsEventCalendarDialogOpen={setIsEventCalendarDialogOpen}
          refetchData={refetchData}
          currentUserId={currentUserId}
        />
        <CreateEventDialog
          isOpen={isCreateEventDialogOpen}
          setIsCreateEventDialogOpen={setIsCreateEventDialogOpen}
          isInviteDialogOpen={isInviteDialogOpen}
          setIsInviteDialogOpen={setIsInviteDialogOpen}
          refetchData={refetchData}
          selectedCircles={selectedCircles}
          selectedFriends={selectedFriends}
        />
      </div>
    </>
  );
}
