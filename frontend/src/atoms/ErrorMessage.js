import React from 'react';

export function ErrorMessage({ className, ...props }) {
  return <div className="errorMessage" {...props} />;
}
