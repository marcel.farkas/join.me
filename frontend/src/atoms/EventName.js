import React from 'react';
export function ProfileName({ profileData, loading, error }) {
  if (loading) {
    return <div className="pt2 black-90 pre-line break-word">Loading...</div>;
  }

  if (error) {
    return (
      <div className="pt2 black-90 pre-line break-word">Error loading data</div>
    );
  }

  return (
    <div className="pt2 black-90 pre-line break-word">
      {profileData.user.first_name} {profileData.user.last_name}
    </div>
  );
}
