import React from 'react';
export function EventCount({ profileData, loading, error }) {
  if (loading) {
    return <li className="pt2 black-90 pre-line break-word">Loading...</li>;
  }

  if (error) {
    return (
      <li className="pt2 black-90 pre-line break-word">Error loading data</li>
    );
  }

  return (
    <li className="pt2 black-90 pre-line break-word">
      {profileData.user.eventCount} Events
    </li>
  );
}
