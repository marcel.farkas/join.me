import React from 'react';

export function ProfileIcon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="50"
      height="50"
      viewBox="0 0 50 50"
    >
      <path
        id="Icon_open-person"
        data-name="Icon open-person"
        d="M25,0C18.125,0,12.5,7,12.5,15.625S18.125,31.25,25,31.25s12.5-7,12.5-15.625S31.875,0,25,0ZM11.938,31.25A12.513,12.513,0,0,0,0,43.75V50H50V43.75a12.469,12.469,0,0,0-11.937-12.5A17.553,17.553,0,0,1,25,37.5,17.553,17.553,0,0,1,11.938,31.25Z"
      />
    </svg>
  );
}
