import classNames from 'classnames';
import React from 'react';

export function Select({ className, error, children, ...props }) {
  return (
    <select
      className={classNames(error ? 'input-error' : '', className)}
      {...props}
    >
      {children}
    </select>
  );
}
