import React from 'react';
import classNames from 'classnames';

export function Button({
  children,
  color,
  animated,
  as: Component = 'button',
  disabled,
  ...rest
}) {
  return (
    <Component
      className={classNames('btn', animated ? 'btn-anim' : '')}
      type="button"
      disabled={disabled}
      {...rest}
    >
      {children}
    </Component>
  );
}

Button.defaultProps = {
  animated: false,
};
