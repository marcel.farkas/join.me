import React from 'react';
import classNames from 'classnames';

export function LinkButton({
  children,
  className,
  as: Component = 'button',
  disabled,
  ...rest
}) {
  return (
    <Component
      className={classNames('link-button')}
      type="button"
      disabled={disabled}
      {...rest}
    >
      {children}
    </Component>
  );
}
