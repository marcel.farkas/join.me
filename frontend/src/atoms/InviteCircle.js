import React, { useState } from 'react';
import { Button } from 'src/atoms';

export function InviteCircle({ circle, selectedCircles, onCircleAdd }) {
  const [isInvited, setIsInvited] = useState({
    id: circle.id,
    invited: selectedCircles.indexOf(circle) === -1 ? true : false,
  });
  const selectItem = () => {
    setIsInvited((prev) => ({ ...prev, invited: !prev.invited }));
  };
  return (
    <Button
      onClick={() => {
        onCircleAdd(circle, selectedCircles);
        selectItem();
      }}
    >
      <span className="left">{circle.name}</span>{' '}
      <span className="right">
        {isInvited.invited ? 'Add Circle' : 'Remove Circle'}
      </span>
    </Button>
  );
}
