import React from 'react';
import classNames from 'classnames';

export function ProfilePicture({ profilePictureUrl, loading, className, alt }) {
  if (loading) {
    return <div>Loading...</div>;
  }

  const pictureUrl =
    profilePictureUrl || 'https://www.w3schools.com/howto/img_avatar.png';

  return (
    <img
      id="profilePicture"
      alt={alt}
      src={pictureUrl}
      className={classNames(className)}
    />
  );
}
