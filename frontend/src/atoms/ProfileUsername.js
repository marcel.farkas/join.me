import React from 'react';
export function ProfileUsername({ profileData, loading, error }) {
  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error loading data</div>;
  }

  return <div>{profileData.user.userName}</div>;
}
