import React, { useState } from 'react';
import { Button } from 'src/atoms';

export function InviteFriend({ friend, selectedFriends, onFriendAdd }) {
  const [isInvited, setIsInvited] = useState({
    id: friend.id,
    invited: selectedFriends.indexOf(friend) === -1 ? true : false,
  });
  const selectItem = () => {
    setIsInvited((prev) => ({ ...prev, invited: !prev.invited }));
  };

  return (
    <Button
      onClick={() => {
        onFriendAdd(friend, selectedFriends);
        selectItem();
      }}
    >
      <span className="left">
        {friend.first_name} {friend.last_name}
      </span>{' '}
      <span className="right">
        {isInvited.invited ? 'Add Friend' : 'Remove Friend'}
      </span>
    </Button>
  );
}
