import React, { useEffect, useState } from 'react';
import { Field } from 'formik';
const classNames = require('classnames');

export function CheckboxButton({ name, value, text, className, handleChange }) {
  const [checked, setChecked] = useState(false);

  useEffect(() => {
    handleChange(checked, value);
  }, [checked]);

  return (
    <label
      style={{
        textAlign: 'center',
        border: '2px solid #b524ff',
        borderRadius: '10px',
        boxSizing: 'border-box',
        padding: '8px 16px',
        cursor: 'pointer',
        marginBottom: '10px',
      }}
      className={classNames(className, {
        'checkbox-button-checked': checked,
      })}
    >
      {text}
      <Field
        type="checkbox"
        name={name}
        value={value}
        hidden
        onChange={(evt) => {
          setChecked(!checked);
        }}
      />
    </label>
  );
}
