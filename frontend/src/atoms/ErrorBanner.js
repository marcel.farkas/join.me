import React from 'react';
import classNames from 'classnames';

export function ErrorBanner({ className, title, children, ...props }) {
  return (
    <div className={classNames('', className)} {...props}>
      <div
        className={classNames('', {
          mb3: !!children,
        })}
      >
        <span className="">{title || 'Unknown error'}</span>
      </div>
      <div className="">{children}</div>
    </div>
  );
}
