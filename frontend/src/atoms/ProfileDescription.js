import React from 'react';
export function ProfileDescription({ profileData, loading, error }) {
  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error loading data</div>;
  }

  return (
    <div style={{ marginRight: '16px' }}>
      <span>{profileData.user.description}</span>
    </div>
  );
}
