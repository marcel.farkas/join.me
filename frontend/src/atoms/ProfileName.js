import React from 'react';
export function ProfileName({ profileData, loading, error }) {
  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error loading data</div>;
  }

  return (
    <div className="inline_block">
      {profileData.user.first_name} {profileData.user.last_name}
    </div>
  );
}
