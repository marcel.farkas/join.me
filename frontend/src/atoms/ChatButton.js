import React from 'react';

import { Button } from '.';

export function ChatButton({ openChatDialog, eventId }) {
  return (
    <Button className="btn_svg" onClick={() => openChatDialog(eventId)}>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="34.092"
        height="34.097"
        viewBox="0 0 34.092 34.097"
      >
        <g
          id="Icon_ionic-ios-chatboxes"
          data-name="Icon ionic-ios-chatboxes"
          transform="translate(-3.375 -3.375)"
        >
          <path
            id="Path_61"
            data-name="Path 61"
            d="M32.974,11.981H14.809A3.128,3.128,0,0,0,11.686,15.1V27.752a3.121,3.121,0,0,0,3.123,3.115H25.2a.9.9,0,0,1,.623.262l5.164,4.763c.287.279.762.164.762-.238V31.507c0-.492.311-.648.8-.648h.082a3.412,3.412,0,0,0,3.451-3.115V15.1A3.11,3.11,0,0,0,32.974,11.981Z"
            transform="translate(1.378 1.427)"
          />
          <path
            id="Path_62"
            data-name="Path 62"
            d="M13.736,11.4H28.278V6.047a2.673,2.673,0,0,0-2.672-2.672H6.047A2.673,2.673,0,0,0,3.375,6.047V19.589a2.673,2.673,0,0,0,2.672,2.672h5.008V14.072A2.685,2.685,0,0,1,13.736,11.4Z"
          />
        </g>
      </svg>
    </Button>
  );
}
