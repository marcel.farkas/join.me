import classNames from 'classnames';
import React from 'react';

export function TextInput({ className, error, ...props }) {
  return (
    <input
      type="text"
      className={classNames(error ? 'input-error' : '', className)}
      {...props}
    />
  );
}
