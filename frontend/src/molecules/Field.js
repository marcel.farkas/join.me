import React from 'react';

import { Label, TextInput, ErrorMessage } from 'src/atoms';

export function Field({ id, label, error, ...props }) {
  return (
    <div>
      <Label htmlFor={id}>{label}</Label>
      <TextInput id={id} className="" error={!!error} {...props} />
      {error && <ErrorMessage className="">{error}</ErrorMessage>}
    </div>
  );
}
