import React, { useState } from 'react';
import { gql, useMutation } from '@apollo/client';
import { Button } from 'src/atoms';
import Img from 'src/images/Background.png';
import { showToast } from 'src/utils/showToast';
import { EditEventDialog, EventDetailsDialog } from 'src/organisms';
import { EventInfo, EventRight } from 'src/molecules';

const JOIN_EVENT_MUTATION = gql`
  mutation JoinEvent($eventId: Int!) {
    joinEvent(eventId: $eventId) {
      id
      name
    }
  }
`;
const LEAVE_EVENT_MUTATION = gql`
  mutation LeaveEvent($eventId: Int!) {
    leaveEvent(eventId: $eventId) {
      id
      name
    }
  }
`;
const DECLINE_EVENT_MUTATION = gql`
  mutation DeclineInvitation($eventId: Int!) {
    declineInvitation(eventId: $eventId) {
      id
      name
    }
  }
`;

const DELETE_EVENT_MUTATION = gql`
  mutation DeleteEvent($eventId: Int!) {
    deleteEvent(eventId: $eventId)
  }
`;

export function Event({
  event,
  joinedEvents,
  loading,
  error,
  showJoinBtn = false,
  showChatBtn,
  showEditBtn,
  showFeedBtn,
  showDeleteBtn,
  refetchData,
  isInvPage,
  isInviteDialogOpen,
  setIsInviteDialogOpen,
  selectedCircles,
  selectedFriends,
  openFeedDialog,
  openChatDialog,
  currentUserId,
}) {
  const [isEditEventDialogOpen, setIsEditEventDialogOpen] = useState(false);
  const [isEventDetailsDialogOpen, setIsEventDetailsDialogOpen] =
    useState(false);

  const [joinEvent] = useMutation(JOIN_EVENT_MUTATION, {
    onCompleted: ({ joinEvent: { name } }) => {
      if (isInvPage)
        showToast({
          type: 'SUCCESS',
          msg: `You accepted invitation to event ${name}`,
        });
      else showToast({ type: 'SUCCESS', msg: `You joined event ${name}` });
      refetchData();
    },
    onError: (error) => {
      showToast({ type: 'ERROR', msg: error.message });
      refetchData();
    },
  });

  const [leaveEvent] = useMutation(LEAVE_EVENT_MUTATION, {
    onCompleted: ({ leaveEvent: { name } }) => {
      showToast({ type: 'SUCCESS', msg: `You left event ${name}` });
      refetchData();
    },
    onError: (error) => {
      showToast({ type: 'ERROR', msg: error.message });
      refetchData();
    },
  });

  const [declineInvitation] = useMutation(DECLINE_EVENT_MUTATION, {
    onCompleted: ({ declineInvitation: { name } }) => {
      showToast({
        type: 'SUCCESS',
        msg: `You declined invitation to event ${name}`,
      });
      refetchData();
    },
    onError: (error) => {
      showToast({ type: 'ERROR', msg: error.message });
      refetchData();
    },
  });

  const [deleteEvent] = useMutation(DELETE_EVENT_MUTATION, {
    onCompleted: () => {
      showToast({
        type: 'SUCCESS',
        msg: `You deleted the event successfully`,
      });
      refetchData();
    },
    onError: (error) => {
      showToast({ type: 'ERROR', msg: error.message });
      refetchData();
    },
  });

  const handleJoinButtonClick = (eventId) => {
    joinEvent({ variables: { eventId } });
  };

  const handleLeaveButtonClick = (eventId) => {
    leaveEvent({ variables: { eventId } });
  };

  const handleDeclineButtonClick = (eventId) => {
    declineInvitation({ variables: { eventId } });
  };

  const handleDeleteEvent = (eventId) => {
    deleteEvent({ variables: { eventId } });
  };

  const AcceptDeclineBtn = (eventId) => {
    if (!showJoinBtn) return null;

    if (isInvPage) {
      return (
        <div>
          <Button
            className="btn_pink"
            onClick={() => handleJoinButtonClick(eventId)}
          >
            Accept
          </Button>
          <Button
            className="btn_pink"
            onClick={() => handleDeclineButtonClick(eventId)}
          >
            Decline
          </Button>
        </div>
      );
    }
  };

  const JoinBtn = (eventId, isPrivate) => {
    if (!showJoinBtn) return null;
    const joinedEventsIds = joinedEvents.map((event) => event.id);
    if (joinedEventsIds.includes(eventId)) {
      return (
        <Button
          className="btn_pink"
          onClick={() => handleLeaveButtonClick(eventId)}
        >
          Leave
        </Button>
      );
    }
    return (
      <Button
        className="btn_pink"
        onClick={() => handleJoinButtonClick(eventId)}
      >
        {isPrivate ? 'Send request' : 'Join'}
      </Button>
    );
  };

  return (
    <>
      <div className="event_box">
        <div>
          <img
            src={event.eventPictureUrl || Img}
            alt="Event"
            onClick={() => setIsEventDetailsDialogOpen(true)}
          />
        </div>
        <EventInfo
          event={event}
          setIsEventDetailsDialogOpen={setIsEventDetailsDialogOpen}
          loading={loading}
          error={error}
          currentUserId={currentUserId}
        />
        <EventRight
          event={event}
          showChatBtn={showChatBtn}
          showEditBtn={showEditBtn}
          showFeedBtn={showFeedBtn}
          showDeleteBtn={showDeleteBtn}
          isInvPage={isInvPage}
          acceptDeclineBtn={AcceptDeclineBtn}
          joinBtn={JoinBtn}
          joinedEvents={joinedEvents}
          openFeedDialog={openFeedDialog}
          openChatDialog={openChatDialog}
          deleteEvent={handleDeleteEvent}
          setIsEditEventDialogOpen={setIsEditEventDialogOpen}
        />
      </div>
      <EventDetailsDialog
        isOpen={isEventDetailsDialogOpen}
        setIsEventDetailsDialogOpen={setIsEventDetailsDialogOpen}
        event={event}
        loading={loading}
        error={error}
        refetchData={refetchData}
        joinedEvents={joinedEvents}
        isInvPage={isInvPage}
        currentUserId={currentUserId}
      />
      <EditEventDialog
        isOpen={isEditEventDialogOpen}
        setIsEditEventDialogOpen={setIsEditEventDialogOpen}
        event={event}
        refetchData={refetchData}
        isInviteDialogOpen={isInviteDialogOpen}
        setIsInviteDialogOpen={setIsInviteDialogOpen}
        selectedCircles={selectedCircles}
        selectedFriends={selectedFriends}
      />
    </>
  );
}
