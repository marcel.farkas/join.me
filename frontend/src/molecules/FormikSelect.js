import React from 'react';
import { useField } from 'formik';

import { SelectField } from './SelectField';

export function FormikSelect({ name, children, ...props }) {
  const [field, meta] = useField(name);

  const error = meta.touched && meta.error;

  return (
    <SelectField {...field} error={error} {...props}>
      {children}
    </SelectField>
  );
}
