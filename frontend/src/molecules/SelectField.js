import React from 'react';

import { Label, Select, ErrorMessage } from 'src/atoms';

export function SelectField({ id, label, error, ...props }) {
  return (
    <div className="">
      <Label htmlFor={id}>{label}</Label>
      <Select id={id} className="" error={!!error} {...props} />
      {error && <ErrorMessage className="">{error}</ErrorMessage>}
    </div>
  );
}
