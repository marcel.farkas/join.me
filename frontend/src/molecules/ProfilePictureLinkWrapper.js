import React from 'react';
import { ProfilePicture } from 'src/atoms';
import { NavLink } from 'react-router-dom';

export function ProfilePictureLinkWrapper({
  className,
  profilePictureUrl,
  loading,
  error,
  user,
  imageSize,
  currentUserId,
}) {
  if (loading) {
    return <div>Loading...</div>;
  }

  if (currentUserId === user) {
    return (
      <NavLink to={`/profile/`}>
        <ProfilePicture
          className={className}
          user={user}
          profilePictureUrl={profilePictureUrl}
          loading={loading}
          error={error}
          imageSize={imageSize}
        />
      </NavLink>
    );
  } else
    return (
      <NavLink to={`/profile/${user}`}>
        <ProfilePicture
          className={className}
          user={user}
          profilePictureUrl={profilePictureUrl}
          loading={loading}
          error={error}
          imageSize={imageSize}
        />
      </NavLink>
    );
}
