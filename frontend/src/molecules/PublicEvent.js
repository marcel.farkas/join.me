import React, { useState } from 'react';
import { ProfilePictureLinkWrapper } from 'src/molecules';
import Img from 'src/images/Background.png';
import { PublicEventDetailsDialog } from 'src/organisms';

export function PublicEvent({
  event,
  joinedEvents,
  loading,
  error,
  refetchData,
  currentUserId,
}) {
  const [isEventDetailsDialogOpen, setIsEventDetailsDialogOpen] =
    useState(false);

  return (
    <>
      <div className="event_box">
        <div>
          <img
            src={event.eventPictureUrl || Img}
            alt="Event"
            onClick={() => setIsEventDetailsDialogOpen(true)}
          />
        </div>
        <div className="event_info">
          <span
            className="event_name"
            onClick={() => setIsEventDetailsDialogOpen(true)}
          >
            {event.name}
          </span>
          <span>
            {new Date(parseFloat(event.eventDateTime)).toLocaleString()}
          </span>
          <span>{event.place}</span>
          <div className="participants">
            {event.participants.length
              ? event.participants.map((participant) => (
                  <ProfilePictureLinkWrapper
                    className="profil"
                    user={participant?.id}
                    key={participant.id}
                    profilePictureUrl={participant.profilePictureUrl}
                    loading={loading}
                    error={error}
                    imageSize={40}
                    currentUserId={currentUserId}
                  />
                ))
              : 'No participants'}
          </div>
        </div>
        <div className="event_right">
          <span>
            Created by:{' '}
            <span className="bold">
              {event.creator.first_name} {event.creator.last_name}
            </span>
          </span>
          <span>{event.private ? 'Private' : 'Public'}</span>
        </div>
      </div>
      <PublicEventDetailsDialog
        isOpen={isEventDetailsDialogOpen}
        setIsEventDetailsDialogOpen={setIsEventDetailsDialogOpen}
        event={event}
        loading={loading}
        error={error}
        refetchData={refetchData}
        joinedEvents={joinedEvents}
        currentUserId={currentUserId}
      />
    </>
  );
}
