import React from 'react';
import { gql, useMutation } from '@apollo/client';
import { showToast } from 'src/utils/showToast';

import { DeleteButton, EditButton } from 'src/atoms';

const DELETE_CIRCLE_MUTATION = gql`
  mutation DeleteCircle($circleId: Int!) {
    deleteCircle(circleId: $circleId)
  }
`;

export function Circle({ circle, refetchData, openEditDialog }) {
  const [deleteCircle] = useMutation(DELETE_CIRCLE_MUTATION, {
    onCompleted: () => {
      showToast({
        type: 'SUCCESS',
        msg: `You deleted the event successfully`,
      });
      refetchData();
    },
    onError: (error) => {
      showToast({ type: 'ERROR', msg: error.message });
      refetchData();
    },
  });

  const handleDeleteCircle = (circleId) => {
    deleteCircle({ variables: { circleId } });
  };

  return (
    <div
      className="circle"
      style={{
        backgroundColor: 'white',
        height: '50px',
        display: 'flex',
        alignItems: 'center',
        marginBottom: '10px',
      }}
    >
      <div style={{ flexGrow: '1', marginLeft: '10px' }}>{circle.name}</div>
      <div style={{ flexGrow: '8', textAlign: 'right' }}>
        <EditButton handleEditPress={() => openEditDialog(circle.id)} />

        <DeleteButton
          handleDeletePress={handleDeleteCircle}
          itemId={circle.id}
        />
      </div>
    </div>
  );
}
