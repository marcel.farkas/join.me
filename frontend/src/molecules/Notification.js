import React from 'react';
import { useHistory } from 'react-router';

import { Button } from 'src/atoms';
import { route } from 'src/Routes';

export function Notification({ data, deleteNotification }) {
  const history = useHistory();

  const handleNotificationClick = () => {
    switch (data.type) {
      case 'EVENT':
        history.push(route.profilePage());
        break;
      case 'CIRCLE':
        history.push(route.circlesPage());
        break;
      case 'CHAT':
        history.push(route.profilePage());
        break;
      default:
    }
  };

  return (
    <div className="notif">
      <div
        className="notif_msg"
        style={{ flexGrow: '1', marginLeft: '10px' }}
        onClick={handleNotificationClick}
      >
        {data.message}
      </div>
      <div
        className="notif_bot"
        style={{ flexGrow: '8', textAlign: 'right', height: '100%' }}
      >
        <span
          className="notif_type"
          style={{
            marginRight: '10px',
            color: '#065dd8',
            verticalAlign: 'super',
          }}
          onClick={handleNotificationClick}
        >
          {data.type}
        </span>
        <Button
          className="btn btn_x"
          style={{ width: '50px', height: '100%' }}
          onClick={() => deleteNotification(data.id)}
        >
          X
        </Button>
      </div>
    </div>
  );
}
