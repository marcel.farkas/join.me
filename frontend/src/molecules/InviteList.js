import React from 'react';
import { InviteCircle, InviteFriend } from '../atoms';

export function InviteList({
  circleData,
  friendData,
  selectedCircles,
  selectedFriends,
  onCircleAdd,
  onFriendAdd,
}) {
  return (
    <>
      <div className="invite_dialog">
        <div>
          <span className="invite_title">Select circles</span>
          <div style={{ overflow: 'auto' }}>
            {circleData?.data?.userCircles.map((circle) => (
              <InviteCircle
                key={circle.id}
                circle={circle}
                selectedCircles={selectedCircles}
                onCircleAdd={onCircleAdd}
              />
            ))}
          </div>
        </div>
        <div>
          <span className="invite_title">Invite your friends</span>
          <div style={{ overflow: 'auto' }}>
            {friendData?.data?.friends.map((friend) => (
              <InviteFriend
                key={friend.id}
                friend={friend}
                selectedFriends={selectedFriends}
                onFriendAdd={onFriendAdd}
              />
            ))}
          </div>
        </div>
      </div>
    </>
  );
}
