import React from 'react';

export function SendMessageForm({ handleSendMessage }) {
  return (
    <form onSubmit={handleSendMessage}>
      <input
        type="text"
        name="messageInput"
        placeholder="Type your message"
        autoComplete="off"
      ></input>
      <button className="btn" type="submit">
        Send
      </button>
    </form>
  );
}
