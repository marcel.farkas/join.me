import React from 'react';
import { InviteFriend } from '../atoms';

export function CreateCircleList({
  friendData,
  selectedFriends,
  onFriendAdd,
}) {
  return(
    <>
      <div style={{ overflow: 'auto' }}>
        {friendData?.data?.friends.map((friend) => (
          <InviteFriend
            friend={friend}
            selectedFriends={selectedFriends}
            onFriendAdd={onFriendAdd}
          />
        ))}
      </div>
    </>
  );
}
