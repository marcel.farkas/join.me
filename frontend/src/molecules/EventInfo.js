import React from 'react';

import { ProfilePictureLinkWrapper } from 'src/molecules';

export function EventInfo({
  event,
  setIsEventDetailsDialogOpen,
  loading,
  error,
  currentUserId,
}) {
  return (
    <div className="event_info">
      <span
        className="event_name"
        onClick={() => setIsEventDetailsDialogOpen(true)}
      >
        {event.name}
      </span>
      <span>{new Date(parseFloat(event.eventDateTime)).toLocaleString()}</span>
      <span>{event.place}</span>
      <span>
        Capacity: {event.participantsCount}/
        {event.participantsLimit || 'Unlimited'}
      </span>
      <div className="participants">
        {event.participants.length
          ? event.participants.map((participant) => (
              <ProfilePictureLinkWrapper
                className="profil"
                key={participant.id}
                profilePictureUrl={participant.profilePictureUrl}
                loading={loading}
                error={error}
                imageSize={40}
                currentUserId={currentUserId}
                user={participant.id}
              />
            ))
          : 'No participants'}
      </div>
    </div>
  );
}
