import React from 'react';

import { ChatButton, EditButton, FeedButton, DeleteButton } from 'src/atoms';

export function EventRight({
  event,
  showChatBtn = false,
  showEditBtn = false,
  showFeedBtn = false,
  showDeleteBtn = false,
  isInvPage,
  openChatDialog,
  openFeedDialog,
  deleteEvent,
  acceptDeclineBtn,
  joinBtn,
  joinedEvents,
  setIsEditEventDialogOpen,
}) {
  return (
    <div className="event_right">
      {showChatBtn && (
        <ChatButton openChatDialog={openChatDialog} eventId={event.id} />
      )}
      {showEditBtn && <EditButton handleEditPress={setIsEditEventDialogOpen} />}
      {showFeedBtn && (
        <FeedButton openFeedDialog={openFeedDialog} eventId={event.id} />
      )}
      {showDeleteBtn && (
        <DeleteButton handleDeletePress={deleteEvent} itemId={event.id} />
      )}
      <span>
        Created by:{' '}
        <span className="bold">
          {event.creator.first_name} {event.creator.last_name}
        </span>
      </span>
      <span>{event.private ? 'Private' : 'Public'}</span>
      {joinedEvents && joinBtn(event.id, event.private)}
      {isInvPage && acceptDeclineBtn(event.id)}
    </div>
  );
}
