import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { HomePage } from 'src/pages/HomePage';
import { ProfilePage } from 'src/pages/ProfilePage';
import { PublicProfilePage } from 'src/pages/PublicProfilePage';
import { PageNotFound } from 'src/pages/PageNotFound';
import { LandingPage } from 'src/pages/LandingPage';
import { InvitationsPage } from 'src/pages/InvitationsPage';
import { CirclesPage } from 'src/pages/CirclesPage';
import { NotificationsPage } from 'src/pages/NotificationsPage';

export const route = {
  landingPage: () => `/`,
  resetCallback: () => `/:reset`,
  confirmCallback: () => `/:confirm`,
  signup: () => '/signup',
  homePage: () => `/home`,
  profilePage: () => `/profile`,
  publicProfilePage: () => `/profile/:userId`,
  invitationsPage: () => `/invitations`,
  circlesPage: () => `/circles`,
  notificationsPage: () => `/notifications`,
};

export function Routes() {
  return (
    <Switch>
      <Route path={route.homePage()} exact component={HomePage} />
      <Route path={route.invitationsPage()} exact component={InvitationsPage} />
      <Route path={route.circlesPage()} exact component={CirclesPage} />
      <Route
        path={route.notificationsPage()}
        exact
        component={NotificationsPage}
      />
      <Route
        path={route.publicProfilePage()}
        exact
        component={PublicProfilePage}
      />
      <Route path={route.profilePage()} exact component={ProfilePage} />
      <Route path={route.resetCallback()} exact component={LandingPage} />
      <Route path={route.confirmCallback()} exact component={LandingPage} />
      <Route path={route.landingPage()} exact component={LandingPage} />
      <Route path={route.signup()} exact component={PageNotFound} />
      <Route path="*" component={PageNotFound} />
    </Switch>
  );
}
