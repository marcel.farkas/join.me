import React, { useState } from 'react';
import { gql, useQuery } from '@apollo/client';

import { ProfileTemplate } from 'src/templates/ProfileTemplate';

const USER_QUERY = gql`
  query User($limit: Int) {
    user {
      id
      first_name
      last_name
      email
      created_at
      verified
      description
      profilePictureUrl
      friends {
        id
        first_name
        friends {
          id
          first_name
        }
      }
      upcomingEvents {
        id
        place
        name
        eventDateTime
        eventPictureUrl
        participantsLimit
        participantsCount
        created_at
        creator_id
        private
        creator {
          first_name
          last_name
          profilePictureUrl
        }
        participants(limit: $limit) {
          id
          first_name
          last_name
          profilePictureUrl
        }
      }
      joinedEvents {
        id
        place
        name
        information
        eventDateTime
        eventPictureUrl
        participantsLimit
        participantsCount
        created_at
        creator_id
        private
        creator {
          first_name
          last_name
          profilePictureUrl
        }
        participants(limit: $limit) {
          id
          first_name
          last_name
          profilePictureUrl
        }
      }
      pastEvents {
        id
        place
        name
        information
        eventDateTime
        eventPictureUrl
        participantsLimit
        participantsCount
        created_at
        creator_id
        private
        creator {
          first_name
          last_name
          profilePictureUrl
        }
        participants(limit: $limit) {
          id
          first_name
          last_name
          profilePictureUrl
        }
      }
      createdEvents {
        id
        place
        name
        information
        eventDateTime
        eventPictureUrl
        participantsLimit
        participantsCount
        created_at
        creator_id
        private
        creator {
          first_name
          last_name
          profilePictureUrl
        }
        participants(limit: $limit) {
          id
          first_name
          last_name
          profilePictureUrl
        }
      }
      circleCount
      friendCount
      eventCount
    }
  }
`;

const CURRENT_USER_QUERY = gql`
  query User {
    user {
      id
    }
  }
`;

export function ProfilePage() {
  const {
    data,
    loading,
    error,
    refetch: refetchData,
  } = useQuery(USER_QUERY, {
    variables: { limit: 5 },
  });

  const currentUserId = useQuery(CURRENT_USER_QUERY).data?.user.id;

  const [isCreateEventDialogOpen, setIsCreateEventDialogOpen] = useState(false);

  const [isSettingsDialogOpen, setIsSettingsDialogOpen] = useState(false);
  const [isInviteDialogOpen, setIsInviteDialogOpen] = useState(false);

  const [isEventCalendarDialogOpen, setIsEventCalendarDialogOpen] =
    useState(false);

  return (
    <ProfileTemplate
      isCreateEventDialogOpen={isCreateEventDialogOpen}
      setIsCreateEventDialogOpen={setIsCreateEventDialogOpen}
      isSettingsDialogOpen={isSettingsDialogOpen}
      setIsSettingsDialogOpen={setIsSettingsDialogOpen}
      setIsEventCalendarDialogOpen={setIsEventCalendarDialogOpen}
      isEventCalendarDialogOpen={isEventCalendarDialogOpen}
      isInviteDialogOpen={isInviteDialogOpen}
      setIsInviteDialogOpen={setIsInviteDialogOpen}
      profileData={data}
      loading={loading}
      error={error}
      refetchData={refetchData}
      currentUserId={currentUserId}
    />
  );
}
