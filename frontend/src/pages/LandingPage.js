import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { Redirect } from 'react-router';
import { route } from 'src/Routes';

import { LandingTemplate } from 'src/templates/LandingTemplate';
import { useAuth } from 'src/utils/auth';
import { showToast } from '../utils/showToast';

export function LandingPage() {
  const { token: authToken } = useAuth();

  const [
    isPasswordResetRequestDialogOpen,
    setIsPasswordResetRequestDialogOpen,
  ] = useState(false);

  const [isPasswordResetDialogOpen, setIsPasswordResetDialogOpen] =
    useState(false);

  const [isSignInDialogOpen, setIsSignInDialogOpen] = useState(false);

  const [isSignUpDialogOpen, setIsSignUpDialogOpen] = useState(false);

  const [isConfirmEmailDialogOpen, setIsConfirmEmailDialogOpen] =
    useState(false);

  const dialogs = [
    {
      name: 'signIn',
      open: isSignInDialogOpen,
      setOpen: (value) => setIsSignInDialogOpen(value),
    },
    {
      name: 'signUp',
      open: isSignUpDialogOpen,
      setOpen: (value) => setIsSignUpDialogOpen(value),
    },
    {
      name: 'passwordResetRequest',
      open: isPasswordResetRequestDialogOpen,
      setOpen: (value) => setIsPasswordResetRequestDialogOpen(value),
    },
    {
      name: 'passwordReset',
      open: isPasswordResetDialogOpen,
      setOpen: (value) => setIsPasswordResetDialogOpen(value),
    },
    {
      name: 'confirmEmail',
      open: isConfirmEmailDialogOpen,
      setOpen: (value) => setIsConfirmEmailDialogOpen(value),
    },
  ];

  const location = useLocation();

  const [userId, setUserId] = useState('');
  const [token, setToken] = useState('');

  const closeDialog = (name) => {
    dialogs.forEach((dialog) =>
      dialog.name === name ? dialog.setOpen(false) : null,
    );
  };

  const openDialog = (name) => {
    dialogs.forEach((dialog) => {
      if (dialog.open) {
        if (dialog.name !== name) {
          dialog.setOpen(false);
        }
      } else {
        if (dialog.name === name) dialog.setOpen(true);
      }
    });
  };

  useEffect(() => {
    if (location.pathname === '/reset') {
      const type = new URLSearchParams(location.search).get('type');

      if (type === 'reset') {
        setUserId(parseInt(new URLSearchParams(location.search).get('user')));
        setToken(new URLSearchParams(location.search).get('token'));
        openDialog('passwordReset');
      }
    }

    const openConfirmEmailDialog = () => {
      if (location.pathname === '/confirm') {
        const id = parseInt(new URLSearchParams(location.search).get('user'));
        setUserId(id);
        const token = new URLSearchParams(location.search).get('token');
        setToken(token);
        if (id && token) openDialog('confirmEmail');
      }
    };
    openConfirmEmailDialog();
  }, [location.pathname, location.search]);

  return authToken ? (
    <Redirect to={route.homePage()} />
  ) : (
    <LandingTemplate
      userId={userId}
      token={token}
      isSignInDialogOpen={isSignInDialogOpen}
      isSignUpDialogOpen={isSignUpDialogOpen}
      isPasswordResetRequestDialogOpen={isPasswordResetRequestDialogOpen}
      isPasswordResetDialogOpen={isPasswordResetDialogOpen}
      isConfirmEmailDialogOpen={isConfirmEmailDialogOpen}
      openDialog={(name) => openDialog(name)}
      closeDialog={(name) => closeDialog(name)}
      showToast={showToast}
    />
  );
}
