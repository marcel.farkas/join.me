import React from 'react';

export function PageNotFound() {
  return (
    <div className="appWrapper">
      <h1>Error 404:</h1>
      <p>Page not found</p>
    </div>
  );
}
