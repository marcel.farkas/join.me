import React from 'react';

import { CirclesTemplate } from 'src/templates/CirclesTemplate';

export function CirclesPage() {
  return <CirclesTemplate />;
}
