import React, { useState, useCallback, useEffect } from 'react';
import { gql, useQuery, useMutation } from '@apollo/client';
import { useParams } from 'react-router-dom';
import { showToast } from '../utils/showToast';
import { PublicProfileTemplate } from 'src/templates/PublicProfileTemplate';
import { PageNotFound } from './PageNotFound';
import { Redirect } from 'react-router';

const CURRENT_USER_QUERY = gql`
  query User {
    user {
      id
    }
  }
`;

const USER_QUERY = gql`
  query Users($ids: [Int], $limit: Int) {
    users(ids: $ids) {
      id
      first_name
      last_name
      description
      profilePictureUrl
      upcomingEvents {
        id
        place
        name
        eventDateTime
        eventPictureUrl
        created_at
        creator_id
        private
        creator {
          first_name
          last_name
          profilePictureUrl
        }
        participants(limit: $limit) {
          id
          first_name
          last_name
          profilePictureUrl
        }
      }
      joinedEvents {
        id
        place
        name
        information
        eventDateTime
        eventPictureUrl
        created_at
        creator_id
        private
        creator {
          first_name
          last_name
          profilePictureUrl
        }
        participants(limit: $limit) {
          id
          first_name
          last_name
          profilePictureUrl
        }
      }
      pastEvents {
        id
        place
        name
        information
        eventDateTime
        eventPictureUrl
        created_at
        creator_id
        private
        creator {
          first_name
          last_name
          profilePictureUrl
        }
        participants(limit: $limit) {
          id
          first_name
          last_name
          profilePictureUrl
        }
      }
    }
  }
`;

const FRIENDS_QUERY = gql`
  query Friends {
    friends {
      id
      first_name
      last_name
      profilePictureUrl
    }
  }
`;

const ADD_FRIEND_MUTATION = gql`
  mutation AddFriend($selectedUserId: Int!) {
    addFriend(selectedUserId: $selectedUserId)
  }
`;

const REMOVE_FRIEND_MUTATION = gql`
  mutation RemoveFriend($selectedUserId: Int!) {
    removeFriend(selectedUserId: $selectedUserId)
  }
`;

export function PublicProfilePage() {
  const params = useParams();

  const {
    data,
    loading,
    error,
    refetch: refetchData,
  } = useQuery(USER_QUERY, {
    variables: { limit: 5, ids: [parseInt(params.userId)] },
  });

  const currentUserId = useQuery(CURRENT_USER_QUERY).data?.user.id;

  let profileData = [];
  profileData = { ...profileData, user: data?.users[0] };

  const friends = useQuery(FRIENDS_QUERY);

  const [isFriend, setIsFriend] = useState(false);
  useEffect(() => {
    setIsFriend(
      friends.data?.friends
        .map((friend) => friend.id)
        .indexOf(profileData.user?.id) !== -1,
    );
  }, [friends.data?.friends, profileData.user?.id]);

  const [removeFriend] = useMutation(REMOVE_FRIEND_MUTATION, {
    onCompleted: () => {
      showToast({
        type: 'SUCCESS',
        msg: `${profileData.user.first_name} ${profileData.user.last_name} removed from your contacts.`,
      });
    },
    onError: (error) => showToast({ type: 'ERROR', msg: error.message }),
    refetchQueries: [{ query: FRIENDS_QUERY }],
  });

  const [addFriend] = useMutation(ADD_FRIEND_MUTATION, {
    onCompleted: () => {
      showToast({
        type: 'SUCCESS',
        msg: `${profileData.user.first_name} ${profileData.user.last_name} added to your contacts.`,
      });
    },
    onError: (error) => showToast({ type: 'ERROR', msg: error.message }),
    refetchQueries: [{ query: FRIENDS_QUERY }],
  });

  const handleOnFriendBtnClick = useCallback(
    (variables) => {
      isFriend
        ? removeFriend({
            variables: { ...variables, selectedUserId: variables },
          })
        : addFriend({ variables: { ...variables, selectedUserId: variables } });
    },
    [removeFriend, addFriend, isFriend],
  );

  if (loading) return 'Loading...';
  if (error) return 'error';

  //if user does not exist, redirect to PageNotFound
  if (data.users?.length === 0) return <PageNotFound />;

  //if user tries to access their own page somehow,
  //redirect them to their private profile page
  if (data.users[0].id === currentUserId) return <Redirect to="/profile/" />;

  return (
    <PublicProfileTemplate
      profileData={profileData}
      loading={loading}
      error={error}
      refetchData={refetchData}
      currentUserId={currentUserId}
      friends={friends}
      handleOnFriendBtnClick={handleOnFriendBtnClick}
      isFriend={isFriend}
    />
  );
}
