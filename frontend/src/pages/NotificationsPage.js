import React from 'react';

import { NotificationsTemplate } from 'src/templates/NotificationsTemplate';

export function NotificationsPage() {
  return <NotificationsTemplate />;
}
