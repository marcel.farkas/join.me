import React from 'react';
import { gql, useQuery } from '@apollo/client';

import { HomeTemplate } from 'src/templates/HomeTemplate';

const DATA_QUERY = gql`
  query Data {
    upcomingEvents {
      id
      place
      name
      information
      private
      participantsLimit
      participantsCount
      eventPictureUrl
      participants {
        id
        first_name
        last_name
        profilePictureUrl
      }
      created_at
      eventDateTime
      creator_id
      creator {
        first_name
        last_name
        profilePictureUrl
      }
    }
    user {
      joinedEvents {
        id
      }
    }
  }
`;

const CURRENT_USER_QUERY = gql`
  query User {
    user {
      id
    }
  }
`;

export function HomePage() {
  const { data, refetch: refetchData } = useQuery(DATA_QUERY);
  const currentUserId = useQuery(CURRENT_USER_QUERY).data?.user.id;

  return (
    <HomeTemplate
      data={data}
      joinedEvents={data?.user?.joinedEvents}
      refetchData={refetchData}
      currentUserId={currentUserId}
    />
  );
}
