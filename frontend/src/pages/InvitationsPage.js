import React from 'react';
import { gql, useQuery } from '@apollo/client';

import { InvitationsTemplate } from 'src/templates/InvitationsTemplate';

const INVITATION_QUERY = gql`
  query Invs($limit: Int) {
    invitations {
      user {
        id
      }
      event {
        id
        place
        name
        eventDateTime
        eventPictureUrl
        created_at
        creator_id
        private
        creator {
          first_name
          last_name
          profilePictureUrl
        }
        participants(limit: $limit) {
          id
          first_name
          last_name
          profilePictureUrl
        }
      }
      accepted
    }
    circleInvitations {
      id
      circle_id
    }
  }
`;

const CURRENT_USER_QUERY = gql`
  query User {
    user {
      id
    }
  }
`;

export function InvitationsPage() {
  const {
    data,
    loading,
    error,
    refetch: refetchData,
  } = useQuery(INVITATION_QUERY, {
    variables: { limit: 5 },
  });

  const currentUserId = useQuery(CURRENT_USER_QUERY).data?.user.id;

  return (
    <InvitationsTemplate
      invitationData={data}
      loading={loading}
      error={error}
      refetchData={refetchData}
      currentUserId={currentUserId}
    />
  );
}
