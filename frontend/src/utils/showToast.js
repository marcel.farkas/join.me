import React from 'react';
import { toast } from 'react-toastify';

export const showToast = ({ type, msg }) => {
  switch (type) {
    case 'SUCCESS':
      toast.success(<div>{msg}</div>);
      break;
    case 'ERROR':
      toast.error(<div>{msg}</div>);
      break;
    case 'WARNING':
      toast.warn(<div>{msg}</div>);
      break;
    case 'INFO':
      toast.info(<div>{msg}</div>);
      break;
    default:
      toast(<div>{msg}</div>);
  }
};
